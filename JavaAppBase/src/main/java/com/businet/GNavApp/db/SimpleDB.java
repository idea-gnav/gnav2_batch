package com.businet.GNavApp.db;

import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



public class SimpleDB {
	private String driver;
	private String connectString;
	private Connection connection = null;

	public SimpleDB(String driverLib, String driver, String connectString) throws ClassNotFoundException, MalformedURLException {
		this.driver = driver;
		this.connectString = connectString;

		/*
		File file = new File(driverLib);
		URL[] jars = new URL[1];
		jars[0] = file.toURI().toURL();
		URLClassLoader child = new URLClassLoader(jars, this.getClass().getClassLoader());
		child.loadClass(driver);
		Class.forName(driver);
*/
	}

	public void open(String username, String password) throws SQLException {
		connection = DriverManager.getConnection(connectString, username, password);
	}

	public void close() throws SQLException {
		if (connection != null) {
			//コミット
//			connection.commit();
			connection.close();
		}
	}

	public List<Object[]> query(String sql) throws SQLException{
		List<Object[]> list = new ArrayList<Object[]>();
		Statement statement = null;

		try {
			statement = connection.createStatement();
			ResultSet result = statement.executeQuery(sql);

			while (result.next()) {
				Object[] row = new Object[result.getMetaData().getColumnCount()];
				for(int i=1; i<= result.getMetaData().getColumnCount(); i++) {
					row[i-1] = result.getObject(i);
				}
				list.add(row);
	        }
			statement.close();

		} catch(Exception ex) {
			if (statement != null) {
				statement.close();
			}
			throw ex;
		}
		return list;
	}



	// 2018/06/15 aishikawa@LBN Update processing added :Start
	public int queryUpdate(String sql) throws SQLException{
		Statement statement = null;
		int result;
		try {
			statement = connection.createStatement();
			result = statement.executeUpdate(sql);
			statement.close();

		} catch(Exception ex) {
			if (statement != null) {
				statement.close();
			}
			throw ex;
		}
		return result;
	}
	// 2018/06/15 aishikawa@LBN Update processing added :End

}
