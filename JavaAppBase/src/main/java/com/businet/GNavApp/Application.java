package com.businet.GNavApp;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.ini4j.InvalidFileFormatException;

import com.businet.GNavApp.batch.Batchlet;
import com.businet.GNavApp.logger.RLog;

public class Application {
	private static Application current;
	private Configuration config;


	public Configuration getConfig() {
		return config;
	}

	public static Application getCurrent() {
		return current;
	}

	public static void init() throws URISyntaxException {
		try {
			current = new Application();

			RLog.Setup();

		} catch (IOException e) {
			current = null;
			e.printStackTrace();
		}
	}

	public String runBatlet(Batchlet batchlet) throws Exception {
		return batchlet.process();
	}

	private String getJarFolder() {
	    String absolutePath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
	    absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
	    absolutePath = absolutePath.replaceAll("%20"," "); // Surely need to do this here
	    return absolutePath;
	  }

	public Application() throws InvalidFileFormatException, IOException, URISyntaxException {
			ClassLoader classLoader = getClass().getClassLoader();
			config = new Configuration();



			 //String configFile = getJarFolder()+"config.ini";

			String configFile =classLoader.getResource("config.ini").getFile();
			File file = new File(configFile);
			config.init(file);
	}



}
