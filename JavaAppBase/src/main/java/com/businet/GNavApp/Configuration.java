package com.businet.GNavApp;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;

public class Configuration {
	private Wini ini;
	private File iniFile;


	public boolean init(File file) throws InvalidFileFormatException, IOException {
		iniFile = file;
		ini = new Wini();

		ini.load(iniFile);

		return true;
	}


	public int get(String section, String key, int defaultValue) {
		Integer val = ini.get(section, key, Integer.class);
		if (val != null) {
			return val;
		}
		else {
			return defaultValue;
		}
	}

	public String get(String section, String key, String defaultValue) {
		String val = ini.get(section, key, String.class);
		if (val != null) {
			return val;
		}
		else {
			return defaultValue;
		}
	}

	public Map<String, String> getAll(String session) {
		Map<String, String> map = ini.get(session);

		return map;
	}
}

