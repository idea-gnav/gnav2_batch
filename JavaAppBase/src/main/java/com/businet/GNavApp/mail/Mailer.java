package com.businet.GNavApp.mail;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.businet.GNavApp.Application;

public class Mailer {

	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private Session mailSession;
	private boolean isAuthentication = false;
	private String user = "";
	private String password = "";
	private String transportProtocol = "";
	private String from = "";


	private static Mailer _instance;
	public static Mailer Instance(){
		if (_instance == null)
			_instance = new Mailer();
		return _instance;
	}

	public Mailer() {

		Properties props = new Properties();

		Map<String, String> configs = Application.getCurrent().getConfig().getAll("Mailer");

		if (configs != null) {
			for (Map.Entry<String, String> entry : configs.entrySet()) {
				if (entry.getKey().equalsIgnoreCase("user")) {
					user = entry.getValue();
				}
				else if (entry.getKey().equalsIgnoreCase("password")) {
					password = entry.getValue();
				}
				else if (entry.getKey().equalsIgnoreCase("authentication"))
				{
					isAuthentication = true;
				}
				else if (entry.getKey().equalsIgnoreCase("transportProtocol")) {
					transportProtocol = entry.getValue();
				}
				else if (entry.getKey().equalsIgnoreCase("from")) {
					from = entry.getValue();
				}
				else {
					props.put(entry.getKey(),entry.getValue());
				}
			}


			Authenticator authenticator = null;
			if (isAuthentication) {
				authenticator = new javax.mail.Authenticator() {
				      protected PasswordAuthentication getPasswordAuthentication() {
				    	  return new PasswordAuthentication(user,password);
				      }
				};
			}

			if (isAuthentication)
				mailSession = Session.getDefaultInstance(props, authenticator);
			else
				mailSession = Session.getDefaultInstance(props);
		}



	}



    public boolean sendMail(String mailTo, String subject, String textMessage,List<String> pathFileAttachment) {
    	try {
            Message message = new MimeMessage(mailSession);
            message.setFrom(new InternetAddress(from));
            //message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(mailTo));

            String[] mails = mailTo.split(";");

            for(int i=0; i< mails.length; i++) {
            	message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(mails[i]) );
            }


            message.setSubject(subject);			// タイトル

            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText(textMessage);

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            if(pathFileAttachment != null)
            {
	            for (String stringPath : pathFileAttachment) {

	            	messageBodyPart = new MimeBodyPart();
	                String filename = stringPath;

	                DataSource source = new FileDataSource(filename);

	                messageBodyPart.setDataHandler(new DataHandler(source));
//	                messageBodyPart.setDataHandler(new DataHandler(source, "UTF-8"));
	                messageBodyPart.setFileName(source.getName());
	                multipart.addBodyPart(messageBodyPart);
				}
            }
            message.setContent(multipart);

            Transport.send(message);

            return true;

        } catch (AddressException e) {
        	logger.log(Level.SEVERE, e.getMessage(), e);
        	return false;

        } catch (MessagingException e) {
        	logger.log(Level.SEVERE, e.getMessage(), e);
        	return false;
        }
    }
}
