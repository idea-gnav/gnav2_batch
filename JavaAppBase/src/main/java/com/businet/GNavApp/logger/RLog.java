package com.businet.GNavApp.logger;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.businet.GNavApp.Application;

public class RLog {

	static private FileHandler exceptionHandle;
    static private ExceptionFormatter exceptionFormatter;

//    static private FileHandler logHandle;
//    static private SimpleFormatter simpleFormat;

	public static void Setup() throws SecurityException, IOException {
		//Logger ExceptionLogger = new java.util.logging.
		String logPath = Application.getCurrent().getConfig().get("Log", "LogFolder", "");
		int MaxFileSize = Application.getCurrent().getConfig().get("Log", "MaxFileSize", 50000);
		int MaxFileCount = Application.getCurrent().getConfig().get("Log", "MaxFileCount", 5);
		//Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		Logger logger = Logger.getLogger("");
		exceptionHandle = new FileHandler(logPath +"/" + Application.getCurrent().getConfig().get("Log", "LogFilename", ""), MaxFileSize, MaxFileCount, true);
		exceptionHandle.setLevel(Level.ALL);
		exceptionFormatter = new ExceptionFormatter();	//ExceptionFilename@
		exceptionHandle.setFormatter(exceptionFormatter);
		logger.addHandler(exceptionHandle);


	}
}
