package com.businet.GNavApp.logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

import com.businet.GNavApp.Application;

public class ExceptionFormatter extends SimpleFormatter{

	@Override
	public String format(LogRecord record) {

		String returnMessage = super.format(record);

		if (record.getThrown() != null) {
			Throwable ex = record.getThrown();
			Date today = Calendar.getInstance().getTime();

			String exceptionFile = Application.getCurrent().getConfig().get("Log", "LogFolder", "");

			String fileName = Application.getCurrent().getConfig().get("Log", "ExceptionFilename", "");

			SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd_HHmmss_SSS");
			fileName = fileName.replaceFirst("%t", fmt.format(today));
			exceptionFile = exceptionFile +"/" + fileName;

			fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
			try {
				File file = new File(exceptionFile);
		        PrintStream ps;
				ps = new PrintStream(file);
				ps.println("Caused by: " + ex.getMessage());
				ex.printStackTrace(ps);

			} catch (FileNotFoundException e) {

			}
		}
		return returnMessage;
	}

}
