package com.businet.GNavApp;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.businet.GNavApp.batch.SendDeviceLogFormat;
import com.businet.GNavApp.batch.SendPushNotification;

public class Main {
	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private final static Logger failedDeviceLogger = Logger.getLogger(SendPushNotification.FAILED_DEVICE_LOG);

	public static void main(String[] args) {
		try {

			Application.init();

			String logPath = Application.getCurrent().getConfig().get("Log", "LogFolder", "");
			int MaxFileSize = Application.getCurrent().getConfig().get("Log", "MaxFileSize", 50000);
			int MaxFileCount = Application.getCurrent().getConfig().get("Log", "MaxFileCount", 5);


			FileHandler failedDeviceLoggerHandle;
			SendDeviceLogFormat logFormat = new SendDeviceLogFormat();

			failedDeviceLoggerHandle = new FileHandler(logPath +"/" + Application.getCurrent().getConfig().get("Log", "SentDeviceLog", ""), MaxFileSize, MaxFileCount, true );
			failedDeviceLoggerHandle.setLevel(Level.ALL);
			failedDeviceLoggerHandle.setFormatter(logFormat);
			failedDeviceLogger.addHandler(failedDeviceLoggerHandle);

			logger.log(Level.INFO, "START PROGESS PUSH NOTIFICATION");
			String result = Application.getCurrent().runBatlet(new SendPushNotification());
			logger.log(Level.INFO, "END PROGESS PUSH NOTIFICATION\n\n");

			return;

		} catch (Exception e) {
//			System.out.println("FAILED");
			logger.log(Level.SEVERE, e.getMessage(), e);
		}
	}

}
