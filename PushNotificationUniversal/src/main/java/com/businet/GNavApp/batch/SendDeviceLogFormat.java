package com.businet.GNavApp.batch;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class SendDeviceLogFormat extends Formatter{

	@Override
	public String format(LogRecord record) {

		StringBuilder message = new StringBuilder();

		Date today = Calendar.getInstance().getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");

		message.append(fmt.format(today));
		message.append("    ");
		message.append(record.getMessage());
		message.append("\r\n");

		return message.toString();
	}

}