package com.businet.GNavApp.batch;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.businet.GNavApp.Application;
import com.businet.GNavApp.db.SimpleDB;

public class ExecuteQuerys {

	private String driverPath;
	private String driver;
	private String connectString;
	private String username;
	private String password;

	public ExecuteQuerys() {
		driverPath = Application.getCurrent().getConfig().get("Database", "DriverFile", "");
		driver = Application.getCurrent().getConfig().get("Database", "Driver", "");
		connectString = Application.getCurrent().getConfig().get("Database", "Connection", "");
		username = Application.getCurrent().getConfig().get("Database", "UserName", "");
		password = Application.getCurrent().getConfig().get("Database", "Password", "");
	}

	/**
	 * 繝励ャ繧ｷ繝･騾夂衍繝�繝ｼ繝悶Ν縺九ｉ繝�繝ｼ繧ｿ繧貞叙蠕励☆繧�
	 *
	 * @param lastDate
	 * @return
	 */
	public List<Object[]> findByPushNotification(Date lastestDateTime)
			throws ClassNotFoundException, SQLException, MalformedURLException {

		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSSSSS");
//		String setTimestamp = sdf.format(lastestDateTime);
		//DucNKT change to use current JST time: 2019/07/04 ↓↓↓
		Timestamp setTimestamp = new Timestamp(System.currentTimeMillis()); // JST
		//DucNKT change to use current JST time: 2019/07/04 ↑↑↑

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("TARGET_USER_ID, "); // [0]
		sb.append("TARGET_KNGEN_CD, "); // [1]
		sb.append("PUSH_TITLE, "); // [2]
		sb.append("PUSH_MESSAGE, "); // [3]
		sb.append("APP_FLG, "); // [4]
		sb.append("LANGUAGE_CD, "); // [5]
		sb.append("NOTIFICATION_ID, "); // [6]
		// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
		sb.append("REMARKS "); // [7]
		// 2019/06/20 RS DucNKT Add For ReturnMachine :End
		sb.append("FROM TBL_PUSH_NOTIFICATION ");
		sb.append("WHERE DEL_FLG = 0 ");
//		sb.append("AND APP_FLG = 0 ");
		sb.append("AND PUSH_SEND_DTM IS NULL ");
//		sb.append("AND LANGUAGE_CD = 'en' ");
		sb.append("AND PUSH_RSV_DTM <= '" + sdf.format(setTimestamp) + "' "); // JST

//		System.out.println(new String(sb));

		List<Object[]> data;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			db.close();
			return data;

		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}

	/**
	 * 繝�繝舌う繧ｹ繝�繝ｼ繝悶Ν縺九ｉ蟇ｾ雎｡繝�繝舌う繧ｹ繧貞叙蠕励☆繧�
	 *
	 * @param lastDate
	 * @return
	 */
	public List<Object[]> findByDeviceToken(String userIds, String kengens, Integer appFlg, String languageCd,
			Boolean targetForReturnMachine) throws ClassNotFoundException, SQLException, MalformedURLException {

		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("DISTINCT(TUD.DEVICE_TOKEN), ");
		sb.append("TUD.DEVICE_TYPE, ");
		sb.append("TUD.USER_ID, ");
		sb.append("TUD.LANGUAGE_CD ");
		// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
		if (targetForReturnMachine) {
			sb.append(",(  SELECT COUNT(*) FROM tbl_favorite_dtc_history tfdh ");
			sb.append("WHERE tfdh.USER_ID = TUD.USER_ID ");
			sb.append("AND del_flg = 0 AND tfdh.read_flg = 0 ");
			sb.append(") dtcbadge, ");
			sb.append("( ");
			sb.append("SELECT COUNT(*) FROM tbl_return_machine_history trmh ");
			// 2021/07/01 iDEA Yamashita  リターンmシン拠店化対応
			sb.append("JOIN MST_RETURN_MACHINE_SEND_USER mrmsu ON trmh.KYOTEN_CD = mrmsu.KYOTEN_CD ");
//			sb.append("WHERE trmh.USER_ID = TUD.USER_ID ");
			sb.append("WHERE mrmsu.USER_ID = TUD.USER_ID ");
			sb.append("AND trmh.read_flg = 0 ");
			sb.append("AND mrmsu.DELETE_FLG = 0 ");
			// 2021/07/01 iDEA Yamashita  リターンmシン拠店化対応
			sb.append(") rmbadge ");
		}
		// 2019/06/20 RS DucNKT Add For ReturnMachine :End
		sb.append("FROM TBL_USER_DEVICE TUD ");
		sb.append("JOIN MST_USER MU ");
		sb.append("ON TUD.USER_ID = MU.USER_ID ");
		sb.append("JOIN MST_SOSIKI MS ");
		sb.append("ON MU.SOSIKI_CD = MS.SOSIKI_CD ");

		sb.append("WHERE LENGTH(TUD.DEVICE_TOKEN) > 60 "); // 60譁�蟄玲悴貅�縺ｯ蟇ｾ雎｡螟�

		sb.append("AND TUD.DEL_FLG = 0 ");
		sb.append("AND TUD.APP_FLG = " + appFlg + " ");

		// 2019/06/20 RS DucNKT Modify For ReturnMachine :Start
		if (!targetForReturnMachine)
			sb.append("AND LOWER(TUD.LANGUAGE_CD) = '" + languageCd.toLowerCase() + "' ");
		// 2019/06/20 RS DucNKT Modify For ReturnMachine :End

		if (userIds != null && kengens != null)
			sb.append("AND (TUD.USER_ID IN (" + strStrFormat(userIds) + ") OR MS.KENGEN_CD IN (" + strIntFormat(kengens)
					+ ") ) ");
		if (userIds != null && kengens == null)
			sb.append("AND TUD.USER_ID IN (" + strStrFormat(userIds) + ") ");
		if (userIds == null && kengens != null)
			sb.append("AND MS.KENGEN_CD IN (" + strIntFormat(kengens) + ") ");

//		System.out.println(new String(sb));

		List<Object[]> data;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			db.close();
			return data;

		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}

	/**
	 * 繝励ャ繧ｷ繝･騾夂衍蜃ｦ逅�螳御ｺ�譌･譎ゅｒ險倬鹸縺吶ｋ
	 *
	 * @param lastDate
	 * @return
	 */
	public int updateByPushSendDtm(int notificationId)
			throws ClassNotFoundException, SQLException, MalformedURLException {

		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		StringBuilder sb = new StringBuilder();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSSSSS");
		Timestamp tsm = new Timestamp(System.currentTimeMillis()); // JST

		sb.append("UPDATE TBL_PUSH_NOTIFICATION SET PUSH_SEND_DTM = '" + sdf.format(tsm) + "' WHERE NOTIFICATION_ID = "
				+ notificationId);

//		System.out.println(new String(sb));

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;

		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}

	public Object[] findBadgeByUserID(String userId)
			throws ClassNotFoundException, SQLException, MalformedURLException {
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT a.dtcbadge,b.rmbadge ");
		sb.append("FROM ");
		sb.append("(  SELECT COUNT(*) AS dtcbadge FROM tbl_favorite_dtc_history tfdh ");
		sb.append("WHERE tfdh.user_id = '" + userId + "' ");
		sb.append("AND del_flg = 0 AND tfdh.read_flg = 0 ");
		sb.append(") a, ");
		sb.append("( ");
		sb.append("SELECT COUNT(*) AS rmbadge FROM tbl_return_machine_history trmh ");
		sb.append("JOIN MST_RETURN_MACHINE_SEND_USER mrmsu ON trmh.KYOTEN_CD = mrmsu.KYOTEN_CD ");
		sb.append("WHERE mrmsu.USER_ID = '" + userId + "' ");
		//		sb.append("WHERE trmh.user_id = '" + userId + "' ");
		sb.append("AND trmh.read_flg = 0 ");
		sb.append("AND mrmsu.DELETE_FLG = 0 ");
		sb.append(") b ");

		List<Object[]> data;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			db.close();
			if (data == null || data.size() != 1)
				return null;
			else
				return data.get(0);

		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}

	private String strStrFormat(String str) {
		String[] strList = str.split(",");
		StringBuilder sbStr = new StringBuilder();
		for (int index = 0; index < strList.length; index++) {
			sbStr.append("'" + strList[index] + "'");
			if (index < (strList.length - 1))
				sbStr.append(",");
		}
		return new String(sbStr);
	}

	private String strIntFormat(String str) {
		String[] strList = str.split(",");
		StringBuilder sbStr = new StringBuilder();
		for (int index = 0; index < strList.length; index++) {
			sbStr.append(strList[index]);
			if (index < (strList.length - 1))
				sbStr.append(",");
		}
		return new String(sbStr);
	}

}
