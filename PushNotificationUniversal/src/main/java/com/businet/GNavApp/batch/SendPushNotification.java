package com.businet.GNavApp.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Semaphore;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.businet.GNavApp.Application;
import com.eatthepath.pushy.apns.ApnsClient;
import com.eatthepath.pushy.apns.ApnsClientBuilder;
import com.eatthepath.pushy.apns.PushNotificationResponse;
import com.eatthepath.pushy.apns.util.ApnsPayloadBuilder;
import com.eatthepath.pushy.apns.util.SimpleApnsPayloadBuilder;
import com.eatthepath.pushy.apns.util.SimpleApnsPushNotification;
import com.eatthepath.pushy.apns.util.TokenUtil;
import com.eatthepath.pushy.apns.util.concurrent.PushNotificationFuture;

public class SendPushNotification implements Batchlet {

	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private final static Logger deviceLogger = Logger.getLogger(SendPushNotification.FAILED_DEVICE_LOG);

	public final static String FAILED_DEVICE_LOG = "FAILED_DEVICE_LOG";
	private final String DATE_TIME_FORMAT = "yyyyMMdd HH:mm:ss:SSS";

	private String LastSendTimeFile;
//	private String CERTIFICATE_PASSWORD;
	private String CERTIFICATE_PW_RC; // RemoteCAREApp
	private String CERTIFICATE_PW_GN; // GNavApp
//	private String PATH_CERTIFICATE;
	private String PATH_CERTIFICATE_RC; // RemoteCAREApp
	private String PATH_CERTIFICATE_GN; // GNavApp
//	private String AUTH_KEY_FCM;
	private String AUTH_KEY_FCM_RC; // RemoteCAREApp
	private String AUTH_KEY_FCM_GN; // GNavApp
	private String API_URL_FCM;

	private String pushNotificationTitle;
	private String pushNotificationMsg;
	private String userIds;
	private String kengens;
	private Integer appFlg;
	private String languageCd;
	private Integer notificationId;

	// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
	private String TITLE_EN;
	private String TITLE_JA;
	private String TITLE_ID;
	private String TITLE_TR;

	private String MSG_EN;
	private String MSG_JA;
	private String MSG_ID;
	private String MSG_TR;
	// 2019/06/20 RS DucNKT Add For ReturnMachine :End

	private boolean IsTestEnvironment = false;

	private String TOPIC_GNAV;//2021.03.23 DucNKT added
	private String TOPIC_REMOTE_CARE;//2021.03.23 DucNKT added
	
	/**
	 * Default constructor.
	 */
	public SendPushNotification() throws Exception {

		if (Application.getCurrent().getConfig().get("BatchSettings", "Environment", "product").equals("test")) {
			IsTestEnvironment = true;
		}
		TOPIC_GNAV = Application.getCurrent().getConfig().get("BatchSettings", "TopicGnav", "");//2021.03.23 DucNKT added
		TOPIC_REMOTE_CARE = Application.getCurrent().getConfig().get("BatchSettings", "TopicRemoteCare", "");//2021.03.23 DucNKT added
		
		LastSendTimeFile = Application.getCurrent().getConfig().get("BatchSettings", "LastSendTimeFile", "");
//		PATH_CERTIFICATE = Application.getCurrent().getConfig().get("BatchSettings", "IOSCertification", "");
		PATH_CERTIFICATE_RC = Application.getCurrent().getConfig().get("BatchSettings", "IOSCertificationRC", "");
		PATH_CERTIFICATE_GN = Application.getCurrent().getConfig().get("BatchSettings", "IOSCertificationGN", "");
//		CERTIFICATE_PASSWORD = Application.getCurrent().getConfig().get("BatchSettings", "IOSCertificationPassword","");
		CERTIFICATE_PW_RC = Application.getCurrent().getConfig().get("BatchSettings", "IOSCertificationPasswordRC", "");
		CERTIFICATE_PW_GN = Application.getCurrent().getConfig().get("BatchSettings", "IOSCertificationPasswordGN", "");
//		AUTH_KEY_FCM = Application.getCurrent().getConfig().get("BatchSettings", "AndroidFCMKey", "");
		AUTH_KEY_FCM_RC = Application.getCurrent().getConfig().get("BatchSettings", "AndroidFCMKeyRC", "");
		AUTH_KEY_FCM_GN = Application.getCurrent().getConfig().get("BatchSettings", "AndroidFCMKeyGN", "");
		API_URL_FCM = Application.getCurrent().getConfig().get("BatchSettings", "AndroidFCMUrl", "");

		// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
		ReadMessage(Application.getCurrent().getConfig().get("BatchSettings", "MesssageFile", ""));
		ReadTitle(Application.getCurrent().getConfig().get("BatchSettings", "TitleFile", ""));
		// 2019/06/20 RS DucNKT Add For ReturnMachine :End
	}

	/**
	 * @see Batchlet#process()
	 */
	public String process() {
		int sendCounter = 0;

		try {

			ExecuteQuerys eq = new ExecuteQuerys();

			Date lastestTime = ReadLastSend();
			WriteLastSend();

			// 繝励ャ繧ｷ繝･騾夂衍繝�繝ｼ繝悶Ν縺矩�夂衍蟇ｾ雎｡諠�蝣ｱ繧貞叙蠕�
			List<Object[]> tblPushNotification = eq.findByPushNotification(lastestTime);

			if (tblPushNotification != null && tblPushNotification.size() > 0) {

				for (int index = 0; index < tblPushNotification.size(); index++) {

					userIds = null;
					kengens = null;
					appFlg = null;
					languageCd = null;
					notificationId = null;
					// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
					pushNotificationTitle = null;
					pushNotificationMsg = null;
					// 2019/06/20 RS DucNKT Add For ReturnMachine :End
					String remarks = "";

					if (tblPushNotification.get(index)[0] != null)
						userIds = tblPushNotification.get(index)[0].toString(); // [0] TARGET_USER_ID
					if (tblPushNotification.get(index)[1] != null)
						kengens = tblPushNotification.get(index)[1].toString(); // [1] TARGET_KNGEN_CD
					if (tblPushNotification.get(index)[2] != null)
						pushNotificationTitle = tblPushNotification.get(index)[2].toString(); // [2] PUSH_TITLE
					if (tblPushNotification.get(index)[3] != null)
						pushNotificationMsg = tblPushNotification.get(index)[3].toString(); // [3] PUSH_MESSAGE
					if (tblPushNotification.get(index)[4] != null)
						appFlg = Integer.parseInt(tblPushNotification.get(index)[4].toString()); // [4] APP_FLG
					if (tblPushNotification.get(index)[5] != null)
						languageCd = tblPushNotification.get(index)[5].toString(); // [5] LANGUAGE_CD
					if (tblPushNotification.get(index)[6] != null)
						notificationId = Integer.parseInt(tblPushNotification.get(index)[6].toString()); // [6]
																											// NOTIFICATION_ID
					// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
					if (tblPushNotification.get(index)[7] != null)
						remarks = tblPushNotification.get(index)[7].toString(); // REMARKS

					Boolean isReturnMachine = (pushNotificationTitle == null && pushNotificationMsg == null
							&& languageCd == null && remarks.equals("ReturnMachine"));

					// 2019/06/20 RS DucNKT Add For ReturnMachine :End

					// 蟇ｾ雎｡繝�繝舌う繧ｹ繧貞叙蠕�
					List<Object[]> deviceList = eq.findByDeviceToken(userIds, kengens, appFlg, languageCd,isReturnMachine);

					if (deviceList != null && deviceList.size() > 0) {

						// 蜃ｦ逅�譌･譎ゅｒ譖ｴ譁ｰ
						// error縲・xception縺ｧ譖ｴ譁ｰ縺輔ｌ縺壻ｺ碁㍾騾∽ｿ｡縺輔ｌ繧九�ｮ繧帝亟縺舌◆繧√��
						eq.updateByPushSendDtm(notificationId);

						// APNs
						this.SendApns(deviceList, appFlg, isReturnMachine);
						// FCM
						this.SendFCM(deviceList, appFlg, isReturnMachine);

//						// 蜃ｦ逅�譌･譎ゅｒ譖ｴ譁ｰ
//						eq.updateByPushSendDtm(notificationId);

						sendCounter++;
						logger.log(Level.INFO,
								"send push notification. notification id=" + notificationId + " [" + languageCd + "]");

					} else {
						// 蟇ｾ雎｡繝�繝舌う繧ｹ縺後↑縺�蝣ｴ蜷医ｂ縲∝�ｦ逅�譌･譎ゅｒ譖ｴ譁ｰ
						eq.updateByPushSendDtm(notificationId);
						logger.log(Level.INFO,
								"No record found. notification id=" + notificationId + " [" + languageCd + "]");
					}
				}
			} else
				return "No record found";

		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return "FAILED";
		}

		if (sendCounter == 0)
			return "No record found";

		return "Push Notification Complete";
	}

	/**
	 * FCM Android Send Push Notification
	 * 
	 * @param DeviceList
	 * @param appFlg     0:RemoteCAREApp, 1:GNavApp
	 */
	private void SendFCM(List<Object[]> DeviceList, Integer appFlg, Boolean isReturnMachine) {

		StringBuilder strDeviceList = new StringBuilder();

		String PushType = (isReturnMachine) ? "[RETURN MACHINE] " : "";
		strDeviceList.append(PushType + "Begin sending a batch to FCM. Device tokens:");

		JSONArray devices = new JSONArray();

		for (int i = 0; i < DeviceList.size(); i++) {
			Object[] device = DeviceList.get(i);
			if (device[0] != null) {
				String deviceType = "";
				String deviceToken = device[0].toString();
				String userId = "";
				String languageCd = "";
				int dtcbadge = 0, returnMachineBadge = 0, badge = 0;

				if (device[1] != null)
					deviceType = device[1].toString();
				if (device[2] != null)
					userId = device[2].toString();
				if (device[3] != null)
					languageCd = device[3].toString();

				// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
				if (device.length >= 6) {
					if (device[4] != null)
						dtcbadge = Integer.parseInt(device[4].toString());

					if (device[5] != null)
						returnMachineBadge = Integer.parseInt(device[5].toString());
				}
				badge = dtcbadge + returnMachineBadge;

				if (badge >= 1000)
					badge = 999;

				// 2019/06/20 RS DucNKT Add For ReturnMachine :End

				if (deviceType.equals("2") && !userId.isEmpty() && !deviceToken.isEmpty()) {
					strDeviceList.append("\n		[USER_ID: " + userId + "] [DEVICE TOKEN: " + deviceToken
							+ "] [BADGE: " + badge + "] [LANGUAGE: " + languageCd + "]");

					// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
					if (isReturnMachine) {
						JSONObject Message = new JSONObject();
						JSONObject info = new JSONObject();
						try {
							Message.put("to", deviceToken);
							info.put("title", createTitle(languageCd)); // Notification title
							info.put("body", createMassage(languageCd)); // Notification body
							info.put("dtcbadge", badge); // Notification Badge

							Message.put("data", info);
							devices.put(Message);
						} catch (Exception e) {
							logger.log(Level.SEVERE, e.getMessage(), e);
						}
					} else
						devices.put(deviceToken);
					// 2019/06/20 RS DucNKT Add For ReturnMachine :End
				}
			}
		}

		if (devices.length() > 0) {
			deviceLogger.log(Level.INFO, strDeviceList.toString());
		} else {
			return;

		}

		try {
			String authKey = null; // You FCM AUTH key
			String FMCurl = API_URL_FCM;

			if (appFlg.equals(0))
				authKey = AUTH_KEY_FCM_RC;
			if (appFlg.equals(1))
				authKey = AUTH_KEY_FCM_GN;

			URL url = new URL(FMCurl);

			// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
			int cnt = (isReturnMachine) ? devices.length() : 1;

			for (int i = 0; i < cnt; i++) {
				// 2019/06/20 RS DucNKT Add For ReturnMachine :End

				HttpURLConnection conn = (HttpURLConnection) url.openConnection();

				conn.setUseCaches(false);
				conn.setDoInput(true);
				conn.setDoOutput(true);

				conn.setRequestMethod("POST");
				conn.setRequestProperty("Authorization", "key=" + authKey);
				conn.setRequestProperty("Content-Type", "application/json");

				JSONObject data = new JSONObject();

				if (!isReturnMachine) {
					data.put("registration_ids", devices);
					JSONObject info = new JSONObject();
					info.put("title", pushNotificationTitle); // Notification title
					info.put("body", pushNotificationMsg); // Notification body
					data.put("data", info);
				} else {
					// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
					data = devices.getJSONObject(i);
					// 2019/06/20 RS DucNKT Add For ReturnMachine :End
				}

				OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
				wr.write(data.toString());
				wr.flush();
				wr.close();

				int responseCode = conn.getResponseCode();
				if (responseCode != 200) {
					throw new Exception("Failed ! Respone Code: " + responseCode);
				}

				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				if (response != null) {

					JSONObject jResponse = new JSONObject(response.toString());
					int failedCount = jResponse.getInt("failure");

					if (failedCount > 0) {
						JSONArray list = jResponse.getJSONArray("results");

						StringBuilder mes = new StringBuilder();
						mes.append("	Device tokens was sent failed:");

						for (int j = 0; j < list.length(); j++) {
							JSONObject res = list.getJSONObject(j);
							if (res.has("error"))
								mes.append("\n[DEVICE TOKEN: " + devices.getString(j) + "]");
						}

						deviceLogger.log(Level.WARNING, mes.toString());
					}
				}

				in.close();
			}
			deviceLogger.log(Level.INFO, "	Sending a batch to FCM devices successfully.\n\n");
		} catch (Exception e) {
			deviceLogger.log(Level.WARNING,
					"	Sending a batch to FCM devices failed. No device tokens was sent. Caused by:" + e.getMessage()
							+ "\n\n");
			logger.log(Level.SEVERE, e.getMessage(), e);
		}

	}

	/**
	 * APNs iOS Send Push Notification
	 * 
	 * @param DeviceList
	 * @param appFlg     0:RemoteCAREApp, 1:GNavApp
	 */
	private void SendApns(List<Object[]> DeviceList, Integer appFlg, Boolean isReturnMachine) {

		StringBuilder strDeviceList = new StringBuilder();
		String PushType = (isReturnMachine) ? "[RETURN MACHINE] " : "";
		strDeviceList.append(PushType+"Begin sending a batch to APNs. Device tokens:");

		ArrayList<String> devices = new ArrayList<String>();

		// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
		ArrayList<Integer> dtcBadges = new ArrayList<Integer>();
		ArrayList<String> languageCds = new ArrayList<String>();
		// 2019/06/20 RS DucNKT Add For ReturnMachine :End

		for (int index = 0; index < DeviceList.size(); index++) {
			Object[] device = DeviceList.get(index);

			if (device[0] != null) {
				String deviceType = "";
				String deviceToken = device[0].toString();
				String userId = "";
				String languageCd = "";
				// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
				int dtcbadge = 0, returnMachineBadge = 0, badge = 0;
				// 2019/06/20 RS DucNKT Add For ReturnMachine :End
				if (device[1] != null)
					deviceType = device[1].toString();
				if (device[2] != null)
					userId = device[2].toString();
				if (device[3] != null)
					languageCd = device[3].toString();

				// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
				if (device.length >= 6) {
					if (device[4] != null)
						dtcbadge = Integer.parseInt(device[4].toString());

					if (device[5] != null)
						returnMachineBadge = Integer.parseInt(device[5].toString());
				}
				badge = dtcbadge + returnMachineBadge;

				if (badge >= 1000)
					badge = 999;

				// 2019/06/20 RS DucNKT Add For ReturnMachine :End

				if (deviceType.equals("1") && !userId.isEmpty() && !deviceToken.isEmpty()) {
					strDeviceList.append("\n		[USER_ID: " + userId + "] [" + "DEVICE TOKEN: " + deviceToken
							+ "] [BADGE: " + badge + "] [LANGUAGE: " + languageCd + "]");

					devices.add(deviceToken);
					// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
					dtcBadges.add(badge);
					languageCds.add(languageCd);
					// 2019/06/20 RS DucNKT Add For ReturnMachine :End

				}
			}
		}

		if (devices.size() > 0)
			deviceLogger.log(Level.INFO, strDeviceList.toString());
		else
			return;

		ApnsClient apnsClient = null; //2021.09.06 DucNKT modify
		try {

			String pathCertificate = null;
			String certificatePassword = null;
			String topic = null;//2021.03.23 DucNKT added
			if (appFlg.equals(0)) {
				pathCertificate = PATH_CERTIFICATE_RC;
				certificatePassword = CERTIFICATE_PW_RC;
				topic = TOPIC_REMOTE_CARE;//2021.03.23 DucNKT added
			}
			if (appFlg.equals(1)) {
				pathCertificate = PATH_CERTIFICATE_GN;
				certificatePassword = CERTIFICATE_PW_GN;
				topic = TOPIC_GNAV;//2021.03.23 DucNKT added
			}

			//2021.03.23 DucNKT modify : start
			/*
			ApnsService service = null;

			if (IsTestEnvironment)
				service = APNS.newService().withCert(pathCertificate, certificatePassword).withSandboxDestination()
						.build();
			else
				service = APNS.newService().withCert(pathCertificate, certificatePassword).withProductionDestination()
						.build();

			// 2019/06/20 RS DucNKT Modify For ReturnMachine :Start
			if (!isReturnMachine) {

				PayloadBuilder payload = APNS.newPayload();
				payload.alertTitle(pushNotificationTitle);
				payload.alertBody(pushNotificationMsg);
*/
				// 繝励ャ繧ｷ繝･騾夂衍縺ｮ螳溯｡�
				/*
				 * for (int i = 0; i < devices.size(); i++) { service.push(devices.get(i),
				 * payload.build());
				 * 
				 * // 繧ｹ繝ｬ繝�繝峨〒繧ｳ繧ｱ繧九�ｮ繧帝亟豁｢ Thread.sleep(1000); }
				 */
/*
				service.push(devices, payload.build());

			}
			// return machine push notification
			else {

				for (int i = 0; i < devices.size(); i++) {
					PayloadBuilder payloadBuilder = APNS.newPayload();
					payloadBuilder = payloadBuilder.alertTitle(createTitle(languageCds.get(i)))
							.alertBody(createMassage(languageCds.get(i))).badge(dtcBadges.get(i))
							.customField("dtcbadge", dtcBadges.get(i));

					if (payloadBuilder.isTooLong()) {
						deviceLogger.log(Level.WARNING, "Payload is too long, shrinking it");
						payloadBuilder = payloadBuilder.shrinkBody();
					}

					service.push(devices.get(i), payloadBuilder.build());

					// 繧ｹ繝ｬ繝�繝峨〒繧ｳ繧ｱ繧九�ｮ繧帝亟豁｢
					Thread.sleep(1000);
				}

			}
			// Thread.sleep(5000);

			Thread.sleep(10000);
			// 2019/06/20 RS DucNKT Modify For ReturnMachine :End

			if (IsTestEnvironment)
				service = APNS.newService().withCert(pathCertificate, certificatePassword).withSandboxDestination()
						.build();
			else
				service = APNS.newService().withCert(pathCertificate, certificatePassword).withProductionDestination()
						.build();

			Map<String, Date> inactiveDevices = service.getInactiveDevices();
			if (inactiveDevices.size() > 0) {
				StringBuilder mes = new StringBuilder();
				mes.append("	Device tokens was sent failed:");
				for (String dv : inactiveDevices.keySet()) {
					mes.append("\n		[DEVICE TOKEN: " + dv + "]");
				}
				deviceLogger.log(Level.WARNING, mes.toString());
			}

			*/
			


			apnsClient = new ApnsClientBuilder()
			        .setApnsServer(IsTestEnvironment ? ApnsClientBuilder.DEVELOPMENT_APNS_HOST : ApnsClientBuilder.PRODUCTION_APNS_HOST)
			        .setClientCredentials(new File(pathCertificate),certificatePassword )
			        .build();
			
		
			
			final Map<String, Object> inactiveDevices = new HashMap<String, Object>();
			final ArrayList<String> sendSuccessDevices = new ArrayList<String>();
			final List<SimpleApnsPushNotification> ListPushNotification = new ArrayList<SimpleApnsPushNotification>();
			
			
			if (!isReturnMachine) {

			 	final ApnsPayloadBuilder payloadBuilder = new SimpleApnsPayloadBuilder();
			    payloadBuilder.setAlertTitle(pushNotificationTitle);
			    payloadBuilder.setAlertBody(pushNotificationMsg);

			    final String payload = payloadBuilder.build();
				    
			    for (int i = 0; i < devices.size(); i++) {
			    	 
				    final String token = TokenUtil.sanitizeTokenString(devices.get(i));			
				    ListPushNotification.add(new SimpleApnsPushNotification(token, topic, payload));
			    }
			}
			// return machine push notification
			else {

				for (int i = 0; i < devices.size(); i++) {
					final ApnsPayloadBuilder payloadBuilder = new SimpleApnsPayloadBuilder();
				    payloadBuilder.setAlertTitle(createTitle(languageCds.get(i)));
				    payloadBuilder.setAlertBody(createMassage(languageCds.get(i)));
				    payloadBuilder.setBadgeNumber(dtcBadges.get(i));
				    payloadBuilder.addCustomProperty("dtcbadge", dtcBadges.get(i));
				   
				    final String payload = payloadBuilder.build();
				    final String token = TokenUtil.sanitizeTokenString(devices.get(i));
				    ListPushNotification.add(new SimpleApnsPushNotification(token, topic, payload));
				}
			}
			
			final Semaphore semaphore = new Semaphore(1000);
			
			if(ListPushNotification.size() > 0)
			{
				int totalToken = ListPushNotification.size();
				deviceLogger.log(Level.WARNING,"[DEBUG][iOS] Send notification to "+ totalToken + " token");
				
				
				for (int i = 0; i < totalToken; i++) {
					semaphore.acquire();
					final SimpleApnsPushNotification pushNotification = ListPushNotification.get(i);
					final PushNotificationFuture<SimpleApnsPushNotification, PushNotificationResponse<SimpleApnsPushNotification>>
				    sendNotificationFuture = apnsClient.sendNotification(pushNotification);
					
					sendNotificationFuture.whenComplete((response, cause) -> {
						semaphore.release();

						if (response != null) {
							if (response.isAccepted()) {
								sendSuccessDevices.add(response.getPushNotification().getToken());
								System.out.println("[DEBUG] send success: " + response.getPushNotification().getToken());
							} else {

								inactiveDevices.put(response.getPushNotification().getToken(),
										response.getRejectionReason());
								System.out.println("[DEBUG] send failed: " + response.getPushNotification().getToken()
										+ "| reasons: " + response.getRejectionReason());

								response.getTokenInvalidationTimestamp().ifPresent(new Consumer<Instant>() {
									@Override
									public void accept(Instant timestamp) {
										System.out.println("[DEBUG] send failed: the token is invalid at " + timestamp);
									}
								});
							}
						} else {
							inactiveDevices.put(pushNotification.getToken(), "Failed to send push notification.");
							System.err.println("[DEBUG] send failed ");
							cause.printStackTrace();
						}
					});
				}
			}
			
			
			Thread.sleep(5000);
			
			deviceLogger.log(Level.INFO,"[DEBUG][iOS] permit available "+semaphore.availablePermits());
			
			if(sendSuccessDevices.size() > 0)
			{
				StringBuilder mes = new StringBuilder();
				mes.append("	"+sendSuccessDevices.size()+ " Device tokens was sent successed:");
				for (String dv : sendSuccessDevices) {
					mes.append("\n		[DEVICE TOKEN: " + dv +" ]");
				}
				deviceLogger.log(Level.INFO,mes.toString());
			}
			
			if (inactiveDevices.size() > 0) {
				StringBuilder mes = new StringBuilder();
				mes.append("	"+inactiveDevices.size()+ " Device tokens was sent failed:");
				for (String dv : inactiveDevices.keySet()) {
					mes.append("\n		[DEVICE TOKEN: " + dv +" | Error: "+inactiveDevices.get(dv)+"]");
				}
				deviceLogger.log(Level.INFO,mes.toString());
			}
			
			
			//apnsClient.close();
			
			//2021.03.23 DucNKT modify : end
			
			deviceLogger.log(Level.INFO, "	Sending a batch to iOS devices successfully.\n\n\n\n");
		} catch (Exception e) {
			deviceLogger.log(Level.WARNING,
					"	Sending a batch to iOS devices failed. No device tokens was sent. Caused by:" + e.getMessage()
							+ "\n\n\n\n");
			logger.log(Level.SEVERE, e.getMessage(), e);
		}
		finally {
			if(apnsClient != null)
				apnsClient.close();
		}

	}

	/**
	 * 譛�邨ょｮ溯｡梧律譎ゅｒ隱ｭ縺ｿ霎ｼ繧�
	 */
	private Date ReadLastSend() throws ParseException {

		DateFormat sdf = new SimpleDateFormat(DATE_TIME_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

		Date today = new Date();

		try {
			FileReader fileReader = new FileReader(LastSendTimeFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String lastestTime = bufferedReader.readLine();
			bufferedReader.close();

			return sdf.parse(lastestTime);

		} catch (FileNotFoundException ex) {
			return today;

		} catch (IOException ex) {
			return today;

		} catch (NullPointerException e) {
			return today;
		}
	}

	/**
	 * 譛�邨ょｮ溯｡梧律譎ゅｒ譖ｸ縺崎ｾｼ繧�
	 */
	private void WriteLastSend() throws IOException, ClassNotFoundException, SQLException {

		FileWriter fileWriter = new FileWriter(LastSendTimeFile);

		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

		DateFormat dfUTC = new SimpleDateFormat(DATE_TIME_FORMAT);
		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		Date today = new Date();
		dfUTC.setTimeZone(timeZone);

		bufferedWriter.write(dfUTC.format(today));
		bufferedWriter.close();
	}

	// 2019/06/20 RS DucNKT Add For ReturnMachine :Start

	/**
	 * Read message.xml
	 * 
	 * @param path
	 * @throws Exception
	 */
	private void ReadMessage(String path) throws Exception {

		File fXmlFile = new File(path);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);

		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("messages");

		if (nList == null || nList.getLength() < 1)
			throw new Exception("Message file is invalid format");
		Node root = nList.item(0);

		for (int temp = 0; temp < root.getChildNodes().getLength(); temp++) {
			Node nNode = root.getChildNodes().item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				if (nNode.getNodeName() == "english")
					MSG_EN = nNode.getTextContent();

				else if (nNode.getNodeName() == "japanese")
					MSG_JA = nNode.getTextContent();

				else if (nNode.getNodeName() == "indonesian")
					MSG_ID = nNode.getTextContent();

				else if (nNode.getNodeName() == "turkish")
					MSG_TR = nNode.getTextContent();

			}
		}

		return;
	}

	/**
	 * Read message.xml
	 * 
	 * @param path
	 * @throws Exception
	 */
	private void ReadTitle(String path) throws Exception {

		File fXmlFile = new File(path);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);

		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("titles");

		if (nList == null || nList.getLength() < 1)
			throw new Exception("titles file is invalid format");
		Node root = nList.item(0);

		for (int temp = 0; temp < root.getChildNodes().getLength(); temp++) {
			Node nNode = root.getChildNodes().item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				if (nNode.getNodeName() == "english")
					TITLE_EN = nNode.getTextContent();

				else if (nNode.getNodeName() == "japanese")
					TITLE_JA = nNode.getTextContent();

				else if (nNode.getNodeName() == "indonesian")
					TITLE_ID = nNode.getTextContent();

				else if (nNode.getNodeName() == "turkish")
					TITLE_TR = nNode.getTextContent();

			}
		}

		return;
	}

	public String createTitle(String languageCd) {

		String title = null;
		if (languageCd.toLowerCase().equals("en"))
			title = TITLE_EN;
		else if (languageCd.toLowerCase().equals("ja"))
			title = TITLE_JA;
		else if (languageCd.toLowerCase().equals("id"))
			title = TITLE_ID;
		else if (languageCd.toLowerCase().equals("tr"))
			title = TITLE_TR;
		else
			title = TITLE_EN;

		return title;
	}

	public String createMassage(String languageCd) {

		String massage = null;
		if (languageCd.toLowerCase().equals("en"))
			massage = MSG_EN;
		else if (languageCd.toLowerCase().equals("ja"))
			massage = MSG_JA;
		else if (languageCd.toLowerCase().equals("id"))
			massage = MSG_ID;
		else if (languageCd.toLowerCase().equals("tr"))
			massage = MSG_TR;
		else
			massage = MSG_EN;

		return massage;
	}
	// 2019/06/20 RS DucNKT Add For ReturnMachine :End

}