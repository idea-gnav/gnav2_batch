package com.businet.GNavApp.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.json.JSONObject;
import org.xml.sax.SAXException;

import com.businet.GNavApp.Application;
import com.businet.GNavApp.batch.HttpServices.RequestType;

public class Sw2ReportLink implements Batchlet {

	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private final String DATE_TIME_FORMAT = "yyyyMMdd HH:mm:ss:SSS";

	private String LastSendTimeFile;

	//SW2 API Environment
	public String SW2_HOST_URL;
	public String SW2_USER_ID;
	public String SYSTEM_CODE;
	public String AES_KEY;
	public String ATTACHMENT_PATH;
	public String UPLOADED_IMAGE_URL;
	public String UPLOADED_IMAGE_GNAV;

	/**
	 * Default constructor.
	 *
	 * @throws Exception
	 */
	public Sw2ReportLink() throws Exception {

		LastSendTimeFile = Application.getCurrent().getConfig().get("BatchSettings", "LastSendTimeFile", "");
		SW2_HOST_URL = Application.getCurrent().getConfig().get("Sw2Api", "Sw2HostUrl","");
		SW2_USER_ID = Application.getCurrent().getConfig().get("Sw2Api", "Sw2UserId","");
		SYSTEM_CODE = Application.getCurrent().getConfig().get("Sw2Api", "SystemCode","");
		AES_KEY = Application.getCurrent().getConfig().get("Sw2Api", "AesKey","");
		ATTACHMENT_PATH = Application.getCurrent().getConfig().get("Sw2Api", "AttachmentPath", "");
		UPLOADED_IMAGE_URL = Application.getCurrent().getConfig().get("Sw2Api", "uploadImageUrl", "");
		UPLOADED_IMAGE_GNAV = Application.getCurrent().getConfig().get("Sw2Api", "uploadImageGnav", "");

	}

	/**
	 * @throws InterruptedException
	 * @throws TransformerException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @see Batchlet#process()
	 */
	public String process() {

		try {


			// Acquire last communication date
			Date lastestDateTime = ReadLastSend();
			logger.log(Level.INFO, "Last Send Date: " + lastestDateTime);


			ExecuteQuerys eq = new ExecuteQuerys();

			//バッチ番号取得
			int batchNo = eq.findByReportIfSeq();

			int seq = batchNo;


			//1-1. 処理対象レコード取得
			List<Object[]>  targetReport = eq.findByTargetReport(lastestDateTime);

			if(targetReport != null) {

				logger.log(Level.INFO, "Link Target Report by TBL_GNAV_REPORT_INFO = " + targetReport.size());

				int reportIf = 0;
				int attachmentIf = 0;
				int errorCount = 0;
				String insErrorDoc = "''";

				for (int i = 0; i < targetReport.size(); i++) {

					Object[] obj = targetReport.get(i);
					String documentNo = obj[0].toString();
					int reportId = Integer.parseInt(obj[1].toString());
					String deviceLanguage = obj[2].toString();
					int partsCount = Integer.parseInt(obj[3].toString());
					int attachmentCount = Integer.parseInt(obj[4].toString());


					//1-2-2.レポートIF Insert
					if(i == 0) {
						//
						logger.log(Level.INFO, "reportIfInsert  documentNo:" + documentNo + ",seq:" + batchNo + ",batchNo:"+batchNo );
						int resurtReportIf =eq.insertReportIf(documentNo, batchNo, batchNo, deviceLanguage);
						// error
						if(resurtReportIf == -1) {
							//error
							errorCount++;
							logger.log(Level.INFO, "Error InsertReportIf. DcoumentNo : " + documentNo);
							insErrorDoc = insErrorDoc + ",'" + documentNo + "'";
							continue;
						}else {

							reportIf++;
						}
					}else {
						seq = eq.findByReportIfSeq();
						logger.log(Level.INFO, "reportIfInsert  documentNo:" + documentNo + ",seq:" + seq + ",batchNo:"+batchNo );
						int resurtReportIf =eq.insertReportIf(documentNo, seq, batchNo, deviceLanguage);
						if(resurtReportIf == -1) {
							//error
							errorCount++;
							int resurtUpdate = eq.updateSaveFlg(reportId,3);
							logger.log(Level.INFO, "Error InsertReportIf. DcoumentNo : " + documentNo);
							insErrorDoc = insErrorDoc + ",'" + documentNo + "'";
							continue;
						}else {
							reportIf++;
						}
					}

					//1-2-3.部品IF Insert
					if(partsCount > 0) {
						logger.log(Level.INFO, "insertPartsInfo  documentNo:" + documentNo + ",reportId:" + reportId + ",batchNo:"+batchNo );
						int resurtPartsIf =eq.insertPartsInfoIf(reportId, batchNo, documentNo);
						if(resurtPartsIf == -1) {
							//error
							errorCount++;
							int resurtUpdate = eq.updateSaveFlg(reportId,3);
							logger.log(Level.INFO, "Error insertPartsInfoIf. DcoumentNo : " + documentNo);
							insErrorDoc = insErrorDoc + ",'" + documentNo + "'";
							continue;
						}
					}

					//1-2-4.添付IF Insert
					if(attachmentCount > 0) {
						logger.log(Level.INFO, "insertAttachmentIf  documentNo:" + documentNo + ",reportId:" + reportId + ",batchNo:"+batchNo );

						List<Object[]>  attachmentList = eq.findByAttachmentFile(reportId);
						if(attachmentList != null) {
							for (int j = 0; j < attachmentList.size(); j++) {

								Object[] objAttachment = attachmentList.get(j);
								String filePath = null;
								filePath = UPLOADED_IMAGE_URL + UPLOADED_IMAGE_GNAV + objAttachment[0].toString() + "/" +  objAttachment[1].toString();
								System.out.println("filePath:"+filePath);

								// ファイル存在チェック
								URL url = new URL(filePath);
								HttpURLConnection conn = (HttpURLConnection) url.openConnection();
								conn.setRequestMethod("GET");
								conn.setInstanceFollowRedirects(false);
								conn.connect();
								int status = conn.getResponseCode();
								conn.disconnect();

								if(status != HttpURLConnection.HTTP_OK) {
									//ファイル存在しない場合、対象DBレコード削除
									logger.log(Level.INFO, "This file did not exist on the server: " + filePath );
									int deletAttachment = eq.deleteAppAttachment(reportId, objAttachment[2].toString(), SW2_USER_ID);
								}
							}
						}


						if(deviceLanguage.equals("ja")) {
							deviceLanguage = "J";
						}else {
							deviceLanguage = "E";
						}

						int resurtAttachmentIf =eq.insertAttachmentIf(reportId, documentNo,deviceLanguage);
						if(resurtAttachmentIf == -1) {
							//error
							errorCount++;
							int resurtUpdate = eq.updateSaveFlg(reportId,3);
							logger.log(Level.INFO, "Error insertAttachmentIf. DcoumentNo : " + documentNo);
							insErrorDoc = insErrorDoc + ",'" + documentNo + "'";
							continue;
						}else {
							attachmentIf++;
						}

					}

				}

				//IFテーブルInsert errorチェック
				if(errorCount > 0) {
					logger.log(Level.SEVERE ,"SW2 IF TABLE INSERT Error"
							,new Exception("SW2 IF TABLE INSERT Error: BatchNumber = " + batchNo) );
					//処理フラグ3に更新
					int resurtIfInsert = eq.updateSaveFlgAll(batchNo,99,insErrorDoc);
					// Update Last Time Send Push Before Send Push
					WriteLastSend();
					return "FAILED";

				}else {
					logger.log(Level.INFO, "3 Successful Insert to Table.");
				}



				String sw2Token = null;
				//
				if(reportIf > 0 || attachmentIf > 0) {

					//SW2API呼び出し
                	//USER_ID変換
                	String aesUserId = eq.exchangeSecurityKey(SW2_USER_ID, AES_KEY);
                	//言語CD変換
                	String aesLanguage = eq.exchangeSecurityKey("J", AES_KEY);

					//Token取得
					String getTokentUrl = SW2_HOST_URL + "/api-token-auth/app_auth";
					String paramsToken = "user_id=" + aesUserId +  "&language=" + aesLanguage;
					String rs = HttpServices.sendHttpRequest(getTokentUrl, RequestType.POST, paramsToken, null, null);


					if(rs != null && !rs.equals("unsuccess") && !rs.equals("tokenExpired") ) {

						JSONObject jObj = new JSONObject(rs);

						String token = null;
						if(jObj != null) {
							if(jObj.has("token")) {
								token = (String) jObj.get("token");
							}
						}
						sw2Token = eq.restoreSecurityKey(token, AES_KEY);

					//Token取得エラー
					}else {
						logger.log(Level.SEVERE ,"SW2 Token Get Error"
								,new Exception("SW2 Token Get Error: BatchNumber = " + batchNo) );
						//処理フラグ3に更新
						int resurtIfInsert = eq.updateSaveFlgAll(batchNo,99,null);
						// Update Last Time Send Push Before Send Push
						WriteLastSend();
						return "FAILED";
					}


					//バッチユーザ ユニークID取得
					int userUniqueId = eq.findByBatchUserId(SW2_USER_ID);

					//2-1.レポート連携バッチ呼び出しAPI(t97p01)

					if(reportIf > 0 && sw2Token != null) {

						logger.log(Level.INFO, "SW2 API[c97p01] START");

						//呼び出しURL
						String reportIfUrl = SW2_HOST_URL + "/c/c97p01/batch_run/";
						//パラメータ
						String params = "USER_ID=" + userUniqueId +  "&BATCH_NUMBER=" + batchNo;

						String rsReport = HttpServices.sendHttpRequest(reportIfUrl, RequestType.POST, params, sw2Token, null);

						//SW2API 呼び出しエラー
						if(rsReport != null && rsReport.equals("unsuccess")&& !rsReport.equals("tokenExpired")) {
							logger.log(Level.SEVERE ,"SW2 API[c97p01] Error"
									,new Exception("SW2 API[c97p01] Error: BatchNumber = " + batchNo) );

							//添付がある場合、以降定時実行で取込処理外とするためフラグ更新する
							if(attachmentIf > 0) {
								int resurtIfAttachmentUpd = eq.updateSaveAttachmentFlg(batchNo);
							}
							//処理フラグ3に更新
							int resurtIfUpd = eq.updateSaveFlgAll(batchNo,99,null);
							// Update Last Time Send Push Before Send Push
							WriteLastSend();
							return "FAILED";
						}else {

							JSONObject jObj_ = new JSONObject(rsReport);

							if (jObj_.has("result")) {
								Boolean result = (Boolean) jObj_.get("result");
								//SW2API内部エラー発生
								if(!result) {
									logger.log(Level.SEVERE ,"SW2 API[c97p01] result False"
											,new Exception("SW2 API[c97p01] result False: BatchNumber = " + batchNo) );

									//添付がある場合、以降定時実行で取込処理外とするためフラグ更新する
									if(attachmentIf > 0) {
										int resurtIfAttachmentUpd = eq.updateSaveAttachmentFlg(batchNo);
									}
									//処理フラグ3に更新
									int resurtIfInsert = eq.updateSaveFlgAll(batchNo,99,null);
									// Update Last Time Send Push Before Send Push
									WriteLastSend();
									return "FAILED";

								}else {

									logger.log(Level.INFO, "SW2 API[c97p01] Success: BatchNumber = " + batchNo);

								}
							}
						}
					}



					//2-2.添付連携バッチ呼び出しAPI(t96p01)
					if(attachmentIf > 0 && sw2Token != null) {

						logger.log(Level.INFO, "SW2 API[c96p01] START");

						//呼び出しURL
						String attachmentIfUrl = SW2_HOST_URL + "/c/c96p01/batch_run/";
						//パラメータ
						String params = "USER_ID=" + userUniqueId +  "&SAVE_TO_PATH=" + ATTACHMENT_PATH + "&SYSTEM_CODE=" + SYSTEM_CODE;

						String rsAttachment = HttpServices.sendHttpRequest(attachmentIfUrl, RequestType.POST, params, sw2Token, null);

						//SW2API 呼び出しエラー
						if(rsAttachment != null && rsAttachment.equals("unsuccess")&& !rsAttachment.equals("tokenExpired")) {
							logger.log(Level.SEVERE ,"SW2 API[c96p01] Error"
									,new Exception("SW2 API[c96p01] Error: BatchNumber = " + batchNo) );

							//添付がある場合、以降定時実行で取込処理外とするためフラグ更新する
							int resurtIfAttachmentUpd = eq.updateSaveAttachmentFlg(batchNo);
							//処理フラグ3に更新
							int resurtIfInsert = eq.updateSaveFlgAll(batchNo,99,null);
							// Update Last Time Send Push Before Send Push
							WriteLastSend();
							return "FAILED";
						}else {

							JSONObject jObj_ = new JSONObject(rsAttachment);
							Boolean result = false;
							if(jObj_.has("result"))
								result = (Boolean) jObj_.get("result");

							int data = 0;
							if(jObj_.has("data") && !jObj_.isNull("data"))
								data = Integer.parseInt(("data").toString());

							System.out.println("result:"+ result + ",data:"+ data);
							if(!result || data == 99) {
								logger.log(Level.SEVERE ,"SW2 API[c96p01] result False"
										,new Exception("SW2 API[c96p01] result False: BatchNumber = " + batchNo) );

								//添付がある場合、以降定時実行で取込処理外とするためフラグ更新する
								int resurtIfAttachmentUpd = eq.updateSaveAttachmentFlg(batchNo);
								//処理フラグ3に更新
								int resurtIfInsert = eq.updateSaveFlgAll(batchNo,99,null);
								// Update Last Time Send Push Before Send Push
								WriteLastSend();
								return "FAILED";

							}else {
								logger.log(Level.INFO, "SW2 API[c96p01] Success: BatchNumber = " + batchNo);
							}

						}
					}
				}






				//SW2連携バッチ対象外項目のInsert
				for (int i = 0; i < targetReport.size(); i++) {

					Object[] obj = targetReport.get(i);
					String documentNo = obj[0].toString();
					int reportId = Integer.parseInt(obj[1].toString());
					String deviceLanguage = obj[2].toString();
					int partsCount = Integer.parseInt(obj[3].toString());
					int attachmentCount = Integer.parseInt(obj[4].toString());
					int inspectionCount = Integer.parseInt(obj[5].toString());

					//3-2.点検結果TBL Insert
					if(inspectionCount > 0) {
						int resurtInspection = eq.insertInspectionResult(documentNo);
						if(resurtInspection == -1) {
							int resurtUpdate = eq.updateSaveFlg(reportId,99);
							logger.log(Level.INFO, "Error insertInspectionResult. DcoumentNo : " + documentNo);
							continue;
						}
					}


					//5.処理フラグ2更新
					int resurtIfInsert = eq.updateSaveFlg(reportId,100);

				}



			}else {
				logger.log(Level.INFO, "Link Target Report NOT FOUND!" );
				// Update Last Time Send Push Before Send Push
				WriteLastSend();
			}

			// Update Last Time Send Push Before Send Push
			WriteLastSend();



			return "SW2 Report Link Complete";


		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);

			return "FAILED";
		}

	}





	/**
	 *
	 * @return
	 * @throws ParseException
	 */
	private Date ReadLastSend() throws ParseException {
		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		SimpleDateFormat fmtUTC = new SimpleDateFormat(DATE_TIME_FORMAT);
		fmtUTC.setTimeZone(timeZone);
		Date today = new Date();
		String lastestTime = "";
		try {
			FileReader fileReader = new FileReader(LastSendTimeFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			lastestTime = bufferedReader.readLine();
			bufferedReader.close();

			return fmtUTC.parse(lastestTime);

		} catch (FileNotFoundException ex) {
			return today;

		} catch (IOException ex) {
			return today;

		} catch (NullPointerException e) {
			return today;
		}
	}

	/**
	 *
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private void WriteLastSend() throws IOException, ClassNotFoundException, SQLException {

		FileWriter fileWriter = new FileWriter(LastSendTimeFile);

		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

		DateFormat dfUTC = new SimpleDateFormat(DATE_TIME_FORMAT);
		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		Date today = new Date();
		dfUTC.setTimeZone(timeZone);

		bufferedWriter.write(dfUTC.format(today));
		bufferedWriter.close();
	}

}