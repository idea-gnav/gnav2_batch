package com.businet.GNavApp.batch;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

//import java.util.Base64;
import org.apache.commons.codec.binary.Base64;

import com.businet.GNavApp.Application;
import com.businet.GNavApp.db.SimpleDB;

public class ExecuteQuerys {

	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private String driverPath;
	private String driver;
	private String connectString;
	private String username;
	private String password;
	private String attachmentPath;

	public ExecuteQuerys() {

		//SW2 DB
		driverPath = Application.getCurrent().getConfig().get("Database", "DriverFile", "");
		driver = Application.getCurrent().getConfig().get("Database", "Driver", "");
		connectString = Application.getCurrent().getConfig().get("Database", "Connection", "");
		username = Application.getCurrent().getConfig().get("Database", "UserName", "");
		password = Application.getCurrent().getConfig().get("Database", "Password", "");

		attachmentPath = Application.getCurrent().getConfig().get("Sw2Api", "AttachmentPath", "");

	}



	/**
	 * findByTargetReport
	 * @return
	 */
	public List<Object[]> findByTargetReport (Date lastestDateTime)
			throws ClassNotFoundException, SQLException, MalformedURLException {

		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT tari.DOCUMENT_NO ");              // 0
		sb.append(",tari.REPORT_ID ");                      // 1
		sb.append(",tari.LANGUAGE_CD ");                    // 2
		sb.append(",NVL(parts.PARTSCOUNT, 0) ");            // 3
		sb.append(",NVL(attachment.ATTACHMENTCOUNT,0) ");   // 4
		sb.append(",NVL(inspection.INSPECTIONCOUNT,0) ");   // 5
		sb.append("FROM TBL_GNAV_REPORT_INFO tari ");
		sb.append("LEFT JOIN ( ");
		sb.append("SELECT REPORT_ID ");
		sb.append(",COUNT(REPORT_ID) PARTSCOUNT ");
		sb.append("FROM TBL_GNAV_PARTS_INFO ");
		sb.append("GROUP BY REPORT_ID ");
		sb.append(")parts ON tari.REPORT_ID = parts.REPORT_ID ");
		sb.append("LEFT JOIN ( ");
		sb.append("SELECT REPORT_ID ");
		sb.append(",COUNT(REPORT_ID) ATTACHMENTCOUNT ");
		sb.append("FROM TBL_GNAV_ATTACHMENT_FILE ");
		sb.append("WHERE IS_DELETE != '1' ");
		sb.append("GROUP BY REPORT_ID ");
		sb.append(")attachment ON tari.REPORT_ID = attachment.REPORT_ID ");
		sb.append("LEFT JOIN ( ");
		sb.append("SELECT REPORT_ID ");
		sb.append(",COUNT(REPORT_ID) INSPECTIONCOUNT ");
		sb.append("FROM TBL_GNAV_INSPECTION_RESULT ");
		sb.append("GROUP BY REPORT_ID ");
		sb.append(") inspection ON tari.REPORT_ID = inspection.REPORT_ID ");
		sb.append("WHERE tari.IS_DELETE = '0' ");
		//sb.append("AND tari.SAVE_FLG = 1 "); //遒ｺ螳壻ｿ晏ｭ�
		sb.append("AND tari.SAVE_FLG IN (1,2) ");// 1,2 (確定保存,記票)
		sb.append("AND tari.LAST_UPDATE_DATE > TO_TIMESTAMP('" + formatTimeZoneUTC(getDateTime(0, 0, 0, lastestDateTime))
				+ "', 'yyyyMMddhh24:mi:ss.FF6') ");
		sb.append("ORDER BY tari.REPORT_ID ");

		logger.log(Level.INFO, ""+sb);


		List<Object[]> data = null;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			db.close();
			return data;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}


	/**
	 * コメントID取得
	 */
	public Integer findByCommentId()
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("SELECT SEQ_TBL_COMMENT_STORAGE.NEXTVAL ");
		sb.append("FROM DUAL ");

		List<Object[]> data;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			if (data != null && data.size() == 1 && data.get(0)[0] != null)
				return Integer.parseInt(data.get(0)[0].toString());
			db.close();
			return null;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}


	/**
	 * レポートID取得
	 */
	public Integer findByReportIfSeq()
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("SELECT SEQ_TBL_REPORT_IF.NEXTVAL ");
		sb.append("FROM DUAL ");

		List<Object[]> data;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			if (data != null && data.size() == 1 && data.get(0)[0] != null)
				return Integer.parseInt(data.get(0)[0].toString());
			db.close();
			return null;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}


	/**
	 * insertCommentStorage
	 * @param commentId
	 * @param documentNo
	 * @param commentFlg
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 */
	public int insertCommentStorage(int commentId, String documentNo, int commentFlg)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("INSERT INTO TBL_COMMENT_STORAGE( ");
		sb.append("ID ");
		sb.append(",COMMENT_EN ");
		sb.append(",COMMENT_JP ");
		sb.append(",COMMENT_LOCAL ");
		sb.append(",CREATE_DATE ");
		sb.append(",CREATE_USER_ID ");
		sb.append(",CREATE_PROGRAM ");
		sb.append(",LAST_UPDATE_DATE ");
		sb.append(",LAST_UPDATE_USER_ID ");
		sb.append(",LAST_UPDATE_PROGRAM ");
		sb.append(")  ");
		sb.append("SELECT ");
		sb.append(commentId + " ");
		if(commentFlg == 1) {
			sb.append(",DEFECTS_PROBLEMS_DETAILS ");
			sb.append(",DEFECTS_PROBLEMS_DETAILS ");
			sb.append(",DEFECTS_PROBLEMS_DETAILS ");
		}else if(commentFlg == 2) {
			sb.append(",INSPECTION_COMMENT ");
			sb.append(",INSPECTION_COMMENT ");
			sb.append(",INSPECTION_COMMENT ");
		}else if(commentFlg == 3) {
			sb.append(",MATTER_NAME ");
			sb.append(",MATTER_NAME ");
			sb.append(",MATTER_NAME ");
		}
		else if(commentFlg == 4) {
			sb.append(",FAILURE_CONTENTS ");
			sb.append(",FAILURE_CONTENTS ");
			sb.append(",FAILURE_CONTENTS ");
		}
		sb.append(",SYSDATE ");
		sb.append(",APPLY_VOTE_USER_ID ");
		sb.append(",'GNav' ");
		sb.append(",SYSDATE ");
		sb.append(",APPLY_VOTE_USER_ID ");
		sb.append(",'GNav' ");
		sb.append("FROM TBL_GNAV_REPORT_INFO ");
		sb.append("WHERE DOCUMENT_NO = '" + documentNo + "' ");


		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}


	/**
	 * insertReportIf
	 * @param documentNo
	 * @param commentId1
	 * @param commentId2
	 * @param commentId3
	 * @param commentId4
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 */
	public int insertReportIf(String documentNo, int seq, int batchNo, String deviceLanguage)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);



		sb.append("INSERT INTO TBL_REPORT_IF( ");
		sb.append("ID ");                              // 1
		sb.append(",BATCH_NUMBER ");                   // 2
		sb.append(",SYSTEM_CODE ");                    // 3
		sb.append(",PROCESS_FLAG ");                   // 4
		sb.append(",PROCESS_DEFIN_CODE ");             // 5
		sb.append(",DOCUMENT_NO ");                    // 6
		sb.append(",REPORT_TYPE ");                    // 7
		sb.append(",STATUS ");                         // 8
		sb.append(",EXEC_DATE ");                      // 9
		sb.append(",APPLY_VOTE_ORG_CODE ");            // 10
		sb.append(",APPLY_VOTE_DATE ");                // 11
		sb.append(",ORG_CODE ");                       // 12
		sb.append(",INSPECTION_USER_ID ");             // 13
		sb.append(",APPLY_VOTE_USER_ID ");             // 14
		sb.append(",MACHINE_NUMBER ");                 // 15
		sb.append(",MODELS ");                         // 16
		sb.append(",HOURS ");                          // 17
		sb.append(",ADD_ENTRY ");                      // 18
		sb.append(",SERVICE_DEALER_CODE ");            // 19
		sb.append(",LOC_COUNTRY_CODE ");               // 20
		sb.append(",LOC_STATE_CODE ");                 // 21
		sb.append(",LOC_CITY ");                       // 22
		sb.append(",WARRANTY_TYPE ");                  // 23
		sb.append(",MACHINE_ARRIVAL_DATE ");           // 24
		sb.append(",DELIVERY_DATE ");                  // 25
		sb.append(",CUSTOMER_CODE ");                  // 26
		sb.append(",CUSTOMER_NAME ");                  // 27
		sb.append(",PRODUCT_FACTORY ");                // 28
		sb.append(",MACHINE_TYPE ");                   // 29
		sb.append(",USER_AUTHORITY ");                 // 30
		sb.append(",APPL_IF_ID ");                     // 31
		sb.append(",MACHINE_ATTACHMENT_CODE ");        // 32
		sb.append(",ATTACHMENT_COMMENT ");             // 33
		sb.append(",LABOR_SITE_INDUSTRY_CODE ");       // 34
		sb.append(",LABOR_PLACE_COMMENT ");            // 35
		sb.append(",REF_FROM_DOCUMENT_NO ");           // 36
		sb.append(",INSPECTION_TIMES ");               // 37
		sb.append(",RESULT_CODE ");                    // 38
		sb.append(",DEFECTS_PROBLEMS_DETAILS_JP ");    // 39
		sb.append(",DEFECTS_PROBLEMS_DETAILS_EN ");    // 40
		sb.append(",DEFECTS_PROBLEMS_DETAILS_LOCAL "); // 41
		sb.append(",COMMENT_JP ");                     // 42
		sb.append(",COMMENT_EN ");                     // 43
		sb.append(",COMMENT_LOCAL ");                  // 44
		sb.append(",TECHNICAL_TYPE ");                 // 45-1
		sb.append(",CLAIM_TYPE ");                     // 45-2
		sb.append(",CAMPAIGN_NO_NO ");                 // 46
		sb.append(",PRIME_PARTS_NUMBER ");             // 47
		sb.append(",PRIME_PARTS_DESCRIPTION ");        // 48
		sb.append(",MATTER_NAME_JP ");                 // 49
		sb.append(",MATTER_NAME_EN ");                 // 50
		sb.append(",MATTER_NAME_LOCAL ");              // 51
		sb.append(",FAILURE_CONTENTS_JP ");            // 52
		sb.append(",FAILURE_CONTENTS_EN ");            // 53
		sb.append(",FAILURE_CONTENTS_LOCAL ");         // 54
		sb.append(",FAILURE_OCCUR_DATE ");             // 55
		sb.append(",REPAIR_DATE ");                    // 56
		sb.append(",MUST_ANSWER ");                    // 57
		sb.append(",SITE_ARRIVAL_TIME ");              // 58
		sb.append(",PARTS_ARRANGE_WITH_OR_WITHOUT ");  // 59
		sb.append(",PARTS_EXCHANGE ");                 // 60
		sb.append(",MACHINE_STATE ");                  // 61
		sb.append(",TP_NO_1 ");                        // 62
		sb.append(",AREA_CODE_1 ");                    // 63
		sb.append(",PARTS_CODE_1 ");                   // 64
		sb.append(",CONDITION_1 ");                    // 65
		sb.append(",ATTACHMENT_CRACK_PLACE_1 ");       // 66
		sb.append(",TP_NO_2 ");                        // 67
		sb.append(",AREA_CODE_2 ");                    // 68
		sb.append(",PARTS_CODE_2 ");                   // 69
		sb.append(",CONDITION_CODE_2 ");               // 70
		sb.append(",ATTACHMENT_CRACK_PLACE_2 ");       // 71
		sb.append(",TP_NO_3 ");                        // 72
		sb.append(",AREA_CODE_3 ");                    // 73
		sb.append(",PARTS_CODE_3 ");                   // 74
		sb.append(",CONDITION_CODE_3 ");               // 75
		sb.append(",ATTACHMENT_CRACK_PLACE_3 ");       // 76
		sb.append(",ADD_ITEM_TYPE ");                  // 77
		sb.append(",ADD_ATTACHMENT ");                 // 78
		sb.append(",SERIAL_NO ");                      // 79
		sb.append(",ADD_PARTS_NUMBER ");               // 80
		sb.append(",ADD_PARTS_NAME ");                 // 81
		sb.append(",PARTS_ADD_COMMENT ");              // 82-1
		sb.append(",CHANGE_WARRANTY_TYPE ");           // 82-2
		sb.append(",TBL_WORKFLOW_NOTE ");              // 83
		sb.append(",CREATE_DATE ");                    // 84
		sb.append(",CREATE_USER_ID ");                 // 85
		sb.append(",CREATE_PROGRAM ");                 // 86
		sb.append(",LAST_UPDATE_DATE ");               // 87
		sb.append(",LAST_UPDATE_USER_ID ");            // 88
		sb.append(",LAST_UPDATE_PROGRAM ");            // 89
		sb.append(",DEALER_CLAIM_NO ");                // 90
		sb.append(",ADD_DATE ");                       // 91
		sb.append(",CONTACT_DATE ");                   // 92
		sb.append(") ");
		sb.append("SELECT ");
		sb.append(seq + ", ");                         // 1
		sb.append(batchNo + ", ");                     // 2
		sb.append("'GNAV', ");                         // 3
		sb.append("'0', ");                            // 4
		sb.append("tari.PROCESS_DEFIN_CODE, ");        // 5
		sb.append("tari.DOCUMENT_NO, ");               // 6
		sb.append("tari.REPORT_TYPE, ");               // 7
		sb.append("CASE tari.SAVE_FLG ");              //8  確定保存⇒'00'、記票⇒'01'
		sb.append("WHEN 1 THEN '00' ");
		sb.append("WHEN 2 THEN '01' ");
		sb.append("ELSE null ");
		sb.append("END, ");
		sb.append("tari.EXEC_DATE, ");                 // 9
		sb.append("tari.APPLY_VOTE_ORG_CODE, ");       // 10
		sb.append("tari.APPLY_VOTE_DATE, ");           // 11
		sb.append("tari.APPLY_VOTE_ORG_CODE, ");       // 12
		sb.append("tari.INSPECTION_USER_ID, ");        // 13
		sb.append("tari.APPLY_VOTE_USER_ID, ");        // 14
		sb.append("tari.MACHINE_NUMBER, ");            // 15
		sb.append("tari.MODELS, ");                    // 16
		sb.append("tari.HOURS, ");                     // 17
		sb.append("tari.ADD_ENTRY, ");                 // 18
		sb.append("tari.DEALER_CODE, ");               // 19
		sb.append("tari.LOC_COUNTRY_CODE, ");          // 20
		sb.append("tari.LOC_STATE_CODE, ");            // 21
		sb.append("tari.LOC_CITY, ");                  // 22
		sb.append("CASE tari.REPORT_TYPE  ");
		sb.append("WHEN n'090' THEN mwt2.WARRANTY_TYPE_CODE ");
		sb.append("WHEN n'050' THEN mrit.SECURITY_TYPE_CODE ");
		sb.append("ELSE mwt.WARRANTY_TYPE_CODE ");
		sb.append("END,  ");
		sb.append("tari.MACHINE_ARRIVAL_DATE, ");      // 24
		sb.append("tari.DELIVERY_DATE, ");             // 25
		sb.append("tari.CLIENT_CODE, ");               // 26
		sb.append("mc.CLIENT_NAME, ");                 // 27
		sb.append("mm.PRODUCT_FACTORY_CODE, ");        // 28
		sb.append("mm.MACHINE_TYPE, ");                // 29
		sb.append("tari.WF_AUTHORITY, ");                // 30
		sb.append("tari.APPL_IF_ID, ");                 // 31
		sb.append("tari.MACHINE_ATTACHMENT_CODE, ");   // 32
		sb.append("tari.ATTACHMENT_COMMENT, ");        // 33
		sb.append("tari.LABOR_SITE_INDUSTRY_CODE, ");  // 34
		sb.append("tari.LABOR_PLACE_COMMENT, ");       // 35
		sb.append("tari.REF_FROM_DOCUMENT_NO, ");      // 36
		sb.append("mrit.INSPECTION_TIMES, ");
		sb.append("tari.RESULT_CODE, ");               // 38
		if(deviceLanguage.equals("ja")) {
			sb.append("tari.DEFECTS_PROBLEMS_DETAILS, ");  // 39
			sb.append("null, ");                           // 40
			sb.append("null, ");                           // 41
			sb.append("tari.INSPECTION_COMMENT, ");        // 42
			sb.append("null, ");                           // 43
			sb.append("null, ");                           // 44
		}else if(deviceLanguage.equals("id")) {
			sb.append("null, ");                           // 39
			sb.append("null, ");                           // 40
			sb.append("tari.DEFECTS_PROBLEMS_DETAILS, ");  // 41
			sb.append("null, ");                           // 42
			sb.append("null, ");                           // 43
			sb.append("tari.INSPECTION_COMMENT, ");        // 44
		}else {
			sb.append("null, ");                           // 39
			sb.append("tari.DEFECTS_PROBLEMS_DETAILS, ");  // 40
			sb.append("null, ");                           // 41
			sb.append("null, ");                           // 42
			sb.append("tari.INSPECTION_COMMENT, ");        // 43
			sb.append("null, ");                           // 44
		}
		sb.append("CASE tari.REPORT_TYPE ");
		sb.append("WHEN n'060' THEN tari.TECHNICAL_TYPE ");
		sb.append("ELSE null ");
		sb.append("END, ");                                // 45-1
		sb.append("CASE tari.REPORT_TYPE ");
		sb.append("WHEN n'080' THEN tari.TECHNICAL_TYPE ");
		sb.append("ELSE null ");
		sb.append("END, ");                                // 45-2
		sb.append("tari.CAMPAIGN_NO, ");                // 46
		sb.append("tari.PRIME_PARTS_NUMBER, ");            // 47
		sb.append("tari.PRIME_PARTS_NAME, ");              // 48
		if(deviceLanguage.equals("ja")) {
			sb.append("tari.MATTER_NAME, ");               // 49
			sb.append("null, ");                           // 50
			sb.append("null, ");                           // 51
			sb.append("tari.FAILURE_CONTENTS, ");          // 52
			sb.append("null, ");                           // 53
			sb.append("null, ");                           // 54
		}else if(deviceLanguage.equals("id")) {
			sb.append("null, ");                           // 49
			sb.append("null, ");                           // 50
			sb.append("tari.MATTER_NAME, ");               // 51
			sb.append("null, ");                           // 52
			sb.append("null, ");                           // 53
			sb.append("tari.FAILURE_CONTENTS, ");          // 54
		}else {
			sb.append("null, ");                           // 49
			sb.append("tari.MATTER_NAME, ");               // 50
			sb.append("null, ");                           // 51
			sb.append("null, ");                           // 52
			sb.append("tari.FAILURE_CONTENTS, ");          // 53
			sb.append("null, ");                           // 54
		}
		sb.append("tari.FAILURE_OCCUR_DATE, ");            // 55
		sb.append("tari.REPAIR_DATE, ");                   // 56
		sb.append("tari.MUST_ANSWER, ");                   // 57
		sb.append("tari.SITE_ARRIVAL_TIME, ");             // 58
		sb.append("tari.PARTS_ARRANGE_WITH_OR_WITHOUT, "); // 59
		sb.append("tari.PARTS_EXCHANGE, ");                // 60
		sb.append("tari.MACHINE_STATE, ");                 // 61
		sb.append("(SELECT TPNO ");
		sb.append("FROM MST_LABOR_CODE ");
		sb.append("WHERE ID = tari.TPNOID1), ");           // 62
		sb.append("tari.AREA_CODE_1, ");                   // 63
		sb.append("tari.PARTS_CODE_1, ");                  // 64
		sb.append("tari.CONDITION_CODE_1, ");              // 65
		sb.append("tari.ATTACHMENT_CRACK_PLACE_1, ");      // 66
		sb.append("(SELECT TPNO ");
		sb.append("FROM MST_LABOR_CODE ");
		sb.append("WHERE ID = tari.TPNOID2), ");           // 67
		sb.append("tari.AREA_CODE_2, ");                   // 68
		sb.append("tari.PARTS_CODE_2, ");                  // 69
		sb.append("tari.CONDITION_CODE_2, ");              // 70
		sb.append("tari.ATTACHMENT_CRACK_PLACE_2, ");      // 71
		sb.append("(SELECT TPNO ");
		sb.append("FROM MST_LABOR_CODE ");
		sb.append("WHERE ID = tari.TPNOID3), ");           // 72
		sb.append("tari.AREA_CODE_3, ");                   // 73
		sb.append("tari.PARTS_CODE_3, ");                  // 74
		sb.append("tari.CONDITION_CODE_3, ");              // 75
		sb.append("tari.ATTACHMENT_CRACK_PLACE_3, ");      // 76
		sb.append("tari.ADD_ITEM_TYPE, ");                 // 77
		sb.append("tari.ADD_ATTACHMENT, ");                // 78
		sb.append("tari.SERIAL_NO, ");                     // 79
		sb.append("tari.ADD_PARTS_NUMBER, ");              // 80
		sb.append("tari.ADD_PARTS_NAME, ");                // 81
		sb.append("tari.PARTS_ADD_COMMENT, ");             // 82-1
		sb.append("tari.CHANGE_WARRANTY_TYPE, ");          // 82-3
		sb.append("tari.WF_COMMENT, ");             	   // 83
		sb.append("tari.CREATE_DATE, ");                   // 84
		sb.append("tari.CREATE_USER_ID, ");                // 85
		sb.append("tari.CREATE_PROGRAM, ");                // 86
		sb.append("tari.LAST_UPDATE_DATE, ");              // 87
		sb.append("tari.LAST_UPDATE_USER_ID, ");           // 88
		sb.append("tari.LAST_UPDATE_PROGRAM, ");           // 89
		sb.append("tari.DEALER_CLAIM_NO, ");               // 90
		sb.append("tari.ADD_DATE, ");                      // 91
		sb.append("tari.CONTACT_DATE ");                   // 92
		sb.append("FROM TBL_GNAV_REPORT_INFO tari ");
		sb.append("LEFT JOIN MST_CUSTOMER mc ");
		sb.append("ON ( tari.CLIENT_CODE = mc.CLIENT_CODE ");
		sb.append("AND mc.IS_DELETE = '0') ");
		sb.append("LEFT JOIN MST_MACHINE mm ");
		sb.append("ON (tari.MACHINE_NUMBER = mm.MACHINE_NUMBER ");
		sb.append("AND mm.IS_DELETE = '0') ");
		sb.append("LEFT JOIN MST_USER mu ");
		sb.append("ON (tari.APPLY_VOTE_USER_ID = mu.ID) ");
		sb.append("LEFT JOIN MST_REGULARLY_INSPECTION_TYPE mrit ");
		sb.append("ON (tari.INSPECTION_TYPE_ID = mrit.ID) ");
		sb.append("LEFT JOIN MST_WARRANTY_TYPE mwt ");
		sb.append("ON (tari.WARRANTY_TYPE_ID = mwt.id) ");
		sb.append("LEFT JOIN MST_CODE_VALUE_V mcvv14 ");
		sb.append("ON (mcvv14.CODE_TYPE = '0014' ");
		sb.append("AND  tari.ADD_PARTS_NUMBER = mcvv14.CODE ");
		if(deviceLanguage.equals("ja")) {
			sb.append("AND mcvv14.LANGUAGE = 'J'  )  ");
		}else {
			sb.append("AND mcvv14.LANGUAGE = 'E'  )  ");
		}
		sb.append("LEFT JOIN MST_WARRANTY_TYPE mwt2 ");
		sb.append("ON (tari.CHANGE_WARRANTY_TYPE = mwt2.id) ");
		sb.append("WHERE tari.IS_DELETE = '0' ");
		sb.append("AND tari.DOCUMENT_NO = '"+ documentNo + "' ");

		System.out.println(sb);

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
		//	throw ex;
			int result = -1;
			return result;
		}
	}


	/**
	 * insertPartsInfoIf
	 * @param reportId
	 * @param batchNo
	 * @param documentNo
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 */
	public int insertPartsInfoIf(int reportId, int batchNo, String documentNo)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("INSERT INTO TBL_PARTS_INFORMATION_IF( ");
		sb.append("ID ");                                   // 1
		sb.append(",BATCH_NUMBER ");                        // 2
		sb.append(",SYSTEM_CODE ");                         // 3
		sb.append(",PROCESS_FLAG ");                        // 4
		sb.append(",DOCUMENT_NO ");                         // 5
		sb.append(",LINE_NO ");                             // 6
		sb.append(",PARTS_NUMBER ");                        // 7
		sb.append(",PARTS_NAME ");                          // 8
		sb.append(",ENTRY_QTY ");                           // 9
		sb.append(",FAILURE_PRODUCT_SERIAL_NO ");           // 10
		sb.append(",BRAND_NEW_SERIAL_NO ");                 // 11
		sb.append(",ACCEPT_NO ");                           // 12
		sb.append(",IF_CLAIM_NO ");                         // 13
		sb.append(",CREATE_DATE ");                         // 14
		sb.append(",CREATE_USER_ID ");                      // 15
		sb.append(",CREATE_PROGRAM ");                      // 16
		sb.append(",LAST_UPDATE_DATE ");                    // 17
		sb.append(",LAST_UPDATE_USER_ID ");                 // 18
		sb.append(",LAST_UPDATE_PROGRAM ");                 // 19
		sb.append(",SHIPMENT_DATE ");               		// 20
		sb.append(",ORDER_DATE ");                 			// 21
		sb.append(",ARRIVAL_DATE ");                 		// 22
		sb.append(") ");
		sb.append("SELECT ");
		sb.append("SEQ_TBL_PARTS_INFORMATION_IF.NEXTVAL, ");  // 1
		sb.append(batchNo +", ");                             // 2
		sb.append("'GNAV', ");                                // 3
		sb.append("'0', ");                                   // 4
		sb.append("'" + documentNo + "', ");                  // 5
		sb.append("tapi.LINE_NO, ");                          // 6
		sb.append("tapi.PARTS_NUMBER, ");                     // 7
		sb.append("tapi.PARTS_NAME, ");                       // 8
		sb.append("tapi.ENTRY_QTY, ");                        // 9
		sb.append("tapi.FAILURE_PRODUCT_SERIAL_NO, ");        // 10
		sb.append("tapi.BRAND_NEW_SERIAL_NO, ");              // 11
		sb.append("tapi.ACCEPT_NO, ");                        // 12
		sb.append("'G@NAV_" + documentNo + "', ");           // 13
		sb.append("tapi.CREATE_DATE, ");                      // 14
		sb.append("tapi.CREATE_USER_ID, ");                   // 15
		sb.append("tapi.CREATE_PROGRAM, ");                   // 16
		sb.append("tapi.LAST_UPDATE_DATE, ");                 // 17
		sb.append("tapi.LAST_UPDATE_USER_ID, ");              // 18
		sb.append("tapi.LAST_UPDATE_PROGRAM, ");               // 19
		sb.append("tapi.SHIPMENT_DATE, ");               	  // 20
		sb.append("tapi.ORDER_DATE, ");               	 	  // 21
		sb.append("tapi.ARRIVAL_DATE ");               	     // 22

		sb.append("FROM TBL_GNAV_PARTS_INFO tapi ");
		sb.append("LEFT JOIN TBL_GNAV_REPORT_INFO tari ON ");
		sb.append("tapi.REPORT_ID = tari.REPORT_ID ");

		sb.append("WHERE tapi.REPORT_ID = " + reportId + " ");

		System.out.println(sb);

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
		//	throw ex;
			int result = -1;
			return result;
		}
	}


	/**
	 * findByAttachmentFile
	 * @return
	 */
	public List<Object[]> findByAttachmentFile (int reportId)
			throws ClassNotFoundException, SQLException, MalformedURLException {

		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT tari.DOCUMENT_NO,  ");
		sb.append("taaf.FILE_NAME, ");
		sb.append("taaf.FILE_NO ");
		sb.append("FROM TBL_GNAV_ATTACHMENT_FILE taaf ");
		sb.append("LEFT JOIN TBL_GNAV_REPORT_INFO tari ON taaf.REPORT_ID = tari.REPORT_ID ");
		sb.append("WHERE taaf.REPORT_ID = " + reportId + " ");



		List<Object[]> data = null;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			db.close();
			return data;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}

	/**
	 * deleteAppAttachment
	 * @param reportId,fileNo
	 * @param documentNo
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 */
	public int deleteAppAttachment(int reportId, String fileNo, String batchUser)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("UPDATE TBL_GNAV_ATTACHMENT_FILE ");
		sb.append("SET IS_DELETE = '1', ");
		sb.append("LAST_UPDATE_DATE = SYSDATE, ");
		sb.append("LAST_UPDATE_USER_ID = ( ");
		sb.append("SELECT ID ");
		sb.append("FROM MST_USER ");
		sb.append("WHERE USER_ACCOUNT = '"+ batchUser +"' ");
		sb.append("), ");
		sb.append("LAST_UPDATE_PROGRAM = 'REPORT_BATCH' ");
		sb.append("WHERE REPORT_ID = " + reportId + " ");
		sb.append("AND FILE_NO = '"+ fileNo +"' ");


		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
		//	throw ex;
			int result = -1;
			return result;
		}

	}



	/**
	 * insertAttachmentIf
	 * @param reportId
	 * @param documentNo
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 */
	public int insertAttachmentIf(int reportId, String documentNo, String language)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("INSERT INTO TBL_ATTACHMENT_IF( ");
		sb.append("ID ");
		sb.append(",SYSTEM_CODE ");
		sb.append(",SAVE_PATH ");
		sb.append(",GET_FLAG ");
		sb.append(",DOCUMENT_NO ");
		sb.append(",CREATE_DATE ");
		sb.append(",CREATE_USER_ID ");
		sb.append(",CREATE_PROGRAM ");
		sb.append(",LAST_UPDATE_DATE ");
		sb.append(",LAST_UPDATE_USER_ID ");
		sb.append(",LAST_UPDATE_PROGRAM ");
		sb.append(",TITLE_CODE ");
		sb.append(",TITLE ");
		sb.append(",NOTE ");
		sb.append(",PRINT_FLAG ");
		sb.append(",CASE_NO ");
		sb.append(",FORMAT ");
		sb.append(") ");
		sb.append("SELECT ");
		sb.append("SEQ_TBL_ATTACHMENT_IF.NEXTVAL, ");
		sb.append("'GNAV', ");
		sb.append("'" + attachmentPath + "/" + documentNo +"/' || FILE_NAME, ");
		sb.append("'0', ");
		sb.append("'" + documentNo + "', ");
		sb.append("CREATE_DATE, ");
		sb.append("CREATE_USER_ID, ");
		sb.append("CREATE_PROGRAM, ");
		sb.append("LAST_UPDATE_DATE, ");
		sb.append("LAST_UPDATE_USER_ID, ");
		sb.append("LAST_UPDATE_PROGRAM, ");

		//DucNKT added ↓↓↓↓↓
		sb.append("A.TITLECODE, ");
		sb.append("A.TITLE, ");
		sb.append("CASE WHEN A.NOTE IS NULL AND TITLECODE = '100' THEN A.TITLE  ");
		sb.append("ELSE A.NOTE END , ");
		sb.append("'1', ");
		sb.append("'G@NAV_" + documentNo + "', ");
		//DucNKT added ↑↑↑↑↑
		sb.append("SUBSTR(FILE_NAME,-3) ");

		sb.append("FROM TBL_GNAV_ATTACHMENT_FILE taaf ");
		sb.append("LEFT JOIN ");
		sb.append("( SELECT ");
		sb.append("taaf.FILE_NO, ");//DucNKT added
		sb.append("taaf.REPORT_ID, ");//DucNKT added

		sb.append("CASE ");
		sb.append("WHEN (tari.REPORT_TYPE IN ('010','020','030','060','080')) ");
		sb.append("THEN ( ");
		sb.append("CASE ");
		sb.append("WHEN taaf.FILE_NO = '01' THEN '10' ");               // 機械全体
		sb.append("WHEN taaf.FILE_NO IN ('02','04','06') THEN '20' ");  // 不具合部位
		sb.append("WHEN taaf.FILE_NO IN ('03','05','07') THEN '30' ");  // 不具合部品情報
		sb.append("WHEN taaf.FILE_NO IN ('08','09','10') THEN '100' "); // その他
		//TODO:動画のコード変換⇒SW2汎用マスタのコード値決まり次第
		sb.append("WHEN taaf.FILE_NO = '11' THEN '110' ");              // 動画
		sb.append("WHEN taaf.FILE_NO = '12' THEN '90' ");               // お客様サイン
		sb.append("ELSE '100' ");                                       // その他画像
		sb.append("END ) ");

		sb.append("WHEN (tari.report_type = '040') ");
		sb.append("THEN ( ");
		sb.append("CASE ");
		sb.append("WHEN taaf.FILE_NO = '01' THEN '10' ");               // 機械全体
		sb.append("WHEN taaf.FILE_NO IN ('02','04','06') THEN '20' ");  // 不具合部位
		sb.append("WHEN taaf.FILE_NO IN ('03','05','07') THEN '30' ");  // 不具合部品詳細
		sb.append("WHEN taaf.FILE_NO IN ('08') THEN '70' ");            // 契約書
		sb.append("WHEN taaf.FILE_NO IN ('09','10') THEN '100' ");      // その他
		//TODO:動画のコード変換⇒SW2汎用マスタのコード値決まり次第
		sb.append("WHEN taaf.FILE_NO = '11' THEN '110' ");              // 動画
		sb.append("WHEN taaf.FILE_NO = '12' THEN '90' ");               // お客様サイン
		sb.append("ELSE '100' ");                                       // その他画像
		sb.append("END ) ");

		sb.append("WHEN (tari.report_type IN ('050','055')) ");
		sb.append("THEN ( ");
		sb.append("CASE ");
		sb.append("WHEN taaf.FILE_NO = '01' THEN '10' ");               // 機械全体
		sb.append("WHEN taaf.FILE_NO = '02' THEN '40' ");               // HR
		sb.append("WHEN taaf.FILE_NO IN ('03','05','07') THEN '20' ");  // 不具合部品
		sb.append("WHEN taaf.FILE_NO IN ('04','06','08') THEN '30' ");  // 不具合部品詳細
		sb.append("WHEN taaf.FILE_NO IN ('09','10') THEN '100' ");      // その他
		//TODO:動画のコード変換⇒SW2汎用マスタのコード値決まり次第
		sb.append("WHEN taaf.FILE_NO = '11' THEN '110' ");              // 動画
		sb.append("WHEN taaf.FILE_NO = '12' THEN '90' ");               // お客様サイン
		sb.append("ELSE '100' ");                                       // その他画像
		sb.append("END ) ");

		sb.append("WHEN (tari.report_type = '090') ");
		sb.append("THEN ( ");
		sb.append("CASE ");
		//TODO:動画のコード変換⇒SW2汎用マスタのコード値決まり次第
		sb.append("WHEN taaf.FILE_NO = '11' THEN '110' ");              // 動画
		sb.append("WHEN taaf.FILE_NO = '12' THEN '90' ");               // お客様サイン
		sb.append("ELSE '100' ");                                       // その他,その他画像
		sb.append("END ) ");

		sb.append("WHEN (tari.report_type = '100') ");
		sb.append("THEN ( ");
		sb.append("CASE ");
		sb.append("WHEN taaf.FILE_NO = '01' THEN '60' ");               // 送付部品写真
		sb.append("WHEN taaf.FILE_NO = '02' THEN '50' ");               // 送り状伝票
		//TODO:動画のコード変換⇒SW2汎用マスタのコード値決まり次第
		sb.append("WHEN taaf.FILE_NO = '11' THEN '110' ");              // 動画
		sb.append("WHEN taaf.FILE_NO = '12' THEN '90' ");               // お客様サイン
		sb.append("ELSE '100' ");                                       // その他,その他画像
		sb.append("END ) ");
		sb.append("END AS titleCode, ");

		sb.append("mcvv.CODE_VALUE_1 AS title, ");
		sb.append("taaf.NOTE AS note ");
		sb.append("FROM TBL_GNAV_ATTACHMENT_FILE taaf ");
		sb.append("JOIN TBL_GNAV_REPORT_INFO tari ON taaf.REPORT_ID = tari.REPORT_ID ");
		sb.append(",MST_CODE_VALUE_V mcvv ");
		sb.append("WHERE ");
		sb.append("taaf.REPORT_ID = " + reportId + " ");
		sb.append("AND taaf.IS_DELETE != '1'  ");;//DucNKT added
		sb.append("AND mcvv.CODE_TYPE = '0130' ");
		sb.append("AND mcvv.language = '" + language + "' ");
		sb.append("AND mcvv.CODE_VALUE_2 = tari.REPORT_TYPE ");
		sb.append("AND mcvv.CODE_VALUE_3 = taaf.FILE_NO ");
		//sb.append("and SUBSTR(TBL_ATTACHMENT_FILE.ORIGINAL_FILE_NAME,-6,2) = taaf.FILE_NO  ");

		sb.append("UNION ");
		sb.append("SELECT ");
		sb.append("taaf.FILE_NO, ");//DucNKT added
		sb.append("taaf.REPORT_ID, ");//DucNKT added
		sb.append("'90' AS titleCode, ");
		sb.append("null AS title, ");
		sb.append("taaf.NOTE AS note ");
		sb.append("FROM TBL_GNAV_ATTACHMENT_FILE taaf ");
		sb.append("WHERE ");
		sb.append("taaf.REPORT_ID = " + reportId + " ");
		sb.append("AND taaf.IS_DELETE != '1'  ");//DucNKT added
		sb.append("AND FILE_NO = 12 "); //署名
		//sb.append("and SUBSTR(TBL_ATTACHMENT_FILE.ORIGINAL_FILE_NAME,-6,2) = FILE_NO  ");

		sb.append(") A ");
		sb.append("ON taaf.REPORT_ID = A.REPORT_ID ");
		sb.append("AND taaf.FILE_NO = A.FILE_NO ");
		sb.append("WHERE taaf.REPORT_ID = " + reportId + " ");
		sb.append("AND taaf.IS_DELETE != '1'  ");

		System.out.println(sb);

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
		//	throw ex;
			int result = -1;
			return result;
		}

	}

	/**
	 * insertInspectionResult
	 * @param documentNo
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 */
	public int insertInspectionResult(String documentNo)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("INSERT INTO TBL_INSPECTION_RESULT ");
		sb.append("SELECT ");
		sb.append("SEQ_TBL_INSPECTION_RESULT.NEXTVAL, ");
		sb.append("tp.ID, ");
		sb.append("tp.DOCUMENT_NO, ");
		sb.append("tair.REPORT_TYPE, ");
		sb.append("tair.INSPECTION_ITEM_ID, ");
		sb.append("tair.INSPECTION_RESULT, ");
		sb.append("tair.ORG_ID, ");
		sb.append("tair.CREATE_DATE, ");
		sb.append("tair.CREATE_USER_ID, ");
		sb.append("tair.CREATE_PROGRAM, ");
		sb.append("tair.LAST_UPDATE_DATE, ");
		sb.append("tair.LAST_UPDATE_USER_ID, ");
		sb.append("tair.LAST_UPDATE_PROGRAM ");
		sb.append("FROM TBL_GNAV_INSPECTION_RESULT tair ");
		sb.append("JOIN TBL_GNAV_REPORT_INFO tarp ON tair.REPORT_ID = tarp.REPORT_ID ");
		sb.append("JOIN TBL_PROCESS tp ON tarp.DOCUMENT_NO = tp.DOCUMENT_NO ");
		sb.append("WHERE tp.DOCUMENT_NO = '" + documentNo + "' ");

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
		//	throw ex;
			int result = -1;
			return result;
		}

	}


	/**
	 * updatePartsInfo
	 * @param reportId
	 * @param documentNo
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 */
	public int updatePartsInfo(Integer reportId, String documentNo)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("UPDATE TBL_PARTS_INFO ");
		sb.append("SET ( ");
		sb.append("SHIPMENT_DATE, ");
		sb.append("ORDER_DATE, ");
		sb.append("ARRIVAL_DATE ");
		sb.append(" ) = ( ");
		sb.append("SELECT ");
		sb.append("tapi.SHIPMENT_DATE, ");
		sb.append("tapi.ORDER_DATE, ");
		sb.append("tapi.ARRIVAL_DATE ");
		sb.append("FROM TBL_GNAV_PARTS_INFO tapi ");
		sb.append("WHERE ");
		sb.append("TBL_PARTS_INFO.LINE_NO = tapi.LINE_NO ");
		sb.append("AND tapi.REPORT_ID = " + reportId + ") ");
		sb.append("WHERE ");
		sb.append("DOCUMENT_NO = '" + documentNo + "' ");


		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
		//	throw ex;
			int result = -1;
			return result;
		}

	}


	/**
	 * updateAttachmentFile
	 * @param reportId
	 * @param documentNo
	 * @param language
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 */
	public int updateAttachmentFile(Integer reportId, String documentNo, String language)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("UPDATE TBL_ATTACHMENT_FILE ");
		sb.append("SET ( ");
		sb.append("TITLE_CODE, ");
		sb.append("TITLE, ");
		sb.append("NOTE ");
		sb.append(") = ( ");
		sb.append("SELECT ");

		sb.append("CASE ");

		sb.append("WHEN (tari.REPORT_TYPE IN ('010','020','030','060','080')) ");
		sb.append("THEN ( ");
		sb.append("CASE ");
		sb.append("WHEN taaf.FILE_NO = '01' THEN '10' ");               // 機械全体
		sb.append("WHEN taaf.FILE_NO IN ('02','04','06') THEN '20' ");  // 不具合部位
		sb.append("WHEN taaf.FILE_NO IN ('03','05','07') THEN '30' ");  // 不具合部品情報
		sb.append("WHEN taaf.FILE_NO IN ('08','09','10') THEN '100' "); // その他
		//TODO:動画のコード変換⇒SW2汎用マスタのコード値決まり次第
		sb.append("WHEN taaf.FILE_NO = '11' THEN '110' ");              // 動画
		sb.append("WHEN taaf.FILE_NO = '12' THEN '90' ");               // お客様サイン
		sb.append("ELSE '100' ");                                       // その他画像
		sb.append("END ) ");

		sb.append("WHEN (tari.report_type = '040') ");
		sb.append("THEN ( ");
		sb.append("CASE ");
		sb.append("WHEN taaf.FILE_NO = '01' THEN '10' ");               // 機械全体
		sb.append("WHEN taaf.FILE_NO IN ('02','04','06') THEN '20' ");  // 不具合部位
		sb.append("WHEN taaf.FILE_NO IN ('03','05','07') THEN '30' ");  // 不具合部品詳細
		sb.append("WHEN taaf.FILE_NO IN ('08') THEN '70' ");            // 契約書
		sb.append("WHEN taaf.FILE_NO IN ('09','10') THEN '100' ");      // その他
		//TODO:動画のコード変換⇒SW2汎用マスタのコード値決まり次第
		sb.append("WHEN taaf.FILE_NO = '11' THEN '110' ");              // 動画
		sb.append("WHEN taaf.FILE_NO = '12' THEN '90' ");               // お客様サイン
		sb.append("ELSE '100' ");                                       // その他画像
		sb.append("END ) ");

		sb.append("WHEN (tari.report_type IN ('050','055')) ");
		sb.append("THEN ( ");
		sb.append("CASE ");
		sb.append("WHEN taaf.FILE_NO = '01' THEN '10' ");               // 機械全体
		sb.append("WHEN taaf.FILE_NO = '02' THEN '40' ");               // HR
		sb.append("WHEN taaf.FILE_NO IN ('03','05','07') THEN '20' ");  // 不具合部品
		sb.append("WHEN taaf.FILE_NO IN ('04','06','08') THEN '30' ");  // 不具合部品詳細
		sb.append("WHEN taaf.FILE_NO IN ('09','10') THEN '100' ");      // その他
		//TODO:動画のコード変換⇒SW2汎用マスタのコード値決まり次第
		sb.append("WHEN taaf.FILE_NO = '11' THEN '110' ");              // 動画
		sb.append("WHEN taaf.FILE_NO = '12' THEN '90' ");               // お客様サイン
		sb.append("ELSE '100' ");                                       // その他画像
		sb.append("END ) ");

		sb.append("WHEN (tari.report_type = '090') ");
		sb.append("THEN ( ");
		sb.append("CASE ");
		//TODO:動画のコード変換⇒SW2汎用マスタのコード値決まり次第
		sb.append("WHEN taaf.FILE_NO = '11' THEN '110' ");              // 動画
		sb.append("WHEN taaf.FILE_NO = '12' THEN '90' ");               // お客様サイン
		sb.append("ELSE '100' ");                                       // その他,その他画像
		sb.append("END ) ");

		sb.append("WHEN (tari.report_type = '100') ");
		sb.append("THEN ( ");
		sb.append("CASE ");
		sb.append("WHEN taaf.FILE_NO = '01' THEN '60' ");               // 送付部品写真
		sb.append("WHEN taaf.FILE_NO = '02' THEN '50' ");               // 送り状伝票
		//TODO:動画のコード変換⇒SW2汎用マスタのコード値決まり次第
		sb.append("WHEN taaf.FILE_NO = '11' THEN '110' ");              // 動画
		sb.append("WHEN taaf.FILE_NO = '12' THEN '90' ");               // お客様サイン
		sb.append("ELSE '100' ");                                       // その他,その他画像
		sb.append("END ) ");
		sb.append("END AS titleCode, ");

		sb.append("mcvv.CODE_VALUE_1 AS title, ");
		sb.append("taaf.NOTE AS note ");
		sb.append("FROM TBL_GNAV_ATTACHMENT_FILE taaf ");
		sb.append("JOIN TBL_GNAV_REPORT_INFO tari ON taaf.REPORT_ID = tari.REPORT_ID ");
		sb.append(",MST_CODE_VALUE_V mcvv ");
		sb.append("WHERE ");
		sb.append("taaf.REPORT_ID = " + reportId + " ");
		sb.append("AND mcvv.CODE_TYPE = '0130' ");
		sb.append("AND mcvv.language = '" + language + "' ");
		sb.append("AND mcvv.CODE_VALUE_2 = tari.REPORT_TYPE ");
		sb.append("AND mcvv.CODE_VALUE_3 = taaf.FILE_NO ");
		sb.append("and SUBSTR(TBL_ATTACHMENT_FILE.ORIGINAL_FILE_NAME,-6,2) = taaf.FILE_NO  ");

		sb.append("UNION ");
		sb.append("SELECT ");
		sb.append("'90' AS titleCode, ");
		sb.append("null AS title, ");
		sb.append("taaf.NOTE AS note ");
		sb.append("FROM TBL_GNAV_ATTACHMENT_FILE taaf ");
		sb.append("WHERE ");
		sb.append("taaf.REPORT_ID = " + reportId + " ");
		sb.append("AND FILE_NO = 12 "); //署名
		sb.append("and SUBSTR(TBL_ATTACHMENT_FILE.ORIGINAL_FILE_NAME,-6,2) = FILE_NO  ");

		sb.append(") ");

		sb.append("WHERE ");
		sb.append("ORIGINAL_FILE_NAME LIKE 'sw2report%' ");
		sb.append("AND PROCESS_ID = ( ");
		sb.append("SELECT ID ");
		sb.append("FROM TBL_PROCESS ");
		sb.append("WHERE DOCUMENT_NO = '" + documentNo + "' ");
		sb.append(") ");



		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
		//	throw ex;
			int result = -1;
			return result;
		}

	}


	/**
	 * updateSaveFlg
	 * @param reportId
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 */
	public int updateSaveFlg(Integer reportId, Integer updateFlg)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("UPDATE TBL_GNAV_REPORT_INFO ");
		sb.append("SET SAVE_FLG = " + updateFlg + " ");
		sb.append("WHERE REPORT_ID = " + reportId + " ");
		if(updateFlg == 2) {
			sb.append("AND SAVE_FLG = 1 ");
		}

		logger.log(Level.INFO, ""+sb);

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
		//	throw ex;
			int result = -1;
			return result;
		}

	}

	/**
	 * updateSaveFlgAll
	 * @param reportId
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 */
	public int updateSaveFlgAll(Integer batchNo, Integer updateFlg, String insErrorDoc)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		if(insErrorDoc == null) {
			sb.append("UPDATE TBL_GNAV_REPORT_INFO ");
			sb.append("SET SAVE_FLG = " + updateFlg + " ");
			sb.append("WHERE DOCUMENT_NO IN( ");
			sb.append("SELECT DOCUMENT_NO ");
			sb.append("FROM TBL_REPORT_IF ");
			sb.append("WHERE BATCH_NUMBER = " + batchNo + ") ");

		}else {
			sb.append("UPDATE TBL_GNAV_REPORT_INFO ");
			sb.append("SET SAVE_FLG = " + updateFlg + " ");
			sb.append("WHERE DOCUMENT_NO IN( " + insErrorDoc + ") ");

		}

		logger.log(Level.INFO, ""+sb);

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
		//	throw ex;
			int result = -1;
			return result;
		}

	}


	/**
	 * updateSaveAttachmentFlg
	 * 次回処理対象にならないようにフラグ更新を行う
	 * @param reportId
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 */
	public int updateSaveAttachmentFlg(Integer batchNo)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);


			sb.append("UPDATE TBL_ATTACHMENT_IF ");
			sb.append("SET TAKE_FLAG = '0' "); //処理済み扱いとする
			sb.append("WHERE DOCUMENT_NO IN( ");
			sb.append("SELECT DOCUMENT_NO ");
			sb.append("FROM TBL_REPORT_IF ");
			sb.append("WHERE BATCH_NUMBER = " + batchNo + ") ");


		logger.log(Level.INFO, ""+sb);

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
		//	throw ex;
			int result = -1;
			return result;
		}

	}



	/**
	 * 暗号化
	 * exchangeSecurityKey
	 */
	@SuppressWarnings("unchecked")
	public String exchangeSecurityKey(String target, String seqKey) {

		try
        {
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(seqKey.getBytes(), "AES"));
//			return new String(Base64.getUrlEncoder().encode(cipher.doFinal(target.getBytes())));

			String encodeString = Base64.encodeBase64String(cipher.doFinal(target.getBytes()));
			encodeString = encodeString.replace("+", "-");
			encodeString = encodeString.replace("/", "_");
			return encodeString;



        }
        catch (Exception e)
        {
            System.out.println("暗号化error: "+ target + " " + e.toString());
        }
        return null;
	}

	/**
	 * 複合化
	 * exchangeSecurityKey
	 */
	@SuppressWarnings("unchecked")
	public String restoreSecurityKey(String target, String seqKey) {

		try
        {
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(seqKey.getBytes(), "AES"));
//			return new String(cipher.doFinal(Base64.getUrlDecoder().decode(target.getBytes())));
			return new String( cipher.doFinal(Base64.decodeBase64(target.getBytes())));


        }
        catch (Exception e)
        {
            System.out.println("複合化error: "+ target + " " + e.toString());
        }
        return null;
	}


	/**
	 * ユーザID取得
	 */
	public Integer findByBatchUserId(String userAccount)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("SELECT ID ");
		sb.append("FROM MST_USER ");
		sb.append("WHERE USER_ACCOUNT = '" + userAccount + "' ");

		List<Object[]> data;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			if (data != null && data.size() == 1 && data.get(0)[0] != null)
				return Integer.parseInt(data.get(0)[0].toString());
			db.close();
			return null;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}







//========================================================================================================================================

	/**
	 * formatTimeZoneUTC
	 *
	 * @param lastestDateTime: Date
	 * @return
	 */
	public String formatTimeZoneUTC(Date DateTime) {
		SimpleDateFormat fmtUTC = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		fmtUTC.setTimeZone(timeZone);

		return fmtUTC.format(DateTime);
	}

	public Date getDateTime(int minusYear, int minusMonth, int minusDate, Date dateTime) {

		Calendar cal = Calendar.getInstance();
//		cal.setTime(new Timestamp(System.currentTimeMillis()));
		cal.setTime(dateTime);
		cal.add(Calendar.YEAR, minusYear);
		cal.add(Calendar.MONTH, minusMonth);
		cal.add(Calendar.DATE, minusDate);

		return cal.getTime();
	}



}