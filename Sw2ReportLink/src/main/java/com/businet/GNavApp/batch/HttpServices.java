package com.businet.GNavApp.batch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HttpServices {

	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public static enum RequestType {
		POST, GET
	}

	// public static String USER_AGENT = "User-Agent: Mozilla/5.0 (Windows NT 10.0;
	// Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105
	// Safari/537.36 Edg/84.0.522.52";

	public static String GetParam(Map<String, Object> dictionaryParam) {
		if (dictionaryParam == null || dictionaryParam.size() == 0)
			return null;
		StringBuilder param = new StringBuilder();

		for (String key : dictionaryParam.keySet()) {
			if (param.length() != 0)
				param.append("&");

			param.append(key + "=" + dictionaryParam.get(key).toString());
		}
		return param.toString();
	}

	public static String sendHttpRequest(String url, RequestType requestType, String params, String authorizationToken,
			HashMap<String, String> reqHeaders) throws IOException {

//		System.out.println("SEND " + requestType.toString() + " REQUEST TO [" + url + "] , PARAMS [" + params + "]"
//				+ " TOKEN = [" + authorizationToken + "]");
		logger.log(Level.INFO, "SEND " + requestType.toString() + " REQUEST TO [" + url + "] , PARAMS [" + params + "]"
				+ " TOKEN = [" + authorizationToken + "]");


		URL obj = new URL(url);
		HttpURLConnection httpURLConnection = (HttpURLConnection) obj.openConnection();
		httpURLConnection.setRequestMethod(requestType.toString());
		httpURLConnection.setRequestProperty("Accept", "*/*");

		if (authorizationToken != null)
			httpURLConnection.setRequestProperty("Authorization", authorizationToken); // set Authorization Token for
																						// request

		httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

		if (reqHeaders != null) {

			for (Map.Entry<String, String> entry : reqHeaders.entrySet()) {
				httpURLConnection.setRequestProperty(entry.getKey(), entry.getValue());
			}

		}


		httpURLConnection.setDoOutput(true);
		OutputStream os = httpURLConnection.getOutputStream();

		if (params != null) {
			os.write(params.getBytes());
		}

		os.flush();
		os.close();

		int responseCode = httpURLConnection.getResponseCode();
//		System.out.println("Http Response Code :: " + responseCode);
		logger.log(Level.INFO, "Http Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { // success

			BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8"));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

//			System.out.println("RESULT : " + response.toString());
			logger.log(Level.INFO, "RESULT : " + response.toString());

			return response.toString();

		}else if(responseCode == HttpURLConnection.HTTP_UNAVAILABLE){  //Token Expired
//			System.out.println("Token Expired");
			logger.log(Level.INFO, "Token Expired");
			return "tokenExpired";

		} else {
//			System.out.println("unsuccess");
			logger.log(Level.INFO, "unsuccess");
		}

		return "unsuccess";
	}



}