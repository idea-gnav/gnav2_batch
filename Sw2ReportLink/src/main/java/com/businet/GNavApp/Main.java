package com.businet.GNavApp;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.businet.GNavApp.batch.Sw2ReportLink;



public class Main {

	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {

			Application.init();

			
			logger.log(Level.INFO, "START PROGESS SW2 REPORT LINK");
			String result = Application.getCurrent().runBatlet(new Sw2ReportLink());
			System.out.println(result);

			logger.log(Level.INFO, "END PROGESS SW2 REPORT LINK\n\n");

		} catch (Exception e) {
//			System.out.println("FAILED");
			logger.log(Level.SEVERE, e.getMessage(), e);
		}
	}

}
