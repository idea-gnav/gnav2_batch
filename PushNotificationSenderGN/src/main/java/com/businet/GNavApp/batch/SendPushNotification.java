package com.businet.GNavApp.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.Semaphore;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.businet.GNavApp.Application;
import com.businet.GNavApp.Model.ReturnMachineHistoryModel;
import com.eatthepath.pushy.apns.ApnsClient;
import com.eatthepath.pushy.apns.ApnsClientBuilder;
import com.eatthepath.pushy.apns.PushNotificationResponse;
import com.eatthepath.pushy.apns.util.ApnsPayloadBuilder;
import com.eatthepath.pushy.apns.util.SimpleApnsPayloadBuilder;
import com.eatthepath.pushy.apns.util.SimpleApnsPushNotification;
import com.eatthepath.pushy.apns.util.TokenUtil;
import com.eatthepath.pushy.apns.util.concurrent.PushNotificationFuture;


public class SendPushNotification implements Batchlet {

	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private final static Logger deviceLogger = Logger.getLogger(SendPushNotification.FAILED_DEVICE_LOG);
	public final static String FAILED_DEVICE_LOG = "FAILED_DEVICE_LOG";

	private final String DATE_TIME_FORMAT = "yyyyMMdd HH:mm:ss:SSS";

	private String LastSendTimeFile;
	private String PATH_CERTIFICATE;
	private String CERTIFICATE_PASSWORD;

	private String TITLE_EN;
	private String TITLE_JA;
	private String TITLE_ID;
	private String TITLE_TR;

	private String MSG_EN;
	private String MSG_JA;
	private String MSG_ID;
	private String MSG_TR;

	// Method to send Notifications from server to client end.
	public String AUTH_KEY_FCM;
	public String API_URL_FCM;
	private boolean IsTestEnvironment = false;

	// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
	String PushNotificationTarget;

	// 2019/06/20 RS DucNKT Add For ReturnMachine :End
	
	private String TOPIC_GNAV;//2021.03.23 DucNKT added
	//private String TOPIC_REMOTE_CARE;//2021.03.23 DucNKT added
	/**
	 * Default constructor.
	 *
	 * @throws Exception
	 */
	public SendPushNotification(String pushNotificationTarget) throws Exception {

		if (Application.getCurrent().getConfig().get("BatchSettings", "Environment", "product").equals("test")) {
			IsTestEnvironment = true;
		}
		// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
		this.PushNotificationTarget = pushNotificationTarget;
		// 2019/06/20 RS DucNKT Add For ReturnMachine :End

		TOPIC_GNAV = Application.getCurrent().getConfig().get("BatchSettings", "TopicGnav", "");//2021.03.23 DucNKT added
		
		LastSendTimeFile = Application.getCurrent().getConfig().get("BatchSettings", "LastSendTimeFile", "");
		PATH_CERTIFICATE = Application.getCurrent().getConfig().get("BatchSettings", "IOSCertification", "");
		CERTIFICATE_PASSWORD = Application.getCurrent().getConfig().get("BatchSettings", "IOSCertificationPassword",
				"");
		AUTH_KEY_FCM = Application.getCurrent().getConfig().get("BatchSettings", "AndroidFCMKey", "");
		API_URL_FCM = Application.getCurrent().getConfig().get("BatchSettings", "AndroidFCMUrl", "");
		ReadMessage(Application.getCurrent().getConfig().get("BatchSettings", "MesssageFile", ""));
		ReadTitle(Application.getCurrent().getConfig().get("BatchSettings", "TitleFile", ""));
	}

	/**
	 * @throws InterruptedException
	 * @throws TransformerException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @see Batchlet#process()
	 */
	public String process() {

		try {

			// Acquire last communication date
			Date lastestDateTime = ReadLastSend();

			ExecuteQuerys eq = new ExecuteQuerys();

			// 2019/06/20 RS DucNKT modify For ReturnMachine Push notification :Start

			if (PushNotificationTarget.equals("DTC")) {

				int deleteCount = eq.deleteTblFavoriteDtcHistory(lastestDateTime);
				logger.log(Level.INFO, "Delete 3-months or earlier by tbl_favorite_dtc_history = " + deleteCount);

				// 2019-02-04 aishikawa@LBN move :Start
//			int insertCount = eq.insertByNotExistsTblFavoriteDtcHistory(lastestDateTime);
//			logger.log(Level.INFO, "Insert by tbl_favorite_dtc_history = "+insertCount);
				// 2019-02-04 aishikawa@LBN move :End

				int updateCount = eq.updateFukkyuDtcTblFavoriteDtcHistory();
				logger.log(Level.INFO, "Update read_flg by tbl_favorite_dtc_history = " + updateCount);

				updateCount = eq.updateNoMatchTblFavoriteDtcHistory();
				logger.log(Level.INFO, "Update del_flg by tbl_favorite_dtc_history = " + updateCount);

				// 2019-02-04 aishikawa@LBN move :Start
				int insertCount = eq.insertByNotExistsTblFavoriteDtcHistory(lastestDateTime);
				logger.log(Level.INFO, "Insert by tbl_favorite_dtc_history = " + insertCount);
				// 2019-02-04 aishikawa@LBN move :End

				List<Object[]> listPushNotification = eq.findByDtc(lastestDateTime);

				// Update Last Time Send Push Before Send Push Notification
				WriteLastSend();

				if (listPushNotification != null && listPushNotification.size() > 0) {
					// iOS APNs
					this.SendApns(listPushNotification);
					// Android FCM
					this.SendFCM(listPushNotification);
				} else {
					return "No record found";
				}


			}
			// Update Data Return Machine Process
			else if (PushNotificationTarget.equals("ReturnMachine")) {

				// **↓↓↓ 2019/11/8  iDEA山下 パフォーマンス改修対応↓↓↓**//
				// get data return machine

				//チェック対象機番取得
				logger.log(Level.INFO, new String("PROCESSING FIND CHECK TARGET MACHINE ......"));
				List<Object[]> targetMachine = eq.findTargetMachine();
				logger.log(Level.INFO, new String("END PROCESS FIND CHECK TARGET MACHINE :" + targetMachine.size()));

				//エリアInチェック
				logger.log(Level.INFO, new String("PROCESSING FIND RETURN MACHINE DATA ......"));
				List<String> targetUserId = new ArrayList<String>();
				List<ReturnMachineHistoryModel> insertdata = new ArrayList<ReturnMachineHistoryModel>();

				if(targetMachine != null && targetMachine.size() > 0) {

					String targetKibanSerno = "";
					int count = 0;
					int limit = 0;
					for (Object[] obj : targetMachine) {
						if(count < 10 & count == 0) {
							targetKibanSerno =  obj[0].toString();
							count++;
							limit++;
						}else if(count < 10 & count != 0) {
							targetKibanSerno = targetKibanSerno + "," + obj[0].toString();
							count++;
							limit++;
						}

						if(limit == targetMachine.size() || count == 10) {
							logger.log(Level.INFO, new String("AREA IN CHECK START :" + targetKibanSerno));
							List<Object[]> returnMachinesArea = eq.findReturnMachineAndArea(targetKibanSerno);
							logger.log(Level.INFO, new String("AREA IN CHECK END "));
							count =0;

							// keiho icon process
							int keihoIconNo = 9;
							long frontKibanSerno = -1;
							int frontKengenCd = -1;
							int dtcLevel = -1;

							if (returnMachinesArea != null && returnMachinesArea.size() > 0) {
								logger.log(Level.INFO, new String("!!!Ocurrencia!!! "));

								// **↓↓↓ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↓↓↓**//
								String preKyotenCd = "";
								StringBuilder targetKyotenCdSb = new StringBuilder();

								for (Object[] obj2 : returnMachinesArea) {

									ReturnMachineHistoryModel rm = new ReturnMachineHistoryModel();
									long kibanSerno = 0;
									// String userId = "";
									String kyotenCd = "";

									int areaNo = 0;
									//int kengenCd = 0;

									if (obj2[0] != null)
										kibanSerno = ((Number) obj2[0]).longValue();

									if (obj2[1] != null)
										kyotenCd = (String) obj2[1];

									// Do not execute the command if have the same kyotenCd value
									if (!kyotenCd.equals(preKyotenCd)) {
										preKyotenCd = kyotenCd;

										if (targetKyotenCdSb.length() > 0)
											targetKyotenCdSb.append(",");

										targetKyotenCdSb.append(" '" + kyotenCd + "' ");

									}

									if (obj2[2] != null)
										areaNo = ((Number) obj2[2]).intValue();

									//if (obj2[5] != null)
										//kengenCd = ((Number) obj2[5]).intValue();

									rm.setKibanSerno(kibanSerno);
									rm.setKyotenCd(kyotenCd);
									rm.setAreaNo(areaNo);

									boolean dtcNormal = false, dtcImportant = false, yokoku = false, keyikoku = false;

									if (frontKibanSerno != kibanSerno
											//|| frontKengenCd != kengenCd
											) {
										//List<Object[]> dtcLevelCheck = eq.dtcLevelCheck(((Number) kibanSerno).intValue(), kengenCd);
										List<Object[]> dtcLevelCheck = eq.dtcLevelCheck(((Number) kibanSerno).intValue(), 2);

										if (dtcLevelCheck != null && dtcLevelCheck.size() > 0) {
											Object[] obj3 = dtcLevelCheck.get(0);
											if (obj3[1] != null) {
												dtcLevel = Integer.parseInt(obj3[1].toString());
											}
											if (dtcLevel == 3)
												dtcNormal = dtcImportant = true;
											else if (dtcLevel == 2)
												dtcNormal = true;
										}

										yokoku = (obj2[3] != null && ((Number) obj2[3]).intValue() > 0);
										keyikoku = (obj2[4] != null && ((Number) obj2[4]).intValue() > 0);

										if (dtcImportant && keyikoku)
											keihoIconNo = 1;
										else if (dtcImportant && yokoku && !keyikoku)
											keihoIconNo = 2;
										else if (dtcNormal && !dtcImportant && keyikoku)
											keihoIconNo = 3;
										else if (dtcNormal && !dtcImportant && yokoku && !keyikoku)
											keihoIconNo = 4;
										else if (dtcImportant && !yokoku && !keyikoku)
											keihoIconNo = 5;
										else if (dtcNormal && !dtcImportant && !yokoku && !keyikoku)
											keihoIconNo = 6;
										else if (!dtcNormal && !dtcImportant && keyikoku)
											keihoIconNo = 7;
										else if (!dtcNormal && !dtcImportant && yokoku && !keyikoku)
											keihoIconNo = 8;
										else
											keihoIconNo = 9;

										frontKibanSerno = kibanSerno;
										//frontKengenCd = kengenCd;
									}

									rm.setKeihoIconNo(keihoIconNo);
									insertdata.add(rm);
								}

								if (targetKyotenCdSb != null && targetKyotenCdSb.length() > 0) {

									List<Object[]> usrIds = eq.getUserIDbyKyotenCd(targetKyotenCdSb.toString());

									if (usrIds != null && usrIds.size() > 0) {
										for (Object[] userId : usrIds) {
											targetUserId.add(userId[0].toString());
										}
									}
								}

								// **↑↑↑ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↑↑↑**//
							}
						}

					}
				}
/*
				List<Object[]> returnMachinesArea = eq.findReturnMachineAndArea();
				List<String> targetUserId = new ArrayList<String>();
				List<ReturnMachineHistoryModel> insertdata = new ArrayList<ReturnMachineHistoryModel>();


				if (returnMachinesArea != null && returnMachinesArea.size() > 0) {
					logger.log(Level.INFO, new String("END PROCESS FIND RETURN MACHINE DATA :"+returnMachinesArea.size()));
					for (Object[] obj : returnMachinesArea) {

						ReturnMachineHistoryModel rm = new ReturnMachineHistoryModel();
						long kibanSerno = 0;
						String userId = "";
						int areaNo = 0;

						if (obj[0] != null)
							kibanSerno = ((Number) obj[0]).longValue();

						if (obj[1] != null)
							userId = (String) obj[1];

						if (obj[2] != null)
							areaNo = ((Number) obj[2]).intValue();

						rm.setKibanSerno(kibanSerno);
						rm.setUserID(userId);
						rm.setAreaNo(areaNo);

						// keiho icon process
						int keihoIconNo = 9;

						boolean dtcNormal = false, dtcImportant = false, yokoku = false, keyikoku = false;

						int dtcLevel = -1;
						if(obj[3] != null)
							dtcLevel = Integer.parseInt(obj[3].toString());


						if (dtcLevel == 3)
							dtcNormal = dtcImportant = true;
						else if (dtcLevel == 2)
							dtcNormal = true;

						yokoku = (obj[4] != null && ((Number) obj[4]).intValue() > 0);
						keyikoku = (obj[5] != null && ((Number) obj[5]).intValue() > 0);

						if (dtcImportant && keyikoku)
							keihoIconNo = 1;
						else if (dtcImportant && yokoku && !keyikoku)
							keihoIconNo = 2;
						else if (dtcNormal && !dtcImportant && keyikoku)
							keihoIconNo = 3;
						else if (dtcNormal && !dtcImportant && yokoku && !keyikoku)
							keihoIconNo = 4;
						else if (dtcImportant && !yokoku && !keyikoku)
							keihoIconNo = 5;
						else if (dtcNormal && !dtcImportant && !yokoku && !keyikoku)
							keihoIconNo = 6;
						else if (!dtcNormal && !dtcImportant && keyikoku)
							keihoIconNo = 7;
						else if (!dtcNormal && !dtcImportant && yokoku && !keyikoku)
							keihoIconNo = 8;

						rm.setKeihoIconNo(keihoIconNo);
						insertdata.add(rm);
						targetUserId.add(userId);

					}
				}else {
					logger.log(Level.INFO, new String("END PROCESS FIND RETURN MACHINE DATA: DATA NOT FOUND!"));
				}
*/
				// **↑↑↑ 2019/11/8  iDEA山下 パフォーマンス改修対応↑↑↑**//

				WriteLastSend();

				if (insertdata.size() > 0) {
					logger.log(Level.INFO, new String("END PROCESS FIND RETURN MACHINE DATA :"+insertdata.size()));

					logger.log(Level.INFO, new String("PROCESSING INSERT DATA TO TBL_RETURN_MACHINE_HISTORY"));

					eq.insertToTblReturnMachineHistory(insertdata);


					logger.log(Level.INFO, new String("END PROCESSING INSERT DATA TO TBL_RETURN_MACHINE_HISTORY"));

					// 【バッチ対象テーブルへの登録】
					Set<String> set = new HashSet<>(targetUserId);
					targetUserId.clear();
					targetUserId.addAll(set);
					StringBuilder strUserID = new StringBuilder();
					ArrayList<String> lstTargetUserId = new ArrayList<String>();
					int cnt = targetUserId.size();

					for (int i = 0; i < cnt; i++) {
						if (strUserID.length() > 0)
							strUserID.append("," + targetUserId.get(i));
						else
							strUserID.append(targetUserId.get(i));

						if (strUserID.length() > (500 - 25)) // target userid max leght : 500 || userid max lenght 24
						{
							lstTargetUserId.add(strUserID.toString());
							strUserID = new StringBuilder();
						}
					}
					lstTargetUserId.add(strUserID.toString());

					for (String targetId : lstTargetUserId) {
						eq.insertToTblNotification(targetId);
					}

				}else {
					logger.log(Level.INFO, new String("END PROCESS FIND RETURN MACHINE DATA: DATA NOT FOUND!"));
				}

				
				//2021.07.12 DucNKT added for return machine campaign ↓↓↓
				//(4)キャンペーンチェック対象機番検索】
				logger.log(Level.INFO, new String("FIND CAMPAIGN MACHINE ......"));
				List<Object[]> targetCampaignMachine = eq.findTargetMachineCampaign();
				logger.log(Level.INFO, new String("END FIND CAMPAIGN MACHINE :" + targetCampaignMachine.size()));

				List<Object[]> campaignMachineData = null;
				//(5)【キャンペーンエリアInチェック】
				if(targetCampaignMachine != null && targetCampaignMachine.size() > 0) {

				logger.log(Level.INFO, new String("FIND CAMPAIGN MACHINE RETURN TO AREA..."));
					String targetKibanSerno = "";
					int count = 0;
					int limit = 0;
					for (Object[] obj : targetCampaignMachine) {
						if(count < 10 & count == 0) {
							targetKibanSerno =  obj[0].toString();
							count++;
							limit++;
						}else if(count < 10 & count != 0) {
							targetKibanSerno = targetKibanSerno + "," + obj[0].toString();
							count++;
							limit++;
						}

						if(limit == targetCampaignMachine.size() || count == 10) {
							logger.log(Level.INFO, new String("AREA IN CHECK START :" + targetKibanSerno));
							List<Object[]> returnMachinesArea = eq.findReturnMachineCampaignAndArea(targetKibanSerno);
							logger.log(Level.INFO, new String("AREA IN CHECK END "));
							
							if(campaignMachineData == null)
								campaignMachineData = new ArrayList<Object[]>();
								
							if (returnMachinesArea != null && returnMachinesArea.size() > 0) {
								campaignMachineData.addAll(returnMachinesArea);
								logger.log(Level.INFO, new String("!!!Campaign Data Ocurrencia!!! "));
							}
							count =0;
						}
					}
					
					if (campaignMachineData != null && campaignMachineData.size() > 0) {
						logger.log(Level.INFO, new String("END FIND CAMPAIGN MACHINE RETURN TO AREA : "+ campaignMachineData.size()));
						logger.log(Level.INFO, new String("PROCESSING INSERT DATA TO TBL_RETURN_MACHINE_HISTORY_CP"));
						eq.insertToTblReturnMachineHistoryCampaign(campaignMachineData);
						logger.log(Level.INFO, new String("END PROCESSING INSERT DATA TO TBL_RETURN_MACHINE_HISTORY_CP"));
					}else {
						logger.log(Level.INFO, new String("END FIND CAMPAIGN MACHINE RETURN TO AREA : CAMPAIGN DATA NOT FOUND"));
					}

				}
				//3【リターンマシンキャンペーン履歴テーブルへの登録】
				//2021.07.12 DucNKT added for return machine campaign ↑↑↑
				
				
				// 前回機械緯度経度の更新
				logger.info("UPDATE DATA TO TBL_RETURN_MACHINE_WORK");
				eq.updateTblReturnMachineWork();

				// 【2日前以前発生未読データの既読処理】
				logger.info("UPDATE DATA TO TBL_RETURN_MACHINE_HISTORY");
				eq.updateTblReturnMachineHistory();
			}			
			
			// 2019/06/20 RS DucNKT modify For ReturnMachine Push notification :End

		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return "FAILED";
		}
		return "Push Notification Complete";

	}

	/**
	 * Send FCM
	 *
	 * @param listPushNotification
	 */
	private void SendFCM(List<Object[]> listPushNotification) {

		StringBuilder strDeviceList = new StringBuilder();
		strDeviceList.append("Begin sending a batch to FCM. Device tokens:");

		JSONArray ListMsg = new JSONArray();
		JSONObject Message = new JSONObject();
		JSONObject info = new JSONObject();

		for (int i = 0; i < listPushNotification.size(); i++) {

			Object[] pn = listPushNotification.get(i);

			String deviceType = null;
			String deviceToken = null;
			String userId = null;
			String languageCdFCM = null;
			int dtcbadge = 0, returnMachineBadge = 0, badge = 0;

			if (pn[0] != null) {
				deviceToken = pn[0].toString();

				if (pn[1] != null)
					deviceType = pn[1].toString();

				if (pn[2] != null)
					userId = pn[2].toString();

				if (pn[3] != null)
					languageCdFCM = pn[3].toString();

				if (pn[4] != null)
					dtcbadge = Integer.parseInt(pn[4].toString());

				// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
				if (pn[5] != null)
					returnMachineBadge = Integer.parseInt(pn[5].toString());

				badge = dtcbadge + returnMachineBadge;
				// 2019/06/20 RS DucNKT Add For ReturnMachine :End

				if (badge >= 1000)
					badge = 999;

				if (deviceType.equals("2") && !userId.isEmpty() && !deviceToken.isEmpty()) {
					strDeviceList.append("\n		[USER_ID: " + userId + "] [DEVICE TOKEN: " + deviceToken
							+ "] [BADGE: " + badge + "] [" + languageCdFCM + "]");

					Message = new JSONObject();
					info = new JSONObject();
					try {
						Message.put("to", deviceToken);
						info.put("title", createTitle(languageCdFCM));
						info.put("body", createMassage(languageCdFCM));
						info.put("dtcbadge", badge);

						Message.put("data", info);
						ListMsg.put(Message);
					} catch (Exception e) {
						logger.log(Level.SEVERE, e.getMessage(), e);
					}

				}
			}
		}

		if (ListMsg.length() > 0) {
			deviceLogger.log(Level.INFO, strDeviceList.toString());
		} else {
			return;
		}

		// FCM Request
		try {
			String authKey = AUTH_KEY_FCM; // You FCM AUTH key
			String FMCurl = API_URL_FCM;

			URL url = new URL(FMCurl);
			
			for (int i = 0; i < ListMsg.length(); i++) {

				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setUseCaches(false);
				conn.setDoInput(true);
				conn.setDoOutput(true);

				conn.setRequestMethod("POST");
				conn.setRequestProperty("Authorization", "key=" + authKey);
				conn.setRequestProperty("Content-Type", "application/json");

				JSONObject obj = ListMsg.getJSONObject(i);
				OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
				wr.write(obj.toString());
				wr.flush();
				wr.close();

				int responseCode = conn.getResponseCode();
				if (responseCode != 200) {
					throw new Exception("Failed ! Respone Code: " + responseCode);
				}

				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				if (response != null) {

					JSONObject jResponse = new JSONObject(response.toString());
					int failedCount = jResponse.getInt("failure");

					if (failedCount > 0) {
						JSONArray list = jResponse.getJSONArray("results");

						StringBuilder mes = new StringBuilder();
						mes.append("	Device tokens was sent failed:");

						for (int j = 0; j < list.length(); j++) {
							JSONObject res = list.getJSONObject(j);
							if (res.has("error"))
								mes.append(
										System.lineSeparator() + "		[DEVICE TOKEN: " + obj.getString("to") + "]");
						}

						deviceLogger.log(Level.WARNING, mes.toString());
					}
				}

				in.close();
			}

			deviceLogger.log(Level.INFO, "	Sending a batch to FCM devices successfully.\n\n\n\n");
		} catch (Exception e) {
			deviceLogger.log(Level.WARNING,
					"	Sending a batch to FCM devices failed. No device tokens was sent. Caused by:" + e.getMessage()
							+ "\n\n\n\n");
			logger.log(Level.SEVERE, e.getMessage(), e);
		}

	}

	/**
	 * Send APNs
	 *
	 * @param listPushNotification
	 */
	private void SendApns(List<Object[]> listPushNotification) {

		StringBuilder strDeviceList = new StringBuilder();
		strDeviceList.append("Begin sending a batch to APNs. Device tokens:");

		ArrayList<String> devices = new ArrayList<String>();
		ArrayList<Integer> dtcBadges = new ArrayList<Integer>();
		ArrayList<String> languageCds = new ArrayList<String>();

		String languageCd = "";

		for (int i = 0; i < listPushNotification.size(); i++) {
			Object[] pn = listPushNotification.get(i);

			String deviceType = null;
			String deviceToken = null;
			String userId = null;
			int dtcbadge = 0, returnMachineBadge = 0, badge = 0;

			if (pn[0] != null) {
				deviceToken = pn[0].toString();

				if (pn[1] != null)
					deviceType = pn[1].toString();
				if (pn[2] != null)
					userId = pn[2].toString();
				if (pn[3] != null)
					languageCd = pn[3].toString();
				if (pn[4] != null)
					dtcbadge = Integer.parseInt(pn[4].toString());

				// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
				if (pn[5] != null)
					returnMachineBadge = Integer.parseInt(pn[5].toString());

				badge = dtcbadge + returnMachineBadge;
				// 2019/06/20 RS DucNKT Add For ReturnMachine :End

				if (badge >= 1000)
					badge = 999;

				if (deviceType.equals("1") && !userId.isEmpty() && !deviceToken.isEmpty()) {
					strDeviceList.append(System.lineSeparator() + "		[USER_ID: " + userId + "] [DEVICE TOKEN: "
							+ deviceToken + "] [DTC BADGE: " + badge + "] [LANGUAGE: " + languageCd + "]");
					devices.add(deviceToken);
					dtcBadges.add(badge);
					languageCds.add(languageCd);
				}
			}
		}

		if (devices.size() > 0) {
			deviceLogger.log(Level.INFO, strDeviceList.toString());
		} else {
			return;
		}

		// APNs Requests
		ApnsClient apnsClient = null; //2021.09.06 DucNKT modify
		try {

			//2021.03.23 DucNKT modify : start
			/*
			 
			 
			ApnsService service = null;

//			PayloadBuilder payloadBuilder = APNS.newPayload();
//	        payloadBuilder = payloadBuilder
//	        		.alertTitle(createTitle(languageCd))
//	                .alertBody(createMassage(languageCd));
//	        		.alertTitle("DTC TEST!")
//	                .alertBody("This is a push notification send test. for GNav app.");

			if (IsTestEnvironment)
				service = APNS.newService().withCert(PATH_CERTIFICATE, CERTIFICATE_PASSWORD).withSandboxDestination()
						.build();
			else
				service = APNS.newService().withCert(PATH_CERTIFICATE, CERTIFICATE_PASSWORD).withProductionDestination()
						.build();

			for (int i = 0; i < devices.size(); i++) {

//				 PayloadBuilder perDevicePayloadBuilder = payloadBuilder.copy()
				PayloadBuilder payloadBuilder = APNS.newPayload();
				payloadBuilder = payloadBuilder.alertTitle(createTitle(languageCds.get(i)))
						.alertBody(createMassage(languageCds.get(i))).badge(dtcBadges.get(i))
						.customField("dtcbadge", dtcBadges.get(i));

				if (payloadBuilder.isTooLong()) {
					deviceLogger.log(Level.WARNING, "Payload is too long, shrinking it");
					payloadBuilder = payloadBuilder.shrinkBody();
				}

				service.push(devices.get(i), payloadBuilder.build());

				// スレッドでコケるのを防止
				Thread.sleep(1000);
			}

			Thread.sleep(10000);

			if (IsTestEnvironment)
				service = APNS.newService().withCert(PATH_CERTIFICATE, CERTIFICATE_PASSWORD).withSandboxDestination()
						.build();
			else
				service = APNS.newService().withCert(PATH_CERTIFICATE, CERTIFICATE_PASSWORD).withProductionDestination()
						.build();

			Map<String, Date> inactiveDevices = service.getInactiveDevices();
			if (inactiveDevices.size() > 0) {
				StringBuilder mes = new StringBuilder();
				mes.append("	Device tokens was sent failed:");
				for (String dv : inactiveDevices.keySet()) {
					mes.append("\n		[DEVICE TOKEN: " + dv + "]");
				}
				deviceLogger.log(Level.WARNING, mes.toString());
			}


*/
			

			String topic = TOPIC_GNAV;
			
			apnsClient = new ApnsClientBuilder()
			        .setApnsServer(IsTestEnvironment ? ApnsClientBuilder.DEVELOPMENT_APNS_HOST : ApnsClientBuilder.PRODUCTION_APNS_HOST)
			        .setClientCredentials(new File(PATH_CERTIFICATE),CERTIFICATE_PASSWORD )
			        .build();
			
		
			
			final Map<String, Object> inactiveDevices = new HashMap<String, Object>();
			final ArrayList<String> sendSuccessDevices = new ArrayList<String>();
			
			final Semaphore semaphore = new Semaphore(1000);
			int totalToken = devices.size();
			deviceLogger.log(Level.WARNING,"[DEBUG][iOS] Send notification to "+ totalToken + " token");
			
			for (int i = 0; i < totalToken; i++) {
				semaphore.acquire();
				final ApnsPayloadBuilder payloadBuilder = new SimpleApnsPayloadBuilder();
			    payloadBuilder.setAlertTitle(createTitle(languageCds.get(i)));
			    payloadBuilder.setAlertBody(createMassage(languageCds.get(i)));
			    payloadBuilder.setBadgeNumber(dtcBadges.get(i));
			    payloadBuilder.addCustomProperty("dtcbadge", dtcBadges.get(i)) ;			    			    
			    
			    final String payload = payloadBuilder.build();
			    final String token = TokenUtil.sanitizeTokenString(devices.get(i));	
			   			    
			    final SimpleApnsPushNotification pushNotification = new SimpleApnsPushNotification(token, topic, payload);
				final PushNotificationFuture<SimpleApnsPushNotification, PushNotificationResponse<SimpleApnsPushNotification>>
			    sendNotificationFuture = apnsClient.sendNotification(pushNotification);
				
				sendNotificationFuture.whenComplete((response, cause) -> {
					
					semaphore.release();
					
				    if (response != null) {
				    	if (response.isAccepted()) {
				    		sendSuccessDevices.add(response.getPushNotification().getToken());
				            System.out.println("[DEBUG] send success: "+response.getPushNotification().getToken());
				        } else {
				        	
				        	inactiveDevices.put(response.getPushNotification().getToken(), response.getRejectionReason());
				            System.out.println("[DEBUG] send failed: " +response.getPushNotification().getToken() + "| reasons: "+
				            		response.getRejectionReason());

				            response.getTokenInvalidationTimestamp().ifPresent(new Consumer<Instant>() {
								@Override
								public void accept(Instant timestamp) {
								    System.out.println("[DEBUG] send failed: the token is invalid at " + timestamp);
								}
							});
				        }
				    } else {
				    	inactiveDevices.put(pushNotification.getToken(), "Failed to send push notification.");
				    	System.err.println("[DEBUG] send failed ");
				        cause.printStackTrace();
				    }
				});
								
			}										
			
			Thread.sleep(5000);
			
			deviceLogger.log(Level.INFO,"[DEBUG][iOS] permit available "+semaphore.availablePermits());
			
			if(sendSuccessDevices.size() > 0)
			{
				StringBuilder mes = new StringBuilder();
				mes.append("	"+sendSuccessDevices.size()+ " Device tokens was sent successed:");
				for (String dv : sendSuccessDevices) {
					mes.append("\n		[DEVICE TOKEN: " + dv +" ]");
				}
				deviceLogger.log(Level.INFO,mes.toString());
			}
			
			if (inactiveDevices.size() > 0) {
				StringBuilder mes = new StringBuilder();
				mes.append("	"+inactiveDevices.size()+ " Device tokens was sent failed:");
				for (String dv : inactiveDevices.keySet()) {
					mes.append("\n		[DEVICE TOKEN: " + dv +" | Error: "+inactiveDevices.get(dv)+"]");
				}
				deviceLogger.log(Level.INFO,mes.toString());
			}
			
			//2021.03.23 DucNKT modify : end
			deviceLogger.log(Level.INFO, "	Sending a batch to iOS devices successfully.\n\n\n\n");

		} catch (Exception e) {
			deviceLogger.log(Level.WARNING,
					"	Sending a batch to iOS devices failed. No device tokens was sent. Caused by:" + e.getMessage()
							+ "\n\n\n\n");
			logger.log(Level.SEVERE, e.getMessage(), e);
		}
		finally {
			if (apnsClient != null)
				apnsClient.close();
		}

	}

	/**
	 *
	 * @return
	 * @throws ParseException
	 */
	private Date ReadLastSend() throws ParseException {
		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		SimpleDateFormat fmtUTC = new SimpleDateFormat(DATE_TIME_FORMAT);
		fmtUTC.setTimeZone(timeZone);
		Date today = new Date();
		String lastestTime = "";
		try {
			FileReader fileReader = new FileReader(LastSendTimeFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			lastestTime = bufferedReader.readLine();
			bufferedReader.close();

			return fmtUTC.parse(lastestTime);

		} catch (FileNotFoundException ex) {
			return today;

		} catch (IOException ex) {
			return today;

		} catch (NullPointerException e) {
			return today;
		}
	}

	/**
	 *
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private void WriteLastSend() throws IOException, ClassNotFoundException, SQLException {

		FileWriter fileWriter = new FileWriter(LastSendTimeFile);

		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

		DateFormat dfUTC = new SimpleDateFormat(DATE_TIME_FORMAT);
		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		Date today = new Date();
		dfUTC.setTimeZone(timeZone);

		bufferedWriter.write(dfUTC.format(today));
		bufferedWriter.close();
	}

	/**
	 * Read message.xml
	 *
	 * @param path
	 * @throws Exception
	 */
	private void ReadMessage(String path) throws Exception {

		File fXmlFile = new File(path);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);

		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("messages");

		if (nList == null || nList.getLength() < 1)
			throw new Exception("Message file is invalid format");
		Node root = nList.item(0);

		for (int temp = 0; temp < root.getChildNodes().getLength(); temp++) {
			Node nNode = root.getChildNodes().item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				if (nNode.getNodeName() == "english")
					MSG_EN = nNode.getTextContent();

				else if (nNode.getNodeName() == "japanese")
					MSG_JA = nNode.getTextContent();

				else if (nNode.getNodeName() == "indonesian")
					MSG_ID = nNode.getTextContent();

				else if (nNode.getNodeName() == "turkish")
					MSG_TR = nNode.getTextContent();

			}
		}

		return;
	}

	/**
	 * Read message.xml
	 *
	 * @param path
	 * @throws Exception
	 */
	private void ReadTitle(String path) throws Exception {

		File fXmlFile = new File(path);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);

		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("titles");

		if (nList == null || nList.getLength() < 1)
			throw new Exception("titles file is invalid format");
		Node root = nList.item(0);

		for (int temp = 0; temp < root.getChildNodes().getLength(); temp++) {
			Node nNode = root.getChildNodes().item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				if (nNode.getNodeName() == "english")
					TITLE_EN = nNode.getTextContent();

				else if (nNode.getNodeName() == "japanese")
					TITLE_JA = nNode.getTextContent();

				else if (nNode.getNodeName() == "indonesian")
					TITLE_ID = nNode.getTextContent();

				else if (nNode.getNodeName() == "turkish")
					TITLE_TR = nNode.getTextContent();

			}
		}

		return;
	}

	public String createTitle(String languageCd) {

		String title = null;
		if (languageCd.toLowerCase().equals("en"))
			title = TITLE_EN;
		else if (languageCd.toLowerCase().equals("ja"))
			title = TITLE_JA;
		else if (languageCd.toLowerCase().equals("id"))
			title = TITLE_ID;
		else if (languageCd.toLowerCase().equals("tr"))
			title = TITLE_TR;
		else
			title = TITLE_EN;

		return title;
	}

	public String createMassage(String languageCd) {

		String massage = null;
		if (languageCd.toLowerCase().equals("en"))
			massage = MSG_EN;
		else if (languageCd.toLowerCase().equals("ja"))
			massage = MSG_JA;
		else if (languageCd.toLowerCase().equals("id"))
			massage = MSG_ID;
		else if (languageCd.toLowerCase().equals("tr"))
			massage = MSG_TR;
		else
			massage = MSG_EN;

		return massage;
	}

}