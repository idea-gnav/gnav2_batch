package com.businet.GNavApp.batch;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.businet.GNavApp.Application;
import com.businet.GNavApp.Model.ReturnMachineHistoryModel;
import com.businet.GNavApp.db.SimpleDB;

public class ExecuteQuerys {

	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private String driverPath;
	private String driver;
	private String connectString;
	private String username;
	private String password;

	public ExecuteQuerys() {
		driverPath = Application.getCurrent().getConfig().get("Database", "DriverFile", "");
		driver = Application.getCurrent().getConfig().get("Database", "Driver", "");
		connectString = Application.getCurrent().getConfig().get("Database", "Connection", "");
		username = Application.getCurrent().getConfig().get("Database", "UserName", "");
		password = Application.getCurrent().getConfig().get("Database", "Password", "");
	}

	/**
	 * formatTimeZoneUTC
	 *
	 * @param lastestDateTime: Date
	 * @return
	 */
	public String formatTimeZoneUTC(Date DateTime) {
		SimpleDateFormat fmtUTC = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		fmtUTC.setTimeZone(timeZone);

		return fmtUTC.format(DateTime);
	}

	public Date getDateTime(int minusYear, int minusMonth, int minusDate, Date dateTime) {

		Calendar cal = Calendar.getInstance();
//		cal.setTime(new Timestamp(System.currentTimeMillis()));
		cal.setTime(dateTime);
		cal.add(Calendar.YEAR, minusYear);
		cal.add(Calendar.MONTH, minusMonth);
		cal.add(Calendar.DATE, minusDate);

		return cal.getTime();
	}

	/**
	 * deleteTblFavoriteDtcHistory: 譛�邨よ峩譁ｰ蠕�3繝ｶ譛医ョ繝ｼ繧ｿ縺ｮ蜑企勁 DEL_FLG 0,1 縺ｫ髢｢繧上ｉ縺壹�ヽEAD_FLG 1
	 * 縺ｮ蝣ｴ蜷医↓髯舌ｋ縲�
	 */
	public int deleteTblFavoriteDtcHistory(Date lastestDateTime)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("DELETE FROM TBL_FAVORITE_DTC_HISTORY  ");
		sb.append("WHERE READ_FLG = 1 ");
		sb.append("AND UPDATE_DTM < TO_TIMESTAMP('" + formatTimeZoneUTC(getDateTime(0, -3, 0, lastestDateTime))
				+ "', 'yyyyMMddhh24:mi:ss.FF6') ");
		sb.append(" ");

//		System.out.println(new String(sb));

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}

	/**
	 * insertByNotExistsTblFavoriteDtcHistory -
	 * 譛�邨ょ�ｦ逅�譌･譎ゆｻ･髯阪↓逋ｺ逕溘＠縺溘♀豌励↓蜈･繧頑ｩ滓｢ｰ縺ｮ譛ｪ蠕ｩ譌ｧDTC繧貞ｱ･豁ｴ繝�繝ｼ繝悶Ν縺ｫ險倬鹸縺吶ｋ縲�
	 *
	 * @param lastestDateTime
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 */
	public int insertByNotExistsTblFavoriteDtcHistory(Date lastestDateTime)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder insert = new StringBuilder();
		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		insert.append("INSERT INTO TBL_FAVORITE_DTC_HISTORY( ");
		insert.append("USER_ID, ");
		insert.append("KIBAN_SERNO, ");
		insert.append("KIBAN_ADDR, ");
		insert.append("EVENT_NO, ");
		insert.append("EVENT_SHUBETU, ");
		insert.append("RECV_TIMESTAMP, ");
		insert.append("HASSEI_TIMESTAMP, ");
		insert.append("CON_TYPE, ");
		insert.append("READ_FLG, ");
		insert.append("DEL_FLG, ");
		insert.append("REGIST_DTM, ");
		insert.append("REGIST_USER, ");
		insert.append("REGIST_PRG, ");
		insert.append("UPDATE_DTM, ");
		insert.append("UPDATE_USER, ");
		insert.append("UPDATE_PRG ");
		insert.append(") ");

		sb.append("SELECT  ");

		sb.append("TUF.USER_ID, ");
		sb.append("TES.KIBAN_SERNO, ");
		sb.append("TES.KIBAN_ADDR, ");
		sb.append("TES.EVENT_NO, ");
		sb.append("TES.EVENT_SHUBETU, ");
		sb.append("TES.RECV_TIMESTAMP, ");
		sb.append("TES.HASSEI_TIMESTAMP, ");
		sb.append("TES.CON_TYPE, ");
		sb.append("0, ");
		sb.append("0, ");
		sb.append("SYSDATE, ");
		sb.append("'PushBatch', ");
		sb.append("'GNavApp', ");
		sb.append("SYSDATE, ");
		sb.append("'PushBatch', ");
		sb.append("'GNavApp' ");

		sb.append("FROM TBL_EVENT_SEND TES ");

		sb.append("JOIN TBL_USER_FAVORITE TUF ");
		sb.append("ON TES.KIBAN_SERNO = TUF.KIBAN_SERNO ");
		sb.append("AND TUF.DEL_FLG = 0 ");

		sb.append("AND EXISTS ( "); // GNav繝ｦ繝ｼ繧ｶ繝ｼ邨槭ｊ霎ｼ縺ｿ
		sb.append("SELECT * FROM TBL_USER_DEVICE TUD ");
		sb.append("WHERE TUD.USER_ID = TUF.USER_ID ");
		sb.append("AND TUD.APP_FLG = 1 ");
		sb.append(") ");

		sb.append("AND ( (TES.EVENT_SHUBETU = 8 AND TES.CON_TYPE IN('C', 'C2') ) ");
		sb.append("OR ( TES.EVENT_SHUBETU = 3 AND TES.CON_TYPE IN('T2', 'T', 'S', 'D', 'N', 'NH', 'ND') ) ) ");

		sb.append("AND TES.FUKKYU_TIMESTAMP IS NULL ");
		sb.append("AND TES.REGIST_DTM > TO_TIMESTAMP('" + formatTimeZoneUTC(lastestDateTime)
				+ "', 'yyyyMMddhh24:mi:ss.FF6') ");

		sb.append("JOIN MST_USER MU ");
		sb.append("ON TUF.USER_ID = MU.USER_ID ");

		sb.append("JOIN MST_SOSIKI MS ");
		sb.append("ON MU.SOSIKI_CD = MS.SOSIKI_CD ");

		// **↓↓↓ 2021/01/28 iDEA Yamashita　結合条件の修正 ↓↓↓**//
		sb.append("JOIN MST_MACHINE mm ON tes.KIBAN_SERNO = mm.KIBAN_SERNO AND mm.DELETE_FLAG = 0 ");

		sb.append("INNER JOIN TBL_KEIHO_HYOJI_SET TK  ");
//		sb.append("ON (TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE OR NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE)) ");
		sb.append("ON ( ");
		sb.append("TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE ");
		sb.append("OR (( MM.CON_TYPE != 'D' OR MM.EISEI_FLG != 1) AND NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE) )) ");
		// **↑↑↑ 2021/01/28 iDEA Yamashita　結合条件の修正 ↑↑↑**//
		sb.append("AND TES.EVENT_SHUBETU = TK.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = TK.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = TK.MACHINE_KBN ");
		sb.append("AND TK.KENGEN_CD = MS.KENGEN_CD ");

//		sb.append("INNER JOIN MST_EVENT_SEND MES ");

		sb.append("LEFT JOIN ( ");
		sb.append("SELECT DISTINCT EVENT_CODE, SHUBETU_CODE, CON_TYPE, MACHINE_KBN, DELETE_FLAG ");
		sb.append("FROM MST_EVENT_SEND ");
		sb.append("WHERE DELETE_FLAG = 0 ");
		sb.append(") MES ");

		sb.append("ON TES.EVENT_NO = MES.EVENT_CODE ");
		sb.append("AND TES.EVENT_SHUBETU = MES.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = MES.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = MES.MACHINE_KBN ");

//		sb.append("AND MES.DELETE_FLAG = 0 ");
//		sb.append("AND MES.LANGUAGE_CD = 'en' ");
//		sb.append("AND TES.EVENT_SHUBETU = 8 ");
//		sb.append("AND TES.CON_TYPE IN ('C','C2') ");

//		sb.append("AND TES.SYORI_FLG <> 9 ");


		// **↓↓↓ 2021/01/28 iDEA Yamashita　処理フラグ9データの参照制御を追加 ↓↓↓**//
//		sb.append("WHERE TES.SYORI_FLG <> 9 ");
//		sb.append("AND NOT EXISTS ( ");
		sb.append("WHERE NOT EXISTS ( ");
		sb.append("SELECT * FROM TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("WHERE DEL_FLG = 0 ");
		sb.append("AND TUF.USER_ID = TFDH.USER_ID ");
		sb.append("AND TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.CON_TYPE = TFDH.CON_TYPE ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append(") ");
		sb.append("AND ( ( EXISTS ( ");
		sb.append("SELECT * ");
		sb.append("FROM MST_USER mu ");
		sb.append("LEFT JOIN MST_SOSIKI ms ON mu.SOSIKI_CD = ms.SOSIKI_CD ");
		sb.append("LEFT JOIN MST_KENGEN mk ON ms.KENGEN_CD = mk.KENGEN_CD ");
		sb.append("WHERE ");
		sb.append("tuf.USER_ID = mu.USER_ID ");
		sb.append("AND mk.KEIHOU_KENGEN != 1 ");
		sb.append(") AND tes.SYORI_FLG <> 9 ) ");
		sb.append("OR ( EXISTS ( ");
		sb.append("SELECT * ");
		sb.append("FROM MST_USER mu ");
		sb.append("LEFT JOIN MST_SOSIKI ms ON mu.SOSIKI_CD = ms.SOSIKI_CD ");
		sb.append("LEFT JOIN MST_KENGEN mk ON ms.KENGEN_CD = mk.KENGEN_CD ");
		sb.append("WHERE ");
		sb.append("tuf.USER_ID = mu.USER_ID ");
		sb.append("AND mk.KEIHOU_KENGEN = 1 ");
		sb.append(") ) ) ");
		// **↑↑↑ 2021/01/28 iDEA Yamashita　処理フラグ9データの参照制御を追加 ↑↑↑**//
//		sb.append("AND TES.FUKKYU_TIMESTAMP IS NULL ");
		sb.append("ORDER BY TES.HASSEI_TIMESTAMP_LOCAL DESC ");

		System.out.println(new String(sb));

		try {
			db.open(username, password);
			// 2019-02-13 aishikawa@LBN add :Start
			List<Object[]> querys = db.query(sb.toString());
			// 2019-02-13 aishikawa@LBN add :End
			int result = db.queryUpdate(insert.toString() + sb.toString());
			db.close();
			// 2019-02-13 aishikawa@LBN add :Start
			StringBuilder logs = new StringBuilder();
			logs.append("Insert by query. = " + sb.toString() + "\n");
			if (querys.size() > 0) {
				for (Object[] log : querys) {
					logs.append("		[USER_ID:" + log[0].toString() + "] [KIBAN_SERNO:" + log[1].toString()
							+ "] [KIBAN_ADDR:" + log[2].toString() + "] [EVENT_NO:" + log[3].toString()
							+ "] [EVENT_SHUBETU:" + log[4].toString() + "] [RECV_TIMESTAMP:" + log[5].toString()
							+ "] [HASSEI_TIMESTAMP:" + log[6].toString() + "] [CON_TYPE:" + log[7].toString() + "]\n");
				}
				logs.append("\n");
			} else {
				logs.append("[NO DATA]\n");
			}
			logger.log(Level.INFO, new String(logs));
			// 2019-02-13 aishikawa@LBN add :End
			return result;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}

	/**
	 * 蠕ｩ譌ｧ貂医∩縺ｮ譌｢隱ｭ蜃ｦ逅� updateByFukkyuReadFlg DEL_FLG 0, 1 縺ｫ髢｢繧上ｉ縺壹�√♀豌励↓蜈･繧灰TC縺ｮ蠕ｩ譌ｧ貂医∩譌｢隱ｭ繝輔Λ繧ｰ譖ｴ譁ｰ
	 */
	public int updateFukkyuDtcTblFavoriteDtcHistory()
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("UPDATE TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("SET READ_FLG = 1, ");
		sb.append("UPDATE_DTM = sysdate, ");
		sb.append("UPDATE_USER = 'PushBatch', ");
		sb.append("UPDATE_PRG = 'FukkyuDtc' ");
		sb.append("WHERE TFDH.READ_FLG = 0 ");
		sb.append("AND EXISTS ( ");
		sb.append("SELECT * FROM TBL_EVENT_SEND TES ");
		sb.append("WHERE TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append("AND TES.FUKKYU_TIMESTAMP IS NOT NULL ");
		sb.append(")");

//		System.out.println(new String(sb));

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}

	/**
	 * 蜃ｦ逅�貂医ヵ繝ｩ繧ｰ9縺ｫ譖ｴ譁ｰ縺輔ｌ縺溷�ｴ蜷医�ｮ閠�諷ｮ 驕主悉縺ｫ螻･豁ｴ繝�繝ｼ繝悶Ν縺ｫ險倬鹸縺輔ｌ縺溘′縲√う繝吶Φ繝医ユ繝ｼ繝悶Ν縺ｮ繧ｭ繝ｼ謨ｴ蜷域�ｧ縺ｨ繧後↑縺�蝣ｴ蜷医�ｮ閠�諷ｮ 譌｢隱ｭ繝輔Λ繧ｰ1縲∝炎髯､繝輔Λ繧ｰ1縺ｫ譖ｴ譁ｰ
	 */
	public int updateNoMatchTblFavoriteDtcHistory() throws SQLException, ClassNotFoundException, MalformedURLException {
//Mis

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("UPDATE TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("SET DEL_FLG = 1, ");
		sb.append("READ_FLG = 1, ");
		sb.append("UPDATE_DTM = sysdate, ");
		sb.append("UPDATE_USER = 'PushBatch', ");
		sb.append("UPDATE_PRG = 'NoMatchDtc' ");
		sb.append("WHERE DEL_FLG = 0 ");
		sb.append("AND ( NOT EXISTS ( ");
		sb.append("SELECT * FROM TBL_EVENT_SEND TES ");
		sb.append("WHERE TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append(") OR EXISTS ( ");
		sb.append("SELECT * FROM TBL_EVENT_SEND TES ");
		sb.append("WHERE TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append("AND TFDH.DEL_FLG = 0 ");
		sb.append("AND TES.SYORI_FLG = 9 ");
		// **↓↓↓ 2021/01/28 iDEA Yamashita　処理フラグ9データの参照制御を追加 ↓↓↓**//
		sb.append("AND NOT EXISTS ( ");
		sb.append("SELECT * ");
		sb.append("FROM MST_USER mu ");
		sb.append("LEFT JOIN MST_SOSIKI ms ON mu.SOSIKI_CD = ms.SOSIKI_CD ");
		sb.append("LEFT JOIN MST_KENGEN mk ON ms.KENGEN_CD = mk.KENGEN_CD ");
		sb.append("WHERE ");
		sb.append("tfdh.USER_ID = mu.USER_ID ");
		sb.append("AND mk.KEIHOU_KENGEN = 1 ");
		sb.append(") ");
		// **↑↑↑ 2021/01/28 iDEA Yamashita　処理フラグ9データの参照制御を追加 ↑↑↑**//
		sb.append(")) ");

		System.out.println(new String(sb));

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;

		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}

	/**
	 * findByDtc
	 *
	 * @param lastestDateTime: Date
	 * @return
	 */
	public List<Object[]> findByDtc(Date lastestDateTime)
			throws ClassNotFoundException, SQLException, MalformedURLException {

		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT DISTINCT(TUD.DEVICE_TOKEN), TUD.DEVICE_TYPE ,TUD.USER_ID ,TUD.LANGUAGE_CD ");
		sb.append(", ( SELECT COUNT(*) FROM TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("WHERE TFDH.USER_ID = TUD.USER_ID AND DEL_FLG = 0 AND TFDH.READ_FLG = 0 )as DTCBADGE ");
		// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
		// **↓↓↓ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↓↓↓**//
		sb.append(", ( SELECT COUNT(*) FROM TBL_RETURN_MACHINE_HISTORY TRMH ");
		sb.append("LEFT JOIN MST_RETURN_MACHINE_SEND_USER mrmsu ");
		sb.append("ON mrmsu.KYOTEN_CD = TRMH.KYOTEN_CD ");
		sb.append("WHERE mrmsu.USER_ID = TUD.USER_ID AND TRMH.READ_FLG = 0 )as RMBADGE ");
		// **↑↑↑ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↑↑↑**//
		// 2019/06/20 RS DucNKT Add For ReturnMachine :End
		sb.append("FROM TBL_USER_DEVICE TUD ");

		sb.append("INNER JOIN TBL_USER_FAVORITE TUF ");
		sb.append("ON TUD.USER_ID = TUF.USER_ID ");
		sb.append("AND TUD.DEL_FLG = 0  ");
		sb.append("AND TUD.APP_FLG = 1 "); // GNAV繝ｦ繝ｼ繧ｶ繝ｼ邨槭ｊ霎ｼ縺ｿ
		sb.append("AND TUF.DEL_FLG = 0 ");
//		sb.append("AND TUD.APP_FLG = "+languageCd);			// 險�隱槫ｯｾ蠢�
		sb.append("AND LENGTH(TUD.DEVICE_TOKEN) > 60 "); // 60譁�蟄玲悴貅�縺ｯ蟇ｾ雎｡螟�

		sb.append("AND EXISTS( ");

		sb.append("SELECT ");
		sb.append("TES.KIBAN_SERNO ");
		sb.append("FROM TBL_EVENT_SEND TES ");

		// **↓↓↓ 2021/01/28 iDEA Yamashita　結合条件の修正 ↓↓↓**//
		sb.append("JOIN MST_MACHINE mm ON tes.KIBAN_SERNO = mm.KIBAN_SERNO AND mm.DELETE_FLAG = 0 ");

		sb.append("INNER JOIN TBL_KEIHO_HYOJI_SET TK  ");

//		sb.append("ON (TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE OR NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE)) ");
		sb.append("ON ( ");
		sb.append("TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE ");
		sb.append("OR (( MM.CON_TYPE != 'D' OR MM.EISEI_FLG != 1) AND NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE) )) ");
		// **↑↑↑ 2021/01/28 iDEA Yamashita　結合条件の修正 ↑↑↑**//
		sb.append("AND TES.EVENT_SHUBETU = TK.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = TK.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = TK.MACHINE_KBN ");

		sb.append("INNER JOIN MST_USER MU ");
		sb.append("ON MU.KENGEN_CD = TK.KENGEN_CD ");

//		sb.append("INNER JOIN MST_EVENT_SEND MES ");

		sb.append("LEFT JOIN ( ");
		sb.append("SELECT DISTINCT EVENT_CODE, SHUBETU_CODE, CON_TYPE, MACHINE_KBN, DELETE_FLAG ");
		sb.append("FROM MST_EVENT_SEND ");
		sb.append("WHERE DELETE_FLAG = 0 ");
		sb.append(") MES ");

		sb.append("ON TES.EVENT_NO = MES.EVENT_CODE ");
		sb.append("AND TES.EVENT_SHUBETU = MES.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = MES.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = MES.MACHINE_KBN ");

		sb.append("WHERE TES.SYORI_FLG <> 9 ");

		sb.append("AND ( (TES.EVENT_SHUBETU = 8 AND TES.CON_TYPE IN('C', 'C2') ) ");
		sb.append("OR ( TES.EVENT_SHUBETU = 3 AND TES.CON_TYPE IN('T2', 'T', 'S', 'D', 'N', 'NH', 'ND') ) ) ");

//		sb.append("WHERE MES.DELETE_FLAG = 0 ");
//		sb.append("AND MES.LANGUAGE_CD = 'en' ");
//		sb.append("AND TES.EVENT_SHUBETU = 8 ");
//		sb.append("AND TES.CON_TYPE IN ('C','C2') ");

		sb.append("AND TES.REGIST_DTM > TO_TIMESTAMP('" + formatTimeZoneUTC(lastestDateTime)
				+ "', 'yyyyMMddhh24:mi:ss.FF6') ");

		sb.append("AND TUF.USER_ID = TUD.USER_ID ");
		sb.append("AND TES.KIBAN_SERNO =  TUF.KIBAN_SERNO ");

		sb.append(" )");

		System.out.println(new String(sb));

		List<Object[]> data;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			db.close();
			return data;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}

	public List<Object[]> findReturnMachineAndArea(String kibanSerno )
			throws ClassNotFoundException, SQLException, MalformedURLException {
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		StringBuilder sb = new StringBuilder();

		// **↓↓↓ 2019/11/8  iDEA山下 パフォーマンス改修対応 ↓↓↓**//
		sb.append("SELECT ");
//		sb.append("a.kiban_serno, ");     //0
//		sb.append("a.user_id, ");         //1
//		sb.append("a.area_no, ");         //2
		// **↓↓↓ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↓↓↓**//
		sb.append("a.kiban_serno, "); // 0
		sb.append("a.kyoten_cd, "); // 1
		sb.append("a.area_no, "); // 2
//		sb.append("b.alertlv, ");
		sb.append("a.yokoku_count, "); // 3
		sb.append("a.keyikoku_count "); // 4
		//sb.append(",a.kengen_cd "); // 5
		sb.append("FROM ");
		sb.append("( ");
		sb.append("SELECT ");
		// sb.append("mm.kiban_serno,trma.user_id,trma.area_no,mm.yokoku_count,mm.keyikoku_count, ");
//		sb.append("mm.kiban_serno,trma.kyoten_cd,trma.area_no,mm.yokoku_count,mm.keyikoku_count, ");
		sb.append("mm.kiban_serno,turm.kyoten_cd,trma.area_no,mm.yokoku_count,mm.keyikoku_count, "); //機番の拠店CD取得
		sb.append("trma.radius / 1000 AS radius, ");
		sb.append("( 6378.137 * acos(round(sin(trma.ido / 3600 / 1000 * atan2(0,-1) / 180) * sin(mm.new_ido / 3600 / 1000 * atan2(0,-1) / 180) ");
		sb.append("+ cos(trma.ido / 3600 / 1000 * atan2(0,-1) / 180) * cos(mm.new_ido / 3600 / 1000 * atan2(0,-1) / 180) * cos(abs( (mm.new_keido / 3600 / 1000) ");
		sb.append("- (trma.keido / 3600 / 1000) ) * atan2(0,-1) / 180),20) ) )  AS distance_t, ");
		sb.append("( 6378.137 * acos(round(sin(trma.ido / 3600 / 1000 * atan2(0,-1) / 180) * sin(trmw.ido / 3600 / 1000 * atan2(0,-1) / 180) ");
		sb.append("+ cos(trma.ido / 3600 / 1000 * atan2(0,-1) / 180) * cos(trmw.ido / 3600 / 1000 * atan2(0,-1) / 180) * cos(abs( (trmw.keido / 3600 / 1000) ");
		sb.append("- (trma.keido / 3600 / 1000) ) * atan2(0,-1) / 180),20) ) )  AS distance_y ");
		//sb.append(",mk.kengen_cd ");
		sb.append("FROM ");
		sb.append("mst_machine mm ");
		sb.append("JOIN tbl_user_return_machine turm ON mm.kiban_serno = turm.kiban_serno ");
		// sb.append("JOIN tbl_return_machine_area trma ON turm.user_id = trma.user_id ");
		sb.append("JOIN tbl_return_machine_work trmw ON turm.kiban_serno = trmw.kiban_serno ");
//		sb.append("JOIN tbl_return_machine_area trma ON turm.kyoten_cd = trma.kyoten_cd "); //拠店フリー対応
		sb.append(",tbl_return_machine_area trma "); //拠店フリー対応
		// sb.append("JOIN mst_user mu ON trma.user_id = mu.user_id ");
		// sb.append("JOIN mst_sosiki ms ON mu.sosiki_cd = ms.sosiki_cd ");
		// sb.append("JOIN mst_kengen mk ON ms.kengen_cd = mk.kengen_cd ");
		sb.append("WHERE ");
		sb.append("mm.delete_flag = 0 ");
		sb.append("AND mm.kiban_serno IN( " + kibanSerno + " ) ");
		sb.append("AND turm.del_flg = 0 ");
		sb.append("AND turm.del_flg = 0 ");
		sb.append("AND trma.delete_flg = 0 ");
		sb.append("AND mm.new_ido IS NOT NULL ");
		sb.append("AND mm.new_keido IS NOT NULL ");
		sb.append("AND ( CASE ");
		sb.append("WHEN trma.warningcurrentlyprogress_flg = 1 ");
		sb.append("OR trma.periodicservicenotice_flg = 1 ");
		sb.append("OR trma.periodicservicenowdue_flg = 1 ");
		sb.append("OR trma.hour_meter_flg = 1  ");
		sb.append("THEN ( CASE ");
		sb.append("WHEN (  ");
		sb.append(
				"( CASE WHEN trma.warningcurrentlyprogress_flg = 1 THEN ( CASE WHEN mm.keyihou_hasseyi_tyu_count > 0 THEN 1 ELSE 0 END ) ELSE 0 END ) = 1 ");
		sb.append(
				"OR ( CASE WHEN trma.periodicservicenotice_flg = 1 THEN ( CASE WHEN mm.yokoku_count > 0 THEN 1 ELSE 0 END ) ELSE 0 END ) = 1 ");
		sb.append(
				"OR ( CASE WHEN trma.periodicservicenowdue_flg = 1 THEN ( CASE WHEN mm.keyikoku_count > 0 THEN 1 ELSE 0 END ) ELSE 0 END ) = 1 ");
		sb.append(
				"OR ( CASE WHEN trma.hour_meter_flg = 1 THEN ( CASE WHEN ( floor(mm.new_hour_meter / 60.0 * 10) / 10 ) <= trma.hour_meter THEN 1 ELSE 0 END ) ELSE 0 END ) = 1 )  ");
				sb.append("THEN 1 ELSE 0 END ) ");
		sb.append("ELSE 1 ");
		sb.append("END ) = 1 ");
		sb.append(") a ");
		sb.append("WHERE a.radius >= a.distance_t ");
		sb.append("AND a.radius < a.distance_y ");
		sb.append("order by a.kiban_serno ");

		// **↑↑↑ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↑↑↑**//
		/*
		sb.append("SELECT ");
		sb.append("a.kiban_serno, ");
		sb.append("a.user_id, ");
		sb.append("a.area_no, ");
		sb.append("b.alertlv, ");
		sb.append("a.yokoku_count, ");
		sb.append("a.keyikoku_count ");
		sb.append("FROM ");
		sb.append("( ");
		sb.append("SELECT ");
		sb.append("mm.kiban_serno,trma.user_id,trma.area_no,mm.yokoku_count,mm.keyikoku_count ");
		sb.append("FROM ");
		sb.append("mst_machine mm ");
		sb.append("JOIN tbl_user_return_machine turm ON mm.kiban_serno = turm.kiban_serno ");
		sb.append("JOIN tbl_return_machine_area trma ON turm.user_id = trma.user_id ");
		sb.append("JOIN tbl_return_machine_work trmw ON turm.kiban_serno = trmw.kiban_serno ");
		sb.append("WHERE ");
		sb.append("mm.delete_flag = 0 ");
		sb.append("AND turm.del_flg = 0 ");
		sb.append("AND trma.delete_flg = 0 ");
		sb.append("AND mm.new_ido IS NOT NULL ");
		sb.append("AND mm.new_keido IS NOT NULL ");
		sb.append(
				"AND ( trma.radius / 1000 >= ( 6378.137 * acos(round(sin(trma.ido / 3600 / 1000 * atan2(0,-1) / 180) * sin(mm.new_ido ");
		sb.append(
				"/ 3600 / 1000 * atan2(0,-1) / 180) + cos(trma.ido / 3600 / 1000 * atan2(0,-1) / 180) * cos(mm.new_ido / 3600 / 1000 * ");
		sb.append(
				"atan2(0,-1) / 180) * cos(abs( (mm.new_keido / 3600 / 1000) - (trma.keido / 3600 / 1000) ) * atan2(0,-1) / 180),20) ) ");
		sb.append(") ) ");
		sb.append(
				"AND ( trma.radius / 1000 < ( 6378.137 * acos(round(sin(trma.ido / 3600 / 1000 * atan2(0,-1) / 180) * sin(trmw.ido / 3600 ");
		sb.append(
				"/ 1000 * atan2(0,-1) / 180) + cos(trma.ido / 3600 / 1000 * atan2(0,-1) / 180) * cos(trmw.ido / 3600 / 1000 * atan2(0 ");
		sb.append(
				",-1) / 180) * cos(abs( (trmw.keido / 3600 / 1000) - (trma.keido / 3600 / 1000) ) * atan2(0,-1) / 180),20) ) ) ) ");
		sb.append("AND ( CASE ");
		sb.append("WHEN trma.warningcurrentlyprogress_flg = 1 ");
		sb.append("OR trma.periodicservicenotice_flg = 1 ");
		sb.append("OR trma.periodicservicenowdue_flg = 1 ");
		sb.append("OR trma.hour_meter_flg = 1  ");
		sb.append("THEN ( CASE ");
		sb.append("WHEN (  ");
		sb.append(
				"( CASE WHEN trma.warningcurrentlyprogress_flg = 1 THEN ( CASE WHEN mm.keyihou_hasseyi_tyu_count > 0 THEN 1 ELSE 0 END ) ELSE 0 END ) = 1 ");
		sb.append(
				"OR ( CASE WHEN trma.periodicservicenotice_flg = 1 THEN ( CASE WHEN mm.yokoku_count > 0 THEN 1 ELSE 0 END ) ELSE 0 END ) = 1 ");
		sb.append(
				"OR ( CASE WHEN trma.periodicservicenowdue_flg = 1 THEN ( CASE WHEN mm.keyikoku_count > 0 THEN 1 ELSE 0 END ) ELSE 0 END ) = 1 ");
		sb.append(
				"OR ( CASE WHEN trma.hour_meter_flg = 1 THEN ( CASE WHEN ( floor(mm.new_hour_meter / 60.0 * 10) / 10 ) <= trma.hour_meter THEN 1 ELSE 0 END ) ELSE 0 END ) = 1 )  ");
		sb.append("THEN 1 ELSE 0 END ) ");
		sb.append("ELSE 1 ");
		sb.append("END ) = 1 ");
		sb.append(") a ");
		sb.append("lEFT JOIN ( ");
		sb.append("SELECT ");
		sb.append("tes.kiban_serno, ");
		sb.append("MAX(mes.alert_lv) alertlv ");
		sb.append("FROM ");
		sb.append("tbl_event_send tes ");
		sb.append("INNER JOIN tbl_keiho_hyoji_set tk ON ( TO_CHAR(tes.event_no) = tk.event_code ");
		sb.append("OR nvl(tes.n_event_no,'N_EVENT_NO') = TO_CHAR(tk.event_code) ) ");
		sb.append("AND tes.event_shubetu = tk.shubetu_code ");
		sb.append("AND tes.con_type = tk.con_type ");
		sb.append("AND tes.machine_kbn = tk.machine_kbn ");
		sb.append("INNER JOIN mst_user mu ON mu.kengen_cd = tk.kengen_cd ");
		sb.append("INNER JOIN ( ");
		sb.append("SELECT DISTINCT ");
		sb.append("event_code, shubetu_code, con_type, machine_kbn,delete_flag,alert_lv ");
		sb.append("FROM mst_event_send ");
		sb.append("WHERE delete_flag = 0 ");
		sb.append(") mes ON tes.event_no = mes.event_code ");
		sb.append("AND tes.event_shubetu = mes.shubetu_code ");
		sb.append("AND tes.con_type = mes.con_type ");
		sb.append("AND tes.machine_kbn = mes.machine_kbn ");
		sb.append("WHERE ");
		sb.append("tes.syori_flg <> 9 ");
		sb.append(
				"AND ( ( tes.event_shubetu = 8 AND tes.con_type IN ('C','C2')) OR ( tes.event_shubetu = 3 AND tes.con_type IN ('T2','T','S','D','N','NH','ND') )) ");
		sb.append("AND tes.fukkyu_timestamp IS NULL ");
		sb.append("GROUP BY tes.kiban_serno ");
		sb.append(") b ON a.kiban_serno = b.kiban_serno ");
*/
		// **↑↑↑ 2019/11/8  iDEA山下 パフォーマンス改修対応 ↑↑↑**//

		List<Object[]> data;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			db.close();
			return data;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}
	
	
	
	
	//2021.07.12 DucNKT リターンマシン　キャンペーン対応 ↓↓↓
	public List<Object[]> findReturnMachineCampaignAndArea(String kibanSerno )
			throws ClassNotFoundException, SQLException, MalformedURLException {
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		StringBuilder sb = new StringBuilder();

		
		sb.append("SELECT ");
		sb.append("a.kiban_serno, "); // 0
		sb.append("a.area_no "); // 1
		sb.append("FROM ");
		sb.append("( ");
		sb.append("SELECT ");
		sb.append("mm.kiban_serno,trma.area_no, ");
		sb.append("trma.radius / 1000 AS radius, ");
		sb.append("( 6378.137 * acos(round(sin(trma.ido / 3600 / 1000 * atan2(0,-1) / 180) * sin(mm.new_ido / 3600 / 1000 * atan2(0,-1) / 180) ");
		sb.append("+ cos(trma.ido / 3600 / 1000 * atan2(0,-1) / 180) * cos(mm.new_ido / 3600 / 1000 * atan2(0,-1) / 180) * cos(abs( (mm.new_keido / 3600 / 1000) ");
		sb.append("- (trma.keido / 3600 / 1000) ) * atan2(0,-1) / 180),20) ) )  AS distance_t, ");
		sb.append("( 6378.137 * acos(round(sin(trma.ido / 3600 / 1000 * atan2(0,-1) / 180) * sin(trmw.ido / 3600 / 1000 * atan2(0,-1) / 180) ");
		sb.append("+ cos(trma.ido / 3600 / 1000 * atan2(0,-1) / 180) * cos(trmw.ido / 3600 / 1000 * atan2(0,-1) / 180) * cos(abs( (trmw.keido / 3600 / 1000) ");
		sb.append("- (trma.keido / 3600 / 1000) ) * atan2(0,-1) / 180),20) ) )  AS distance_y ");
		sb.append("FROM ");
		sb.append("mst_machine mm ");
		sb.append("JOIN tbl_return_machine_work trmw ON mm.kiban_serno = trmw.kiban_serno ");
		sb.append(",tbl_return_machine_area trma "); 
		sb.append("WHERE ");
		sb.append("mm.delete_flag = 0 ");
		sb.append("AND mm.kiban_serno IN( " + kibanSerno + " ) ");
		sb.append("AND trma.delete_flg = 0 ");
		sb.append("AND mm.new_ido IS NOT NULL ");
		sb.append("AND mm.new_keido IS NOT NULL ");
		sb.append(") a ");
		sb.append("WHERE a.radius >= a.distance_t ");
		sb.append("AND a.radius < a.distance_y ");
		sb.append("order by a.kiban_serno ");

		List<Object[]> data;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			db.close();
			return data;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}
	//2021.07.12 DucNKT リターンマシン　キャンペーン対応 ↑↑↑
	
	
	
	

	// **↓↓↓ 2019/11/8  iDEA山下 パフォーマンス改修対応 ↓↓↓**//
	public List<Object[]> findTargetMachine()
			throws ClassNotFoundException, SQLException, MalformedURLException {
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("mm.kiban_serno ");
		sb.append("FROM mst_machine mm ");
		sb.append("JOIN tbl_return_machine_work trmw ON mm.kiban_serno = trmw.kiban_serno ");
		sb.append("WHERE mm.kiban_serno IN( ");
		sb.append("SELECT ");
		sb.append("turm.kiban_serno ");
		sb.append("FROM tbl_user_return_machine turm ");
		sb.append("WHERE turm.del_flg = 0 ");
		sb.append(") ");
		sb.append("AND mm.delete_flag = 0 ");
		sb.append("AND mm.new_ido IS NOT NULL ");
		sb.append("AND mm.new_keido IS NOT NULL ");
		sb.append("AND ( mm.new_ido != trmw.ido ");
		sb.append("OR mm.new_keido != trmw.keido ) ");

		List<Object[]> data;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			db.close();
			return data;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}

	
	//2021.07.27 DucNKT　リターンマシン　キャンペーン対応 ↓↓↓
	public List<Object[]> findTargetMachineCampaign()
			throws ClassNotFoundException, SQLException, MalformedURLException {
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("mm.kiban_serno ");
		sb.append("FROM mst_machine mm ");
		sb.append("JOIN mst_return_machine_cp mrmc ON mm.kiban_serno = mrmc.kiban_serno ");
		sb.append("JOIN tbl_return_machine_work trmw ON mm.kiban_serno = trmw.kiban_serno ");
		sb.append("WHERE mm.delete_flag = 0 ");
		sb.append("AND mrmc.delete_flg = 0 ");
		sb.append("AND mm.new_ido IS NOT NULL ");
		sb.append("AND mm.new_keido IS NOT NULL ");
		sb.append("AND ( mm.new_ido != trmw.ido ");
		sb.append("OR mm.new_keido != trmw.keido ) ");

		List<Object[]> data;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			db.close();
			return data;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}
	//2021.07.27 DucNKT リターンマシン　キャンペーン対応 ↑↑↑
		
	public List<Object[]> dtcLevelCheck(Integer kibanSerno, Integer kengenCd)
			throws ClassNotFoundException, SQLException, MalformedURLException {
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("tes.kiban_serno, ");
		sb.append("MAX(mes.alert_lv) alertlv ");
		sb.append("FROM ");
		sb.append("tbl_event_send tes ");
		// **↓↓↓ 2021/01/28 iDEA Yamashita　結合条件の修正 ↓↓↓**//
		sb.append("INNER JOIN MST_MACHINE mm ON tes.KIBAN_SERNO = mm.KIBAN_SERNO ");
		// **↑↑↑ 2021/01/28 iDEA Yamashita　結合条件の修正 ↑↑↑**//
		sb.append("INNER JOIN tbl_keiho_hyoji_set tk ON ( ");
		sb.append("TO_CHAR(tes.event_no) = tk.event_code ");
		// **↓↓↓ 2021/01/28 iDEA Yamashita　結合条件の修正 ↓↓↓**//
//		sb.append("OR nvl(tes.n_event_no,'N_EVENT_NO') = TO_CHAR(tk.event_code) ) ");
		sb.append("OR (( MM.CON_TYPE != 'D' OR MM.EISEI_FLG != 1) AND NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE) )) ");
		// **↑↑↑ 2021/01/28 iDEA Yamashita　結合条件の修正 ↑↑↑**//
		sb.append("AND tes.event_shubetu = tk.shubetu_code ");
		sb.append("AND tes.con_type = tk.con_type ");
		sb.append("AND tes.machine_kbn = tk.machine_kbn ");
		//sb.append("AND tk.kengen_cd = " + kengenCd + " ");
		sb.append("AND tk.kengen_cd = 2 "); //2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」
		sb.append("INNER JOIN ( ");
		sb.append("SELECT DISTINCT ");
		sb.append("event_code, shubetu_code, con_type, machine_kbn,delete_flag,alert_lv ");
		sb.append("FROM mst_event_send ");
		sb.append("WHERE delete_flag = 0 ");
		sb.append(") mes ON tes.event_no = mes.event_code ");
		sb.append("AND tes.event_shubetu = mes.shubetu_code ");
		sb.append("AND tes.con_type = mes.con_type ");
		sb.append("AND tes.machine_kbn = mes.machine_kbn ");
		// **↓↓↓ 2021/01/28 iDEA Yamashita　処理フラグ9データの参照制御を追加 ↓↓↓**//
		sb.append("LEFT JOIN MST_KENGEN mk ON tk.KENGEN_CD = mk.KENGEN_CD ");
		sb.append("WHERE ");
//		sb.append("tes.syori_flg <> 9 ");
		sb.append("( ( mk.KEIHOU_KENGEN != 1 ");
		sb.append("AND tes.SYORI_FLG != 9 ) ");
		sb.append("OR ( mk.KEIHOU_KENGEN = 1 ) ) ");
		// **↑↑↑ 2021/01/28 iDEA Yamashita　処理フラグ9データの参照制御を追加 ↑↑↑**//
		sb.append("AND ( ( tes.event_shubetu = 8 AND tes.con_type IN ('C','C2')) OR ( tes.event_shubetu = 3 AND tes.con_type IN ('T2','T','S','D','N','NH','ND') )) ");
		sb.append("AND tes.fukkyu_timestamp IS NULL ");
		sb.append("AND tes.kiban_serno = " + kibanSerno +" ");
		sb.append("GROUP BY tes.kiban_serno ");


		List<Object[]> data;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			db.close();
			return data;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}

	// **↑↑↑ 2019/11/8  iDEA山下 パフォーマンス改修対応 ↑↑↑**//

	public Object[] findReturnMachineByAreaCondition(Long kibanSerno, Integer areaNo, String userId,
			Integer warningcurrentlyprogress_flg, Integer periodicservicenotice_flg, Integer periodicservicenowdue_flg,
			Integer hour_meter_flg, Double hour_meter)
			throws ClassNotFoundException, SQLException, MalformedURLException {

		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		StringBuilder sb = new StringBuilder();
		boolean isMultilFlg = false;

		sb.append("SELECT ");
		sb.append("turm.kiban_serno, ");
		sb.append("trma.user_id, ");
		sb.append("trma.area_no ");
		sb.append("FROM ");
		sb.append("tbl_return_machine_area trma ");
		sb.append("INNER JOIN tbl_user_return_machine turm ON ( turm.user_id = trma.user_id ) ");
		sb.append("WHERE ");
		sb.append("trma.delete_flg = 0 ");
		sb.append("AND turm.kiban_serno =" + kibanSerno + " ");
		sb.append("AND trma.area_no =" + areaNo + " ");
		sb.append("AND trma.user_id ='" + userId + "' ");

		if (warningcurrentlyprogress_flg == 1 || periodicservicenotice_flg == 1 || periodicservicenowdue_flg == 1
				|| hour_meter_flg == 1) {

			sb.append("AND ( ");
			// DTC
			if (warningcurrentlyprogress_flg == 1) {
				sb.append("EXISTS ( ");
				sb.append("SELECT mm1.kiban_serno ");
				sb.append("FROM MST_MACHINE mm1 ");
				sb.append("WHERE turm.kiban_serno = mm1.kiban_serno ");
				sb.append("AND mm1.delete_flag = 0 ");
				sb.append("AND mm1.keyihou_hasseyi_tyu_count > 0 ");
				sb.append("AND trma.warningcurrentlyprogress_flg = 1 "); // DTC逋ｺ逕溘メ繧ｧ繝�繧ｯ蟇ｾ雎｡
				sb.append(") ");
				isMultilFlg = true;
			}
			// 螳壽悄謨ｴ蛯吩ｺ亥相
			if (periodicservicenotice_flg == 1) {
				if (isMultilFlg)
					sb.append("OR ");
				sb.append("EXISTS ( ");
				sb.append("SELECT mm2.kiban_serno ");
				sb.append("FROM MST_MACHINE mm2 ");
				sb.append("WHERE turm.kiban_serno = mm2.kiban_serno ");
				sb.append("AND mm2.delete_flag = 0 ");
				sb.append("AND mm2.yokoku_count > 0 ");
				sb.append("AND trma.periodicservicenotice_flg = 1 "); // 螳壽悄謨ｴ蛯吩ｺ亥相逋ｺ逕溘メ繧ｧ繝�繧ｯ蟇ｾ雎｡
				sb.append(") ");
				isMultilFlg = true;
			}
			// 螳壽悄謨ｴ蛯呵ｭｦ蜻�
			if (periodicservicenowdue_flg == 1) {
				if (isMultilFlg)
					sb.append("OR ");
				sb.append("EXISTS ( ");
				sb.append("SELECT mm3.kiban_serno ");
				sb.append("FROM MST_MACHINE mm3 ");
				sb.append("WHERE turm.kiban_serno = mm3.kiban_serno	");
				sb.append("AND mm3.delete_flag = 0 ");
				sb.append("AND mm3.keyikoku_count > 0 ");
				sb.append("AND trma.periodicservicenowdue_flg = 1 "); // 螳壽悄謨ｴ蛯呵ｭｦ蜻顔匱逕溘メ繧ｧ繝�繧ｯ蟇ｾ雎｡
				sb.append(") ");
				isMultilFlg = true;
			}
			// 謖�螳壹い繝ｯ繝｡繝ｼ繧ｿ莉･荳�
			if (hour_meter_flg == 1) {
				if (isMultilFlg)
					sb.append("OR ");
				sb.append("EXISTS ( ");
				sb.append("SELECT ");
				sb.append("mm4.kiban_serno ");
				sb.append("FROM ");
				sb.append("mst_machine mm4 ");
				sb.append("WHERE ");
				sb.append("turm.kiban_serno = mm4.kiban_serno ");
				sb.append("AND trma.hour_meter_flg = 1 "); // 謖�螳壹い繝ｯ繝｡繝ｼ繧ｿ荳矩剞逋ｺ逕溘メ繧ｧ繝�繧ｯ蟇ｾ雎｡
				sb.append("AND floor(mm4.new_hour_meter / 60.0 * 10) / 10 <= trma.hour_meter "); // 繧ｨ繝ｪ繧｢謖�螳壹い繝ｯ繝｡繝ｼ繧ｿ莉･荳�
				sb.append(") ");
			}

			sb.append(") ");
		} else if (warningcurrentlyprogress_flg == 0 & periodicservicenotice_flg == 0 & periodicservicenowdue_flg == 0
				& hour_meter_flg == 0) {
			sb.append("AND ( ");
			sb.append("EXISTS ( ");
			sb.append("SELECT ");
			sb.append("mm5.kiban_serno ");
			sb.append("FROM ");
			sb.append("mst_machine mm5 ");
			sb.append("WHERE ");
			sb.append("turm.kiban_serno = mm5.kiban_serno ");
			sb.append("AND (trma.warningcurrentlyprogress_flg = 0 ");
			sb.append("AND trma.periodicservicenotice_flg = 0 ");
			sb.append("AND trma.periodicservicenowdue_flg = 0 ");
			sb.append("AND trma.hour_meter_flg = 0 ) ");
			sb.append(") ");
			sb.append(") ");
		}

		List<Object[]> data;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			db.close();

			if (data == null || data.size() == 0 || data.size() > 1)
				return null;

			return data.get(0);

		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}



	public int insertToTblReturnMachineHistory(List<ReturnMachineHistoryModel> data)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		sb.append("INSERT ALL ");
		for (ReturnMachineHistoryModel returnMachine : data) {
			sb.append(System.lineSeparator());
			sb.append(GetSqlInsertToTBL_RETURN_MACHINE_HISTORY(returnMachine.getKyotenCd(), returnMachine.getAreaNo(),
					returnMachine.getKibanSerno(), returnMachine.getKeihoIconNo()));
		}
		sb.append(System.lineSeparator());
		sb.append("SELECT * FROM dual");
		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}

	// **↓↓↓ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↓↓↓**//
	public String GetSqlInsertToTBL_RETURN_MACHINE_HISTORY(String kyotenCd, int areaNo, Long kibanSerno,
			int keihoIconNo) {
		StringBuilder sb = new StringBuilder();
		sb.append("INTO TBL_RETURN_MACHINE_HISTORY ( ");
		// sb.append("USER_ID, ");
		sb.append("KYOTEN_CD, ");
		sb.append("AREA_NO, ");
		sb.append("KIBAN_SERNO, ");
		sb.append("RECV_TIMESTAMP, ");
		sb.append("READ_FLG, ");
		sb.append("REGIST_PRG, ");
		sb.append("REGIST_DTM, ");
		sb.append("REGIST_USER, ");
		sb.append("UPDATE_PRG, ");
		sb.append("UPDATE_DTM, ");
		sb.append("UPDATE_USER, ");
		sb.append("KEIHO_ICON_NO ");
		sb.append(") ");
		sb.append("VALUES( ");
		// sb.append("'" + userId + "', ");
		sb.append("'" + kyotenCd + "', ");
		sb.append(areaNo + ", ");
		sb.append(kibanSerno + ", ");
		sb.append("SYSDATE, ");
		sb.append("0, ");
		sb.append("'GNavApp', ");
		sb.append("SYSDATE, ");
		sb.append("'ReturnMachineBatch', ");
		sb.append("'GNavApp', ");
		sb.append("SYSDATE, ");
		sb.append("'ReturnMachineBatch', ");
		sb.append(keihoIconNo + " ");
		sb.append(") ");
		return sb.toString();
	}
	// **↑↑↑ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↑↑↑**//

	public int updateTblReturnMachineWork() throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		
		sb.append("UPDATE TBL_RETURN_MACHINE_WORK trmw ");
		sb.append("SET ido = ( SELECT new_ido FROM	MST_MACHINE mm	WHERE mm.kiban_serno = trmw.kiban_serno ), ");
		sb.append("keido = ( SELECT new_keido FROM	MST_MACHINE mm	WHERE mm.kiban_serno = trmw.kiban_serno ), ");
		sb.append("recv_timestamp = SYSDATE, ");
		sb.append("update_prg = 'GNavApp', ");
		sb.append("update_dtm = SYSDATE, ");
		sb.append("update_user = 'ReturnMachineBatch' ");
		
		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			
			return result;

		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}

	public int updateTblReturnMachineHistory() throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("UPDATE TBL_RETURN_MACHINE_HISTORY ");
		sb.append("SET READ_FLG = 1 ");
		sb.append("WHERE  RECV_TIMESTAMP <= (SYSDATE -2) ");
		sb.append("AND READ_FLG = 0 ");

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;

		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}

	public int insertToTblNotification(String targetUserID)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("INSERT INTO TBL_PUSH_NOTIFICATION( ");
		sb.append("NOTIFICATION_ID, ");
		sb.append("APP_FLG, ");
		sb.append("TARGET_USER_ID, ");
		sb.append("TARGET_KNGEN_CD, ");
		sb.append("LANGUAGE_CD, ");
		sb.append("PUSH_TITLE, ");
		sb.append("PUSH_MESSAGE, ");
		sb.append("PUSH_RSV_DTM, ");
		sb.append("PUSH_SEND_DTM, ");
		sb.append("REMARKS, ");
		sb.append("DEL_FLG, ");
		sb.append("REGIST_PRG, ");
		sb.append("REGIST_DTM, ");
		sb.append("REGIST_USER, ");
		sb.append("UPDATE_PRG, ");
		sb.append("UPDATE_DTM, ");
		sb.append("UPDATE_USER ");
		sb.append(") ");
		sb.append("VALUES( ");
		sb.append("(SELECT (NVL(max(NOTIFICATION_ID),0)+1) from TBL_PUSH_NOTIFICATION), ");
		sb.append("1, ");
		sb.append("'" + targetUserID + "', ");
		sb.append("NULL, ");
		sb.append("NULL, ");
		sb.append("NULL, ");
		sb.append("NULL, ");
		sb.append("SYSDATE, ");
		sb.append("NULL, ");
		sb.append("'ReturnMachine', ");
		sb.append("0, ");
		sb.append("'GNavApp', ");
		sb.append("SYSDATE, ");
		sb.append("'ReturnMachineBatch', ");
		sb.append("'GNavApp', ");
		sb.append("SYSDATE, ");
		sb.append("'ReturnMachineBatch' ");
		sb.append(") ");

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}

	// **↓↓↓ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↓↓↓**//
	public List<Object[]> getUserIDbyKyotenCd(String kyotenCd) throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("SELECT DISTINCT(mrmsu.USER_ID) ");
		sb.append("FROM MST_RETURN_MACHINE_SEND_USER mrmsu ");
		sb.append("WHERE mrmsu.kyoten_cd IN ( "+kyotenCd+" ) ");
		sb.append("AND mrmsu.DELETE_FLG = 0 ");

		try {
			db.open(username, password);
			//logger.log(Level.INFO, new String(sb.toString()));

			List<Object[]> data = db.query(sb.toString());
			db.close();
			return data;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}
	// **↑↑↑ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↑↑↑**//
	
	
	//2021.07.27 DucNKT リターンマシン　キャンペーン対応 ↓↓↓
	public int insertToTblReturnMachineHistoryCampaign(List<Object[]> data)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		sb.append("INSERT ALL ");
		
		for (Object[] obj : data) {
			
			long kibanSerno = (((Number) obj[0]).longValue());
			int  areaNo = (((Number) obj[1]).intValue());	
			
			sb.append(System.lineSeparator());
			sb.append("INTO TBL_RETURN_MACHINE_HISTORY_CP ( ");
			sb.append("AREA_NO, ");
			sb.append("KIBAN_SERNO, ");
			sb.append("RECV_TIMESTAMP, ");
			sb.append("INSERT_PRG, ");
			sb.append("INSERT_DATETIME, ");
			sb.append("INSERT_USER, ");
			sb.append("UPDATE_PRG, ");
			sb.append("UPDATE_DATETIME, ");
			sb.append("UPDATE_USER ");
			sb.append(") ");
			sb.append("VALUES( ");
			sb.append(areaNo + ", ");
			sb.append(kibanSerno + ", ");
			sb.append("SYSDATE, ");
			sb.append("'GNavApp', ");
			sb.append("SYSDATE, ");
			sb.append("'ReturnMachineBatch', ");
			sb.append("'GNavApp', ");
			sb.append("SYSDATE, ");
			sb.append("'ReturnMachineBatch' ");
			sb.append(") ");
		}
		sb.append(System.lineSeparator());
		sb.append("SELECT * FROM dual");
		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}
	}
	//2021.07.27 DucNKT リターンマシン　キャンペーン対応 ↑↑↑
	
	
}