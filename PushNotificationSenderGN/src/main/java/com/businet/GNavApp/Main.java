package com.businet.GNavApp;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.businet.GNavApp.batch.SendDeviceLogFormat;
import com.businet.GNavApp.batch.SendPushNotification;

public class Main {
	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private final static Logger failedDeviceLogger = Logger.getLogger(SendPushNotification.FAILED_DEVICE_LOG);

	public static void main(String[] args) {
		try {

			Application.init();

			String logPath = Application.getCurrent().getConfig().get("Log", "LogFolder", "");
			int MaxFileSize = Application.getCurrent().getConfig().get("Log", "MaxFileSize", 50000);
			int MaxFileCount = Application.getCurrent().getConfig().get("Log", "MaxFileCount", 5);
			// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
			String pushNotificationTarget = Application.getCurrent().getConfig().get("BatchSettings",
					"PushNotificationTarget", "DTC");
			// 2019/06/20 RS DucNKT Add For ReturnMachine :end

			FileHandler failedDeviceLoggerHandle;
			SendDeviceLogFormat logFormat = new SendDeviceLogFormat();

			failedDeviceLoggerHandle = new FileHandler(
					logPath + "/" + Application.getCurrent().getConfig().get("Log", "SentDeviceLog", ""), MaxFileSize,
					MaxFileCount, true);
			failedDeviceLoggerHandle.setLevel(Level.ALL);
			failedDeviceLoggerHandle.setFormatter(logFormat);
			failedDeviceLogger.addHandler(failedDeviceLoggerHandle);

			// 2019/06/20 RS DucNKT modify For ReturnMachine :Start
			if (pushNotificationTarget.equals("ReturnMachine"))
				logger.log(Level.INFO, "START PROGESS RETURN MACHINE");
			else
				logger.log(Level.INFO, "START PROGESS PUSH NOTIFICATION");

			String result = Application.getCurrent().runBatlet(new SendPushNotification(pushNotificationTarget));

//			System.out.println(result);

			if (pushNotificationTarget.equals("ReturnMachine"))
				logger.log(Level.INFO, "END PROGESS RETURN MACHINE\n\n");
			else
				logger.log(Level.INFO, "END PROGESS PUSH NOTIFICATION\n\n");

			// 2019/06/20 RS DucNKT modify For ReturnMachine :end

		} catch (Exception e) {
//			System.out.println("FAILED");
			logger.log(Level.SEVERE, e.getMessage(), e);
		}
	}

}
