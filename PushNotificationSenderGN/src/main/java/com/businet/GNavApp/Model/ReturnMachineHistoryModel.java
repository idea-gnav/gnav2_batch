package com.businet.GNavApp.Model;

public class ReturnMachineHistoryModel {
	long kibanSerno;
	int areaNo;
	// **↓↓↓ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↓↓↓**//
	// String userID;
	String kyotenCd;
	// **↑↑↑ 2020/04/15 RASIS DucNKT 「リターンマシンの拠点別管理対応」と「リターンマシン複数拠店対応」 ↑↑↑**//

	int keihoIconNo;

	public long getKibanSerno() {
		return this.kibanSerno;
	}

	public void setKibanSerno(long kibanSerno) {
		this.kibanSerno = kibanSerno;
	}

	public int getAreaNo() {
		return this.areaNo;
	}

	public void setAreaNo(int areaNo) {
		this.areaNo = areaNo;
	}

	public String getKyotenCd() {
		return this.kyotenCd;
	}

	public void setKyotenCd(String kyotenCd) {
		this.kyotenCd = kyotenCd;
	}

	public int getKeihoIconNo() {
		return this.keihoIconNo;
	}

	public void setKeihoIconNo(int keihoIconNo) {
		this.keihoIconNo = keihoIconNo;
	}

}