package com.lbxco.RemoteCAREApp.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.Semaphore;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.businet.GNavApp.Application;
import com.businet.GNavApp.batch.Batchlet;
import com.eatthepath.pushy.apns.ApnsClient;
import com.eatthepath.pushy.apns.ApnsClientBuilder;
import com.eatthepath.pushy.apns.PushNotificationResponse;
import com.eatthepath.pushy.apns.util.ApnsPayloadBuilder;
import com.eatthepath.pushy.apns.util.SimpleApnsPayloadBuilder;
import com.eatthepath.pushy.apns.util.SimpleApnsPushNotification;
import com.eatthepath.pushy.apns.util.TokenUtil;
import com.eatthepath.pushy.apns.util.concurrent.PushNotificationFuture;


public class SendPushNotification implements Batchlet {

	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private final static Logger deviceLogger = Logger.getLogger(SendPushNotification.FAILED_DEVICE_LOG);
	public final static String FAILED_DEVICE_LOG = "FAILED_DEVICE_LOG";

	private final String DATE_TIME_FORMAT = "yyyyMMdd HH:mm:ss:SSS";

	private String LastSendTimeFile;
	private String PATH_CERTIFICATE;
	private String CERTIFICATE_PASSWORD;

	private String PUSH_NOTIFICATION_TITLE = "Recent DTC";
	private String ENGLISH_MSG = "";
	private String SPAIN_MSG = "";
	private String PORTUGAR_MSG = "";

	// Method to send Notifications from server to client end.
	public String AUTH_KEY_FCM;
	public String API_URL_FCM;
	private boolean IsTestEnvironment = false;

	
	//private String TOPIC_GNAV;//2021.03.23 DucNKT added
	private String TOPIC_REMOTE_CARE;//2021.03.23 DucNKT added
	
	/**
	 * Default constructor.
	 *
	 * @throws Exception
	 */
	public SendPushNotification() throws Exception {

		if (Application.getCurrent().getConfig().get("BatchSettings", "Environment", "product").equals("test")) {
			IsTestEnvironment = true;
		}

		TOPIC_REMOTE_CARE = Application.getCurrent().getConfig().get("BatchSettings", "TopicRemoteCare", "");//2021.03.23 DucNKT added
		
		LastSendTimeFile = Application.getCurrent().getConfig().get("BatchSettings", "LastSendTimeFile", "");
		PATH_CERTIFICATE = Application.getCurrent().getConfig().get("BatchSettings", "IOSCertification", "");
		CERTIFICATE_PASSWORD = Application.getCurrent().getConfig().get("BatchSettings", "IOSCertificationPassword", "");
		AUTH_KEY_FCM = Application.getCurrent().getConfig().get("BatchSettings", "AndroidFCMKey", "");
		API_URL_FCM = Application.getCurrent().getConfig().get("BatchSettings", "AndroidFCMUrl", "");
		ReadMessage(Application.getCurrent().getConfig().get("BatchSettings", "MesssageFile", ""));
	}


//	2018/07/27 aishikawa@LBN move for ExecuteQuerys :Start
//	private List<Object[]> findByDtc(Date lastestDateTime) throws ClassNotFoundException, SQLException, MalformedURLException {}
//	2018/07/27 aishikawa@LBN move for ExecuteQuerys :End


	/**
	 * @throws InterruptedException
	 * @throws TransformerException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @see Batchlet#process()
	 */
	public String process() {

		try {

			// Acquire last communication date
			Date lastestDateTime = ReadLastSend();

			// 2018/07/27 aishikawa@LBN ExecuteQuery :Stert
			ExecuteQuerys eq = new ExecuteQuerys();

			int deleteCount = eq.deleteTblFavoriteDtcHistory(lastestDateTime);
			logger.log(Level.INFO, "Delete 3-months or earlier by tbl_favorite_dtc_history = "+deleteCount);

			int updateCount = eq.updateFukkyuDtcTblFavoriteDtcHistory();
			logger.log(Level.INFO, "Update read_flg by tbl_favorite_dtc_history = "+updateCount);

			// 2018/10/05 aishikawa@lbn nomatch DTC :Start
			updateCount = eq.updateNoMatchTblFavoriteDtcHistory();
			logger.log(Level.INFO, "Update del_flg by tbl_favorite_dtc_history = "+updateCount);
			// 2018/10/05 aishikawa@lbn nomatch DTC :End

			int insertCount = eq.insertByNotExistsTblFavoriteDtcHistory(lastestDateTime);
			logger.log(Level.INFO, "Insert by tbl_favorite_dtc_history = "+insertCount);


			// List of devices that generated DTC after the last update date and time
			List<Object[]> listPushNotification = eq.findByDtc(lastestDateTime);
			// 2018/07/27 aishikawa@LBN ExecuteQuery :End

			// final HashMap<String, String> sentDeviceList = new HashMap<String,String>();
			// Update Last Time Send Push Before Send Push Notification
			WriteLastSend();

			if (listPushNotification != null && listPushNotification.size() > 0) {
				// iOS APNs
				this.SendApns(listPushNotification);
				// Android FCM
				this.SendFCM(listPushNotification);
			}
			else {
				return "No record found";
			}

		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return "FAILED";
		}
		return "Push Notification Complete";

	}


	/**
	 * Send FCM
	 * @param listPushNotification
	 */
	private void SendFCM(List<Object[]> listPushNotification) {

		StringBuilder strDeviceList = new StringBuilder();
		strDeviceList.append("Begin sending a batch to FCM. Device tokens:");

		// 2018/07/24 DucNKT added for notification number:Start
		// JSONArray devices = new JSONArray();
		JSONArray ListMsg = new JSONArray();
		JSONObject Message = new JSONObject();
		JSONObject info = new JSONObject();
		// 2018/07/24 DucNKT added for notification number:End

		for (int i = 0; i < listPushNotification.size(); i++) {
			Object[] pn = listPushNotification.get(i);

			if (pn[0] != null) {
				String deviceType = "";
				String deviceToken = pn[0].toString();
				String userId = "";
				// 2018/06/15 aishikawa@LBN "Display language" added. Currently unused. :Start
				String deviseLanguage = null;
				// 2018/06/15 aishikawa@LBN "Display language" added. Currently unused. :End
				// 2018/07/24 DucNKT added for notification number:Start
				String dtcbadge = "0";
				// 2018/07/24 DucNKT added for notification number:End

				if (pn[1] != null)
					deviceType = pn[1].toString();

				if (pn[2] != null)
					userId = pn[2].toString();

				// 2018/06/15 aishikawa@LBN "Display language" added. Currently unused. :Start
				if (pn[3] != null)
					deviseLanguage = pn[3].toString();
				// 2018/06/15 aishikawa@LBN "Display language" added. Currently unused. :End
				// 2018/07/24 DucNKT added for notification number:Start
				if (pn[4] != null)
					dtcbadge = pn[4].toString();
				// 2018/07/24 DucNKT added for notification number:End

				if (deviceType.equals("2") && !userId.isEmpty() && !deviceToken.isEmpty()) {
					// 2018/07/27 aishikawa@LBN
					strDeviceList.append("\n		[USER_ID: " + userId + "] [DEVICE TOKEN: " + deviceToken +"] [DTC BADGE: "+dtcbadge+"]");

					// 2018/07/24 DucNKT added for notification number:Start
					Message = new JSONObject();
					info = new JSONObject();
					try {
						Message.put("to", deviceToken);
						info.put("title", PUSH_NOTIFICATION_TITLE);
						info.put("body", ENGLISH_MSG);
						info.put("dtcbadge", dtcbadge);
						Message.put("data", info);
						ListMsg.put(Message);
					} catch (Exception e) {
						logger.log(Level.SEVERE, e.getMessage(), e);
					}
					// devices.put(deviceToken);

					// 2018/07/24 DucNKT added for notification number:End

				}
			}
		}

		// 2018/07/24 DucNKT modify for notification number:Start
		// if (devices.length() > 0) {
		if (ListMsg.length() > 0) {
		// 2018/07/24 DucNKT modify for notification number:End

			deviceLogger.log(Level.INFO,strDeviceList.toString());
		}
		else {
			return;
		}

		// FCM Request
		try {
			String authKey = AUTH_KEY_FCM; // You FCM AUTH key
			String FMCurl = API_URL_FCM;

			URL url = new URL(FMCurl);

			// 2018/07/24 DucNKT modify for notification number:Start
			/*
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);

			conn.setRequestMethod("POST");
			conn.setRequestProperty("Authorization", "key=" + authKey);
			conn.setRequestProperty("Content-Type", "application/json");

			JSONObject data = new JSONObject();
			data.put("registration_ids", devices);
			JSONObject info = new JSONObject();
			info.put("title", PUSH_NOTIFICATION_TITLE); // Notification title
			info.put("body", ENGLISH_MSG); // Notification body 英語のみ対応で固定
			data.put("data", info);

			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(data.toString());
			wr.flush();
			wr.close();

			int responseCode = conn.getResponseCode();
			if (responseCode != 200) {
				throw new Exception("Failed ! Respone Code: " + responseCode);
			}

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			if (response != null) {

				JSONObject jResponse = new JSONObject(response.toString());
				int failedCount = jResponse.getInt("failure");

				if (failedCount > 0) {
					JSONArray list = jResponse.getJSONArray("results");

					StringBuilder mes = new StringBuilder();
					mes.append("	Device tokens was sent failed:");

					for(int i=0; i< list.length(); i++) {
						JSONObject res = list.getJSONObject(i);
						if (res.has("error"))
							mes.append("\n		[DEVICE TOKEN: " + devices.getString(i)+"]");
					}

					deviceLogger.log(Level.WARNING,mes.toString());
				}
			}

			in.close();

			*/


			for (int i = 0; i < ListMsg.length(); i++) {

				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setUseCaches(false);
				conn.setDoInput(true);
				conn.setDoOutput(true);

				conn.setRequestMethod("POST");
				conn.setRequestProperty("Authorization", "key=" + authKey);
				conn.setRequestProperty("Content-Type", "application/json");

				JSONObject obj = ListMsg.getJSONObject(i);
				OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
				wr.write(obj.toString());
				wr.flush();
				wr.close();

				int responseCode = conn.getResponseCode();
				if (responseCode != 200) {
					throw new Exception("Failed ! Respone Code: " + responseCode);
				}

				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				if (response != null) {

					JSONObject jResponse = new JSONObject(response.toString());
					int failedCount = jResponse.getInt("failure");

					if (failedCount > 0) {
						JSONArray list = jResponse.getJSONArray("results");

						StringBuilder mes = new StringBuilder();
						mes.append("	Device tokens was sent failed:");

						for (int j = 0; j < list.length(); j++) {
							JSONObject res = list.getJSONObject(j);
							if (res.has("error"))
//								mes.append("\n		[DEVICE TOKEN: " + obj.getString("to") + "]");
								mes.append(System.lineSeparator()+"		[DEVICE TOKEN: " + obj.getString("to") + "]");
						}

						deviceLogger.log(Level.WARNING, mes.toString());
					}
				}

				in.close();
			}
			// 2018/07/24 DucNKT modify for notification number:End


			deviceLogger.log(Level.INFO,"	Sending a batch to FCM devices successfully.\n\n\n" );
		}
		catch (Exception e) {
			deviceLogger.log(Level.WARNING,"	Sending a batch to FCM devices failed. No device tokens was sent. Caused by:" + e.getMessage() +"\n\n\n" );
			logger.log(Level.SEVERE, e.getMessage(), e);
		}

	}



	/**
	 * Send APNs
	 * @param listPushNotification
	 */
	private void SendApns(List<Object[]> listPushNotification) {

		StringBuilder strDeviceList = new StringBuilder();
		strDeviceList.append("Begin sending a batch to APNs. Device tokens:");

		ArrayList<String> devices = new ArrayList<String>();
		// 2018/07/24 DucNKT modify for notification number:Start
		ArrayList<Integer> dtcBadges = new ArrayList<Integer>();
		// 2018/07/24 DucNKT modify for notification number:End

		for (int i = 0; i < listPushNotification.size(); i++) {
			Object[] pn = listPushNotification.get(i);

			if (pn[0] != null) {
				String deviceType = "";
				String deviceToken = pn[0].toString();
				String userId = "";
				// 2018/06/15 aishikawa@LBN "Display language" added. Currently unused. :Start
				String deviseLanguage = null;
				// 2018/06/15 aishikawa@LBN "Display language" added. Currently unused. :End
				// 2018/07/24 DucNKT modify for notification number:Start
				int dtcbadge = 0;
				// 2018/07/24 DucNKT modify for notification number:End

				if (pn[1] != null)
					deviceType = pn[1].toString();

				if (pn[2] != null)
					userId = pn[2].toString();

				// 2018/06/15 aishikawa@LBN "Display language" added. Currently unused. :Start
				if (pn[3] != null)
					deviseLanguage = pn[3].toString();
				// 2018/06/15 aishikawa@LBN "Display language" added. Currently unused. :End

				// 2018/07/24 DucNKT modify for notification number:Start
				if (pn[4] != null)
					dtcbadge = Integer.parseInt(pn[4].toString());
				// 2018/07/24 DucNKT modify for notification number:End

				if (deviceType.equals("1") && !userId.isEmpty() && !deviceToken.isEmpty()) {
//					strDeviceList.append("\n		[USER_ID: " + userId + "] [" + "DEVICE TOKEN: " + deviceToken +"]");
					strDeviceList.append(System.lineSeparator()+"		[USER_ID: " + userId + "] [DEVICE TOKEN: " + deviceToken +"] [DTC BADGE: "+dtcbadge+"]");
					devices.add(deviceToken);
					// 2018/07/24 DucNKT modify for notification number:Start
					dtcBadges.add(dtcbadge);
					// 2018/07/24 DucNKT modify for notification number:End
				}
			}
		}

		if (devices.size() > 0) {
			deviceLogger.log(Level.INFO,strDeviceList.toString());
		}else {
			return;
		}

		// APNs Requests
		ApnsClient apnsClient = null; //2021.09.06 DucNKT modify
		try {

			//2021.03.23 DucNKT modify : start
			/*
			ApnsService service = null;

			// 2018/07/24 DucNKT modified for notification number:Start
			// String payload = APNS.newPayload().alertTitle(PUSH_NOTIFICATION_TITLE).alertBody(ENGLISH_MSG).build();
			PayloadBuilder payloadBuilder = APNS.newPayload();
	        payloadBuilder = payloadBuilder
	        		.alertTitle(PUSH_NOTIFICATION_TITLE)
	                .alertBody(ENGLISH_MSG);
			// 2018/07/24 DucNKT modified for notification number:End

	        if (IsTestEnvironment)
				service = APNS.newService().withCert(PATH_CERTIFICATE, CERTIFICATE_PASSWORD).withSandboxDestination().build();
			else
				service = APNS.newService().withCert(PATH_CERTIFICATE, CERTIFICATE_PASSWORD).withProductionDestination().build();


			// 2018/07/24 DucNKT modify for notification number:Start
			for (int i = 0; i < devices.size(); i++) {

				 PayloadBuilder perDevicePayloadBuilder = payloadBuilder.copy()
	                        .badge(dtcBadges.get(i)).customField("dtcbadge", dtcBadges.get(i)) ;

				if (perDevicePayloadBuilder.isTooLong()) {
    				deviceLogger.log(Level.WARNING, "Payload is too long, shrinking it");
    				perDevicePayloadBuilder = perDevicePayloadBuilder.shrinkBody();
                }

//				System.out.println("[DEBUG] Device="+devices.get(i)+", Payload="+perDevicePayloadBuilder.build());
				service.push(devices.get(i), perDevicePayloadBuilder.build());
				Thread.sleep(500);
			}
			// 2018/07/24 DucNKT modify for notification number:End


			Thread.sleep(10000);

			if (IsTestEnvironment)
				service = APNS.newService().withCert(PATH_CERTIFICATE, CERTIFICATE_PASSWORD).withSandboxDestination().build();
			else
				service = APNS.newService().withCert(PATH_CERTIFICATE, CERTIFICATE_PASSWORD).withProductionDestination().build();


			Map<String, Date> inactiveDevices = service.getInactiveDevices();
			if (inactiveDevices.size() > 0) {
				StringBuilder mes = new StringBuilder();
				mes.append("	Device tokens was sent failed:");
				for (String dv : inactiveDevices.keySet()) {
					mes.append("\n		[DEVICE TOKEN: " + dv+"]");
				}
				deviceLogger.log(Level.WARNING,mes.toString());
			}
			*/
			
			
			String topic = TOPIC_REMOTE_CARE;
			//String topic = TOPIC_GNAV;
			
			apnsClient = new ApnsClientBuilder()
			        .setApnsServer(IsTestEnvironment ? ApnsClientBuilder.DEVELOPMENT_APNS_HOST : ApnsClientBuilder.PRODUCTION_APNS_HOST)
			        .setClientCredentials(new File(PATH_CERTIFICATE),CERTIFICATE_PASSWORD )
			        .build();
			
		
			
			final Map<String, Object> inactiveDevices = new HashMap<String, Object>();
			final ArrayList<String> sendSuccessDevices = new ArrayList<String>();
			
			final Semaphore semaphore = new Semaphore(1000);
			int totalToken = devices.size();
			deviceLogger.log(Level.WARNING,"[DEBUG][iOS] Send notification to "+ totalToken + " token");
			
			for (int i = 0; i < totalToken; i++) {
				semaphore.acquire();
				final ApnsPayloadBuilder payloadBuilder = new SimpleApnsPayloadBuilder();
			    payloadBuilder.setAlertTitle(PUSH_NOTIFICATION_TITLE);
			    payloadBuilder.setAlertBody(ENGLISH_MSG);
			    payloadBuilder.setBadgeNumber(dtcBadges.get(i));
			    payloadBuilder.addCustomProperty("dtcbadge", dtcBadges.get(i)) ;
			    
			    final String payload = payloadBuilder.build();
			    final String token = TokenUtil.sanitizeTokenString(devices.get(i));	
			   			    
			    final SimpleApnsPushNotification pushNotification = new SimpleApnsPushNotification(token, topic, payload);
				final PushNotificationFuture<SimpleApnsPushNotification, PushNotificationResponse<SimpleApnsPushNotification>>
			    sendNotificationFuture = apnsClient.sendNotification(pushNotification);
				
				sendNotificationFuture.whenComplete((response, cause) -> {

					semaphore.release();

					if (response != null) {
						if (response.isAccepted()) {
							sendSuccessDevices.add(response.getPushNotification().getToken());
							System.out.println("[DEBUG] send success: " + response.getPushNotification().getToken());
						} else {
							
							inactiveDevices.put(response.getPushNotification().getToken(),
									response.getRejectionReason());
							System.out.println("[DEBUG] send failed: " + response.getPushNotification().getToken()
									+ "| reasons: " + response.getRejectionReason());

							response.getTokenInvalidationTimestamp().ifPresent(new Consumer<Instant>() {
								@Override
								public void accept(Instant timestamp) {
									System.out.println("[DEBUG] send failed: the token is invalid at " + timestamp);
								}
							});
						}
					} else {
						inactiveDevices.put(pushNotification.getToken(), "Failed to send push notification.");
						System.err.println("[DEBUG] send failed ");
						cause.printStackTrace();
					}
				});
			}										
			
			Thread.sleep(5000);
			
			deviceLogger.log(Level.INFO,"[DEBUG][iOS] permit available "+semaphore.availablePermits());
			
			if(sendSuccessDevices.size() > 0)
			{
				StringBuilder mes = new StringBuilder();
				mes.append("	"+sendSuccessDevices.size()+ " Device tokens was sent successed:");
				for (String dv : sendSuccessDevices) {
					mes.append("\n		[DEVICE TOKEN: " + dv +" ]");
				}
				deviceLogger.log(Level.INFO,mes.toString());
			}
			
			if (inactiveDevices.size() > 0) {
				StringBuilder mes = new StringBuilder();
				mes.append("	"+inactiveDevices.size()+ " Device tokens was sent failed:");
				for (String dv : inactiveDevices.keySet()) {
					mes.append("\n		[DEVICE TOKEN: " + dv +" | Error: "+inactiveDevices.get(dv)+"]");
				}
				deviceLogger.log(Level.INFO,mes.toString());
			}
			
			
			
			//apnsClient.close();
			
			
			//2021.03.23 DucNKT modify : end

			deviceLogger.log(Level.INFO,"	Sending a batch to iOS devices successfully.\n\n\n" );

		}catch (Exception e) {
			deviceLogger.log(Level.WARNING,"	Sending a batch to iOS devices failed. No device tokens was sent. Caused by:" + e.getMessage() +"\n\n\n");
			logger.log(Level.SEVERE, e.getMessage(), e);
		}
		finally {
			if(apnsClient != null)
				apnsClient.close();
		}
	
	}



	/**
	 *
	 * @return
	 * @throws ParseException
	 */
	private Date ReadLastSend() throws ParseException {
		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		SimpleDateFormat fmtUTC = new SimpleDateFormat(DATE_TIME_FORMAT);
		fmtUTC.setTimeZone(timeZone);
		Date today = new Date();
		String lastestTime = "";
		try {
			FileReader fileReader = new FileReader(LastSendTimeFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			lastestTime = bufferedReader.readLine();
			bufferedReader.close();

			return fmtUTC.parse(lastestTime);

		} catch (FileNotFoundException ex) {
			return today;

		} catch (IOException ex) {
			return today;

		} catch (NullPointerException e) {
			return today;
		}
	}



	/**
	 *
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private void WriteLastSend() throws IOException, ClassNotFoundException, SQLException {

		FileWriter fileWriter = new FileWriter(LastSendTimeFile);

		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

		DateFormat dfUTC = new SimpleDateFormat(DATE_TIME_FORMAT);
		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		Date today = new Date();
		dfUTC.setTimeZone(timeZone);


		bufferedWriter.write(dfUTC.format(today));
		bufferedWriter.close();
	}




	/**
	 * Read message.xml
	 * @param path
	 * @throws Exception
	 */
	private void ReadMessage(String path) throws Exception {

		File fXmlFile = new File(path);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);

		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("messages");

		if (nList == null || nList.getLength() < 1)
			throw new Exception("Message file is invalid format");
		Node root = nList.item(0);

		for (int temp = 0; temp < root.getChildNodes().getLength(); temp++) {
			Node nNode = root.getChildNodes().item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				if (nNode.getNodeName() == "english")
					ENGLISH_MSG = nNode.getTextContent();

				else if (nNode.getNodeName() == "spain")
					SPAIN_MSG = nNode.getTextContent();

				else if (nNode.getNodeName() == "portugar")
					PORTUGAR_MSG = nNode.getTextContent();
			}
		}
	}


}