package com.lbxco.RemoteCAREApp.batch;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.businet.GNavApp.Application;
import com.businet.GNavApp.db.SimpleDB;

public class ExecuteQuerys {

	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private String driverPath;
	private String driver;
	private String connectString;
	private String username;
	private String password;


	public ExecuteQuerys() {
		driverPath = Application.getCurrent().getConfig().get("Database", "DriverFile", "");
		driver = Application.getCurrent().getConfig().get("Database", "Driver", "");
		connectString = Application.getCurrent().getConfig().get("Database", "Connection", "");
		username = Application.getCurrent().getConfig().get("Database", "UserName", "");
		password = Application.getCurrent().getConfig().get("Database", "Password", "");
	}



	/**
	 * formatTimeZoneUTC
	 * @param lastestDateTime: Date
	 * @return
	 */
	public String formatTimeZoneUTC(Date DateTime) {
		SimpleDateFormat fmtUTC = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		fmtUTC.setTimeZone(timeZone);

		return fmtUTC.format(DateTime);
	}

	public Date getDateTime(int minusYear, int minusMonth, int minusDate, Date dateTime) {

		Calendar cal = Calendar.getInstance();
//		cal.setTime(new Timestamp(System.currentTimeMillis()));
		cal.setTime(dateTime);
		cal.add(Calendar.YEAR, minusYear);
		cal.add(Calendar.MONTH, minusMonth);
		cal.add(Calendar.DATE, minusDate);

		return cal.getTime();
	}



	/**
	 * insertByNotExistsTblFavoriteDtcHistory
	 *  - 最終処理日時以降に発生したお気に入り機械の未復旧DTCを履歴テーブルに記録する。
	 * @param lastestDateTime
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 */
	public int insertByNotExistsTblFavoriteDtcHistory(Date lastestDateTime)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder insert = new StringBuilder();
		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		insert.append("INSERT INTO TBL_FAVORITE_DTC_HISTORY( ");
		insert.append("USER_ID, ");
		insert.append("KIBAN_SERNO, ");
		insert.append("KIBAN_ADDR, ");
		insert.append("EVENT_NO, ");
		insert.append("EVENT_SHUBETU, ");
		insert.append("RECV_TIMESTAMP, ");
		insert.append("HASSEI_TIMESTAMP, ");
		insert.append("CON_TYPE, ");
		insert.append("READ_FLG, ");
		insert.append("DEL_FLG, ");
		insert.append("REGIST_DTM, ");
		insert.append("REGIST_USER, ");
		insert.append("REGIST_PRG, ");
		insert.append("UPDATE_DTM, ");
		insert.append("UPDATE_USER, ");
		insert.append("UPDATE_PRG ");
		insert.append(") ");

		sb.append("SELECT  ");
		sb.append("TUF.USER_ID, ");
		sb.append("TES.KIBAN_SERNO, ");
		sb.append("TES.KIBAN_ADDR, ");
		sb.append("TES.EVENT_NO, ");
		sb.append("TES.EVENT_SHUBETU, ");
		sb.append("TES.RECV_TIMESTAMP, ");
		sb.append("TES.HASSEI_TIMESTAMP, ");
		sb.append("TES.CON_TYPE, ");
		sb.append("0, ");
		sb.append("0, ");
		sb.append("SYSDATE, ");
		sb.append("'PushBatch', ");
		sb.append("'RemoteCAREApp', ");
		sb.append("SYSDATE, ");
		sb.append("'PushBatch', ");
		sb.append("'RemoteCAREApp' ");

		sb.append("FROM TBL_EVENT_SEND TES ");

		// **↓↓↓ 2021/03/29 iDEA Yamashita　機番マスタ削除フラグ参照 ↓↓↓**//
		sb.append("JOIN MST_MACHINE mm ON tes.KIBAN_SERNO = mm.KIBAN_SERNO AND mm.DELETE_FLAG = 0 ");
		// **↑↑↑ 2021/03/29 iDEA Yamashita　機番マスタ削除フラグ参照 ↑↑↑**//

		sb.append("JOIN TBL_USER_FAVORITE TUF ");
		sb.append("ON TES.KIBAN_SERNO = TUF.KIBAN_SERNO ");
		sb.append("AND TUF.DEL_FLG = 0 ");

		// 2019-02-04 aishikawa@LBN ユーザー絞り込み条件を追加 :Start
		// RemoteCAREAppユーザーのみ絞り込み
		sb.append("AND EXISTS ( ");
		sb.append("SELECT * FROM TBL_USER_DEVICE TUD ");
		sb.append("WHERE TUD.USER_ID = TUF.USER_ID ");
		sb.append("AND TUD.APP_FLG = 0 ");	// 0:RemoteCAREApp
		sb.append(") ");
		// 2019-02-04 aishikawa@LBN ユーザー絞り込み条件を追加 :End

		sb.append("JOIN MST_USER MU ");
		sb.append("ON TUF.USER_ID = MU.USER_ID ");

		sb.append("JOIN MST_SOSIKI MS ");
		sb.append("ON MU.SOSIKI_CD = MS.SOSIKI_CD ");

		sb.append("INNER JOIN TBL_KEIHO_HYOJI_SET TK  ");
		sb.append("ON (TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE OR NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE)) ");
		sb.append("AND TES.EVENT_SHUBETU = TK.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = TK.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = TK.MACHINE_KBN ");
		sb.append("AND TK.KENGEN_CD = MS.KENGEN_CD ");

		sb.append("INNER JOIN MST_EVENT_SEND MES ");
		sb.append("ON TES.EVENT_NO = MES.EVENT_CODE ");
		sb.append("AND TES.EVENT_SHUBETU = MES.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = MES.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = MES.MACHINE_KBN ");
		sb.append("AND MES.DELETE_FLAG = 0 ");
		sb.append("AND MES.LANGUAGE_CD = 'en' ");
		sb.append("AND TES.EVENT_SHUBETU = 8 ");
		sb.append("AND TES.CON_TYPE IN ('C','C2') ");
		sb.append("AND TES.SYORI_FLG <> 9 ");
		sb.append("AND TES.FUKKYU_TIMESTAMP IS NULL ");
//		sb.append("AND TES.HASSEI_TIMESTAMP >= TO_TIMESTAMP('" + formatTimeZoneUTC(lastestDateTime) + "', 'yyyyMMddhh24:mi:ss.FF6') ");
		sb.append("AND TES.REGIST_DTM > TO_TIMESTAMP('" + formatTimeZoneUTC(lastestDateTime) + "', 'yyyyMMddhh24:mi:ss.FF6') ");

		sb.append("AND NOT EXISTS ( ");
		sb.append("SELECT * FROM TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("WHERE DEL_FLG = 0 ");
		sb.append("AND TUF.USER_ID = TFDH.USER_ID ");
		sb.append("AND TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.CON_TYPE = TFDH.CON_TYPE ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append(") ");

//		sb.append("AND TES.FUKKYU_TIMESTAMP IS NULL ");
		sb.append("ORDER BY TES.HASSEI_TIMESTAMP_LOCAL DESC ");

		try {
			db.open(username, password);
			//2019-02-13 aishikawa@LBN add :Start
			List<Object[]> querys = db.query(sb.toString());
			//2019-02-13 aishikawa@LBN add :End
			int result = db.queryUpdate(insert.toString()+sb.toString());
			db.close();
			//2019-02-13 aishikawa@LBN add :Start
			StringBuilder logs = new StringBuilder();
			logs.append("Insert by query. = "+sb.toString()+"\n");
			if(querys.size()>0) {
				for(Object[] log :querys) {
					logs.append("		[USER_ID:"+log[0].toString()+"] [KIBAN_SERNO:"+log[1].toString()+"] [KIBAN_ADDR:"+log[2].toString()+"] [EVENT_NO:"+log[3].toString()+"] [EVENT_SHUBETU:"+log[4].toString()+"] [RECV_TIMESTAMP:"+log[5].toString()+"] [HASSEI_TIMESTAMP:"+log[6].toString()+"] [CON_TYPE:"+log[7].toString()+"]\n");
				}
				logs.append("\n");
			}else {
				logs.append("[NO DATA]\n");
			}
			logger.log(Level.INFO, new String(logs));
			//2019-02-13 aishikawa@LBN add :End
			return result;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}



	/**
	 * updateByFukkyuReadFlg: お気に入りDTCの復旧済み既読フラグ更新
	 * DEL_FLG 0, 1 に関わらず。
	 */
	public int updateFukkyuDtcTblFavoriteDtcHistory()
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("UPDATE TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("SET READ_FLG = 1, ");
		sb.append("UPDATE_DTM = sysdate, ");
		sb.append("UPDATE_USER = 'PushBatch', ");
		sb.append("UPDATE_PRG = 'FukkyuDtc' ");
		sb.append("WHERE TFDH.READ_FLG = 0 ");						// 2018.08.02 修正
		sb.append("AND EXISTS ( ");
		sb.append("SELECT * FROM TBL_EVENT_SEND TES ");
		sb.append("WHERE TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append("AND TES.FUKKYU_TIMESTAMP IS NOT NULL ");
		sb.append(")");

//		System.out.println(new String(sb));

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}


	// 2018/10/05 aishikawa@lbn nomatch DTC :Start
	/**
	 * 処理済フラグ9に更新された場合の考慮
	 * 過去に履歴テーブルに記録されたが、イベントテーブルのキー整合性とれない場合の考慮
	 * 既読フラグ1、削除フラグ1に更新
	 */
	public int updateNoMatchTblFavoriteDtcHistory()
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

		sb.append("UPDATE TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("SET DEL_FLG = 1, ");
		sb.append("READ_FLG = 1, ");
		sb.append("UPDATE_DTM = sysdate, ");
		sb.append("UPDATE_USER = 'PushBatch', ");
		sb.append("UPDATE_PRG = 'NoMatchDtc' ");
		sb.append("WHERE DEL_FLG = 0 ");
		sb.append("AND NOT EXISTS ( ");
		sb.append("SELECT * FROM TBL_EVENT_SEND TES ");
		sb.append("WHERE TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append(") OR EXISTS ( ");
		sb.append("SELECT * FROM TBL_EVENT_SEND TES ");
		sb.append("WHERE TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append("AND TFDH.DEL_FLG = 0 ");
		sb.append("AND TES.SYORI_FLG = 9 ");
		sb.append(") ");

//		System.out.println(new String(sb));

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;

		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}
	// 2018/10/05 aishikawa@lbn nomatch DTC :End



	/**
	 * findByDtc :DTC発生プッシュ通知対象
	 * @param lastestDateTime: Date
	 * @return
	 */
	public List<Object[]> findByDtc(Date lastestDateTime) throws ClassNotFoundException, SQLException, MalformedURLException {

		SimpleDB db = new SimpleDB(driverPath, driver, connectString);
		StringBuilder sb = new StringBuilder();

		// 2018/06/14 aishikawa@LBN "Display language" added due to table definition change :Start
		// sb.append("SELECT DISTINCT(TUD.DEVICE_TOKEN), TUD.DEVICE_TYPE ,TUD.USER_ID FROM TBL_USER_DEVICE TUD ");
		// sb.append("SELECT DISTINCT(TUD.DEVICE_TOKEN), TUD.DEVICE_TYPE ,TUD.USER_ID, TUD.LANGUAGE_CD FROM TBL_USER_DEVICE TUD ");
		// 2018/06/14 aishikawa@LBN "Display language" added due to table definition change :End
		// 2018/07/24 DucNKT added for notification number:Start
		sb.append("SELECT DISTINCT(TUD.DEVICE_TOKEN), TUD.DEVICE_TYPE ,TUD.USER_ID ,TUD.LANGUAGE_CD ");
		sb.append(", ( SELECT COUNT(*) FROM TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("WHERE TFDH.USER_ID = TUD.USER_ID AND DEL_FLG = 0 AND TFDH.READ_FLG = 0 )as BADGE ");
		sb.append("FROM TBL_USER_DEVICE TUD ");
		// 2018/07/24 DucNKT added for notification number:End
		sb.append("INNER JOIN TBL_USER_FAVORITE TUF ");
		sb.append("ON TUD.USER_ID = TUF.USER_ID ");
		sb.append("AND TUD.DEL_FLG = 0  ");
		// 2018/06/14 aishikawa@LBN "application flag" added due to table definition change :Start
		sb.append("AND TUD.APP_FLG = 0 ");
		// 2018/06/14 aishikawa@LBN "application flag" added due to table definition change :End
		sb.append("AND TUF.DEL_FLG = 0 ");
		sb.append("AND EXISTS( ");

		sb.append("SELECT ");
		sb.append("TES.KIBAN_SERNO ");

		sb.append("FROM TBL_EVENT_SEND TES ");

		// **↓↓↓ 2021/03/29 iDEA Yamashita　機番マスタ削除フラグ参照 ↓↓↓**//
		sb.append("JOIN MST_MACHINE mm ON tes.KIBAN_SERNO = mm.KIBAN_SERNO AND mm.DELETE_FLAG = 0 ");
		// **↑↑↑ 2021/03/29 iDEA Yamashita　機番マスタ削除フラグ参照 ↑↑↑**//

		sb.append("INNER JOIN TBL_KEIHO_HYOJI_SET TK  ");
		sb.append("ON (TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE OR NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE)) ");
		sb.append("AND TES.EVENT_SHUBETU = TK.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = TK.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = TK.MACHINE_KBN ");

		sb.append("INNER JOIN MST_USER MU ");
		sb.append("ON MU.KENGEN_CD = TK.KENGEN_CD ");

		sb.append("INNER JOIN MST_EVENT_SEND MES ");
		sb.append("ON TES.EVENT_NO = MES.EVENT_CODE ");
		sb.append("AND TES.EVENT_SHUBETU = MES.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = MES.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = MES.MACHINE_KBN ");

		sb.append("WHERE MES.DELETE_FLAG = 0 ");
		sb.append("AND MES.LANGUAGE_CD = 'en' ");
		sb.append("AND TES.EVENT_SHUBETU = 8 ");

		sb.append("AND TES.CON_TYPE IN ('C','C2') ");
		sb.append("AND TES.SYORI_FLG <> 9 ");
		sb.append("AND MU.USER_ID = TUD.USER_ID ");

		sb.append("AND TES.KIBAN_SERNO =  TUF.KIBAN_SERNO ");

		sb.append("AND TES.REGIST_DTM ");
		sb.append("> TO_TIMESTAMP('" + formatTimeZoneUTC(lastestDateTime) + "', 'yyyyMMddhh24:mi:ss.FF6') ");
		sb.append(" )");

//		System.out.println(new String(sb));

		List<Object[]> data;
		try {
			db.open(username, password);
			data = db.query(sb.toString());
			db.close();
			return data;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}


	/**
	 * deleteTblFavoriteDtcHistory: 最終更新後3ヶ月データの削除
	 * DEL_FLG 0,1 に関わらず、READ_FLG 1 の場合に限る。
	 */
	public int deleteTblFavoriteDtcHistory(Date lastestDateTime)
			throws SQLException, ClassNotFoundException, MalformedURLException {

		StringBuilder sb = new StringBuilder();
		SimpleDB db = new SimpleDB(driverPath, driver, connectString);

//		System.out.println("[DEBUG] LASTEST DATETIME UTC = "+formatTimeZoneUTC(getDateTime(0, -3, 0, lastestDateTime)));

		sb.append("DELETE FROM TBL_FAVORITE_DTC_HISTORY  ");
		sb.append("WHERE READ_FLG = 1 ");
		sb.append("AND UPDATE_DTM < TO_TIMESTAMP('"+formatTimeZoneUTC(getDateTime(0, -3, 0, lastestDateTime))+"', 'yyyyMMddhh24:mi:ss.FF6') ");
		sb.append(" ");

//		System.out.println(new String(sb));

		try {
			db.open(username, password);
			int result = db.queryUpdate(sb.toString());
			db.close();
			return result;
		} catch (Exception ex) {
			db.close();
			throw ex;
		}

	}




}
