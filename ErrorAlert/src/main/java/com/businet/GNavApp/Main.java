package com.businet.GNavApp;

import java.util.logging.Level;
import java.util.logging.Logger;


public class Main {
	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public static void main(String[] args) {
		try {
			Application.init();
			String result = Application.getCurrent().runBatlet(new SendErrorAlert());
//			System.out.println(result);
		} catch (Exception e) {
//			System.out.println("FAILED");

			logger.log(Level.SEVERE, e.getMessage(), e);
		}
	}

}
