package com.businet.GNavApp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.businet.GNavApp.batch.Batchlet;
import com.businet.GNavApp.mail.Mailer;

public class SendErrorAlert implements Batchlet {

	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private String DATE_TIME_FORMAT = "yyyyMMdd HH:mm:ss:SSS";
	private String RegexErrorApiFileName = "";
	private String RegexErrorApiPushNotificationFileName = "Exception";

	private String PATH_API_LOG_FILE;
//	private String PATH_API_PUSH_NOTIFICATION_LOG_FILE;
	private String PATH_API_PUSH_NOTIFICATION_LOG_FILE_RC;
	private String PATH_API_PUSH_NOTIFICATION_LOG_FILE_GN;
	private String PATH_API_PUSH_NOTIFICATION_LOG_FILE_UNIVERSAL;
	// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
	private String PATH_API_RETURN_MACHINE_NOTIFICATION_LOG_FILE_GN;
	// 2019/06/20 RS DucNKT Add For ReturnMachine :End
	// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :Start
	private String PATH_API_SW2_REPORT_LINK_LOG_FILE;
    // 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :End



	private String LastSendTimeFile;

	private String MAIL_TO = "";
	private String Mail_title = "";
	private String Mail_api_error_header = "";
	private String Mail_push_error_header = "";
	// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
	private String Mail_return_machine_error_header = "";
	// 2019/06/20 RS DucNKT Add For ReturnMachine :End
	// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :Start
	private String Mail_sw2_report_link_error_header = "";
	// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :End

	private String result = "";

	private boolean EnableSendAPIError = false;
	private boolean EnableSendPushNotificationError = false;
	private boolean EnableSendReturnMachineNotificationError = false;
	// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :Start
	private boolean EnableSw2ReportLinkError = false;
	// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :End

	private Mailer mailer;

	/**
	 * Default constructor.
	 */
	public SendErrorAlert() {

		PATH_API_LOG_FILE = Application.getCurrent().getConfig().get("BatchSettings", "APILogFolder", "");

//		PATH_API_PUSH_NOTIFICATION_LOG_FILE = Application.getCurrent().getConfig().get("BatchSettings","PushNotificationLogFolder", "");
		PATH_API_PUSH_NOTIFICATION_LOG_FILE_RC = Application.getCurrent().getConfig().get("BatchSettings","PushNotificationLogFolderRC", "");
		PATH_API_PUSH_NOTIFICATION_LOG_FILE_GN = Application.getCurrent().getConfig().get("BatchSettings","PushNotificationLogFolderGN", "");
		PATH_API_PUSH_NOTIFICATION_LOG_FILE_UNIVERSAL = Application.getCurrent().getConfig().get("BatchSettings","PushNotificationLogFolderUniversal", "");
		// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
		PATH_API_RETURN_MACHINE_NOTIFICATION_LOG_FILE_GN = Application.getCurrent().getConfig().get("BatchSettings","ReturnMachineNotificationLogFolderGN", "");
		Mail_return_machine_error_header=  Application.getCurrent().getConfig().get("BatchSettings", "MailReturnMachineNotificationErrorContentHeader", "");
		// 2019/06/20 RS DucNKT Add For ReturnMachine :End
		// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :Start
		PATH_API_SW2_REPORT_LINK_LOG_FILE = Application.getCurrent().getConfig().get("BatchSettings","Sw2ReportLinkLogFolder", "");
		Mail_sw2_report_link_error_header=  Application.getCurrent().getConfig().get("BatchSettings", "MailSw2ReportLinkErrorContentHeader", "");
		// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :End

		LastSendTimeFile = Application.getCurrent().getConfig().get("BatchSettings", "LastSendTimeFile", "");
		MAIL_TO = Application.getCurrent().getConfig().get("BatchSettings", "MailTo", "");
		Mail_title = Application.getCurrent().getConfig().get("BatchSettings", "MailSubject", "");
		Mail_api_error_header = Application.getCurrent().getConfig().get("BatchSettings", "MailAPIErrorContentHeader","");
		Mail_push_error_header = Application.getCurrent().getConfig().get("BatchSettings", "MailPushNotificationErrorContentHeader", "");
		RegexErrorApiFileName = Application.getCurrent().getConfig().get("BatchSettings", "ApiLogFileName", "");
		RegexErrorApiPushNotificationFileName = Application.getCurrent().getConfig().get("BatchSettings", "PushNotificationLogFileName", "");


		EnableSendAPIError = Application.getCurrent().getConfig().get("BatchSettings", "EnableSendErrorAPI", "").equals("1");		// EnableSendErrorAPI = 1:true, 0:false
		EnableSendPushNotificationError = Application.getCurrent().getConfig().get("BatchSettings", "EnableSendPushNotificationError", "").equals("1");
		EnableSendReturnMachineNotificationError = Application.getCurrent().getConfig().get("BatchSettings", "EnableSendReturnMachineNotificationError", "").equals("1");
		// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :Start
		EnableSw2ReportLinkError = Application.getCurrent().getConfig().get("BatchSettings", "EnableSw2ReportLinkError", "").equals("1");
		// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :End

		mailer = Mailer.Instance();

	}


	/**
	 *
	 * @throws Exception
	 * @see Batchlet#process()
	 */
	public String process() throws Exception {
		try {

			Date lastestTime = ReadLastSend();
			Date tempTime = lastestTime;

			// 2018/09/07 aishikawa@LBN modify gnav add :Start
			result += "\n########## Send error mail Start.##########\n";

			Map<String, String> map = new HashMap<String, String>();
			map.put("GNav", PATH_API_PUSH_NOTIFICATION_LOG_FILE_GN);
			map.put("RemoteCARE", PATH_API_PUSH_NOTIFICATION_LOG_FILE_RC);
			map.put("Universal", PATH_API_PUSH_NOTIFICATION_LOG_FILE_UNIVERSAL);
			// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
			map.put("ReturnMachine", PATH_API_RETURN_MACHINE_NOTIFICATION_LOG_FILE_GN);
			// 2019/06/20 RS DucNKT Add For ReturnMachine :End
			map.put("Universal", PATH_API_PUSH_NOTIFICATION_LOG_FILE_UNIVERSAL);
			// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :Start
			map.put("Sw2ReportLink", PATH_API_SW2_REPORT_LINK_LOG_FILE);
			// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :End

			for(String nKey : map.keySet()) {

				String title = "["+nKey+"] "+Mail_title;

				List<String> apiErrorFiles = new ArrayList<String>();
				List<String> pushErrorFiles = new ArrayList<String>();
				// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
				List<String> returnMachineErrorFiles = new ArrayList<String>();
				// 2019/06/20 RS DucNKT Add For ReturnMachine :End
				// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
				List<String> sw2ReportLinkErrorFiles = new ArrayList<String>();
				// 2019/06/20 RS DucNKT Add For ReturnMachine :End
				List<String> pathFileDetail = new ArrayList<String>();

				if (EnableSendAPIError)
					apiErrorFiles = GetFiles(PATH_API_LOG_FILE, RegexErrorApiFileName, lastestTime, 10);

				// 2019/06/20 RS DucNKT Add For ReturnMachine :Start

				if (nKey.equals("ReturnMachine")) {
					if( EnableSendReturnMachineNotificationError)
						returnMachineErrorFiles = GetFiles(map.get(nKey), RegexErrorApiPushNotificationFileName, lastestTime, 10);
				}

				else if (EnableSendPushNotificationError)
					pushErrorFiles = GetFiles(map.get(nKey), RegexErrorApiPushNotificationFileName, lastestTime, 10);

				// 2019/06/20 RS DucNKT Add For ReturnMachine :End
				// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :Start
				else if (nKey.equals("Sw2ReportLink")) {
					if( EnableSw2ReportLinkError)
						sw2ReportLinkErrorFiles = GetFiles(map.get(nKey), RegexErrorApiPushNotificationFileName, lastestTime, 10);
				}
				// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :End

				String mailContent = buildMailContent(apiErrorFiles, pushErrorFiles,returnMachineErrorFiles,sw2ReportLinkErrorFiles);

				pathFileDetail.addAll(apiErrorFiles);
				pathFileDetail.addAll(pushErrorFiles);
				// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
				pathFileDetail.addAll(returnMachineErrorFiles);
				// 2019/06/20 RS DucNKT Add For ReturnMachine :End
				// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :Start
				pathFileDetail.addAll(sw2ReportLinkErrorFiles);
				// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :End

				//2018/04/17 DucNKT modify :Start
				// Update file last time
				if (pathFileDetail.size() > 0) {

					int sizeApi = apiErrorFiles.size();
					int sizePush = pushErrorFiles.size();

					if (sizeApi > 0) {
						BasicFileAttributes attr;
						attr = Files.readAttributes(Paths.get(apiErrorFiles.get(0)), BasicFileAttributes.class);
						lastestTime = new Date(attr.creationTime().to(TimeUnit.MILLISECONDS));
					}

					if (sizePush > 0) {
						BasicFileAttributes attr;
						attr = Files.readAttributes(Paths.get(pushErrorFiles.get(0)), BasicFileAttributes.class);
						Date fileDate = new Date(attr.creationTime().to(TimeUnit.MILLISECONDS));

						// 2018/12/27 aishikawa@LBN modify :Start
//						if (fileDate.compareTo(lastestTime) > 0) {
						if (fileDate.after(lastestTime)) {
							if(fileDate.after(tempTime))
								tempTime = fileDate;
						}
						// 2018/12/27 aishikawa@LBN modify :End
					}
				}

				if (mailContent.length() > 0) {
					if (mailer.sendMail(MAIL_TO, title, mailContent, pathFileDetail)) {
						result += "["+nKey+"] SEND MAIL SUCCESSFULLY \n";
					} else {
						result += "["+nKey+"] SEND MAIL FAILED \n";
					}
				} else {
					result += "["+nKey+"] NO ERROR WAS FOUND \n";
				}
			}
			// 2018/12/27 aishikawa@LBN Add :Start
			lastestTime = tempTime;
			// 2018/12/27 aishikawa@LBN Add :End
			System.out.println(lastestTime);
			WriteLastSend(lastestTime);

			result += "########## Send error mail End. ##########\n\n\n";
			// 2018/09/07 aishikawa@LBN modify gnav add :End

			logger.log(Level.INFO, result);
			return result;

		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return "PROCESSING WAS FAILED";
		}

	}



	private Date ReadLastSend() {
		SimpleDateFormat fmt = new SimpleDateFormat(DATE_TIME_FORMAT);
		String lastestTime = "";
		try {
			FileReader fileReader = new FileReader(LastSendTimeFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			lastestTime = bufferedReader.readLine();
			bufferedReader.close();
			return fmt.parse(lastestTime);
		} catch (FileNotFoundException ex) {
			return Calendar.getInstance().getTime();
		} catch (IOException ex) {
			return Calendar.getInstance().getTime();
		} catch (ParseException e) {
			return Calendar.getInstance().getTime();
		} catch (NullPointerException e) {
			return Calendar.getInstance().getTime();
		}

	}



	//2018/04/17 DucNKT modify :Start
	private void WriteLastSend(Date LastTime) throws IOException {

		FileWriter fileWriter = new FileWriter(LastSendTimeFile);

		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

		DateFormat df = new SimpleDateFormat(DATE_TIME_FORMAT);
		// Date today = Calendar.getInstance().getTime();
		// bufferedWriter.write(df.format(today));
		bufferedWriter.write(df.format(LastTime));

		bufferedWriter.close();

	}
	//2018/04/17 DucNKT modify :End




	private List<String> GetFiles(String path, String RegexFilename, Date fromDate, int maxFileNum) throws IOException {
		List<File> allFile = new ArrayList<File>();
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();

		if(listOfFiles != null) {
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					String fileName = listOfFiles[i].getName();
					if (fileName.matches(RegexFilename)) {

						BasicFileAttributes attr;
						attr = Files.readAttributes(listOfFiles[i].toPath(), BasicFileAttributes.class);

						Date createDate = new Date(attr.creationTime().to(TimeUnit.MILLISECONDS));
						if (createDate.compareTo(fromDate) > 0) {
							allFile.add(listOfFiles[i]);
						}
					}
				}
			}
		}

		Collections.sort(allFile, new Comparator<File>() {
			@Override
			public int compare(File lhs, File rhs) {
				Date createDate1;
				Date createDate2;
				BasicFileAttributes attr;

				try {
					attr = Files.readAttributes(lhs.toPath(), BasicFileAttributes.class);
					createDate1 = new Date(attr.creationTime().to(TimeUnit.MILLISECONDS));

					attr = Files.readAttributes(rhs.toPath(), BasicFileAttributes.class);
					createDate2 = new Date(attr.creationTime().to(TimeUnit.MILLISECONDS));

					return createDate1.compareTo(createDate2);

				} catch (IOException e) {
					return 0;
				}
			}
		});

		List<String> returnList = new ArrayList<String>();

		//2018/04/17 DucNKT modify :Start
		int total = allFile.size();

		if (total > 0) {
			for (int i = total - 1; i >= 0 && i >= (total - maxFileNum); i--) {
				returnList.add(allFile.get(i).toPath().toString());
			}
		}

		/*
		 * for (int i = 0; i < allFile.size() && i < maxFileNum; i++) {
		 * returnList.add(allFile.get(i).toPath().toString()); }
		 */
		//2018/04/17 DucNKT modify :End

		return returnList;
	}




	/**
	 *
	 * @param apiErrorFiles
	 * @param pushErrorFiles
	 * @return mailContent
	 */
	private String buildMailContent(
			List<String> apiErrorFiles,
			List<String> pushErrorFiles,
			List<String> returnMachineErrorFiles,
			List<String> sw2ReportLinkErrorFiles) {
		StringBuilder mailContent = new StringBuilder();

		if (apiErrorFiles.size() > 0) {
			mailContent.append('\n');
			mailContent.append(Mail_api_error_header);
			mailContent.append('\n');
			for (int i = 0; i < apiErrorFiles.size(); i++) {
				mailContent.append(String.valueOf(i + 1));
				mailContent.append(") ");
				mailContent.append(getName(apiErrorFiles.get(i)));
				mailContent.append(" ");
				mailContent.append(getError(apiErrorFiles.get(i)));
				mailContent.append('\n');
			}

		}

		if (pushErrorFiles.size() > 0) {
			mailContent.append('\n');
			mailContent.append(Mail_push_error_header);
			mailContent.append('\n');
			for (int i = 0; i < pushErrorFiles.size(); i++) {
				mailContent.append(String.valueOf(i + 1));
				mailContent.append(") ");
				mailContent.append(getName(pushErrorFiles.get(i)));
				mailContent.append(" ");
				mailContent.append(getError(pushErrorFiles.get(i)));
				mailContent.append('\n');
			}

		}


		// 2019/06/20 RS DucNKT Add For ReturnMachine :Start
		if (returnMachineErrorFiles.size() > 0) {
			mailContent.append('\n');
			mailContent.append(Mail_return_machine_error_header);
			mailContent.append('\n');
			for (int i = 0; i < returnMachineErrorFiles.size(); i++) {
				mailContent.append(String.valueOf(i + 1));
				mailContent.append(") ");
				mailContent.append(getName(returnMachineErrorFiles.get(i)));
				mailContent.append(" ");
				mailContent.append(getError(returnMachineErrorFiles.get(i)));
				mailContent.append('\n');
			}

		}
		// 2019/06/20 RS DucNKT Add For ReturnMachine :End

		// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :Start
		if (sw2ReportLinkErrorFiles.size() > 0) {
			mailContent.append('\n');
			mailContent.append(Mail_sw2_report_link_error_header);
			mailContent.append('\n');
			for (int i = 0; i < sw2ReportLinkErrorFiles.size(); i++) {
				mailContent.append(String.valueOf(i + 1));
				mailContent.append(") ");
				mailContent.append(getName(sw2ReportLinkErrorFiles.get(i)));
				mailContent.append(" ");
				mailContent.append(getError(sw2ReportLinkErrorFiles.get(i)));
				mailContent.append('\n');
			}

		}
		// 2020/10/19 iDEA Yamashita Add For Sw2ReportLink :End


		return mailContent.toString();
	}


	/**
	 *
	 * @param filename
	 * @return
	 */
	private String getName(String filename) {
		File f = new File(filename);
		return f.getName();
	}




	/**
	 *
	 * @param filename
	 * @return
	 */
	private String getError(String filename) {
		BufferedReader in = null;
		String cause = "";

		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "UTF-8"));	// 譁�蟄怜喧縺大ｯｾ蠢� SJIS or UTF-8 繧ｵ繝ｼ繝舌�ｼ迺ｰ蠅�縺ｫ繧医ｋ
			String str;
			while ((str = in.readLine()) != null) {
				if (str.indexOf("Caused by") >= 0) {
					cause = str.substring(str.indexOf("Caused by") + 10);
					break;
				}
			}
			in.close();

		} catch (Exception ex) {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return cause;
	}



}
